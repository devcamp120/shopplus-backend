-- MySQL dump 10.13  Distrib 8.0.32, for Win64 (x86_64)
--
-- Host: localhost    Database: shop_plus_db
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employees` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `extension` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `job_title` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `report_to` bigint DEFAULT NULL,
  `office_code` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_j9xgmd0ya5jmus09o0b8pqrpb` (`email`),
  KEY `FK6gh4conpwfddlrmed6s5sw10v` (`office_code`),
  CONSTRAINT `FK6gh4conpwfddlrmed6s5sw10v` FOREIGN KEY (`office_code`) REFERENCES `offices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (1,'luongvanbacabcxyz@gmail.com','abc','Bac','abc','Luong',4,1),(2,'abcxyz@gmail.com','abc','Abc','abc','xyz',1,1),(3,'aaa@gmail.com','abc','aaa','abc','bbb',2,1);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graphics`
--

DROP TABLE IF EXISTS `graphics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `graphics` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `gpu_memory` tinyint NOT NULL,
  `gpu_name` varchar(50) NOT NULL,
  `graphics_brand_id` bigint NOT NULL,
  `graphics_type_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_so7glu4y4p1qbkr5b46yoxsyq` (`gpu_name`),
  KEY `FKfryv71fxgc01f1di4mlt8n7pa` (`graphics_brand_id`),
  KEY `FK707ulskcf4jaauk38o44wp9xb` (`graphics_type_id`),
  CONSTRAINT `FK707ulskcf4jaauk38o44wp9xb` FOREIGN KEY (`graphics_type_id`) REFERENCES `graphics_types` (`id`),
  CONSTRAINT `FKfryv71fxgc01f1di4mlt8n7pa` FOREIGN KEY (`graphics_brand_id`) REFERENCES `graphics_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graphics`
--

LOCK TABLES `graphics` WRITE;
/*!40000 ALTER TABLE `graphics` DISABLE KEYS */;
INSERT INTO `graphics` VALUES (1,4,'GTX 1650',1,1),(3,2,'MX350',1,1),(4,6,'A3000',1,3),(5,16,'A5500',1,3),(6,4,'RTX 3050',1,1),(7,6,'RTX 3060',1,1);
/*!40000 ALTER TABLE `graphics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graphics_brands`
--

DROP TABLE IF EXISTS `graphics_brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `graphics_brands` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(2500) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_eg1o4sqag9os5bexcbbndd83n` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graphics_brands`
--

LOCK TABLES `graphics_brands` WRITE;
/*!40000 ALTER TABLE `graphics_brands` DISABLE KEYS */;
INSERT INTO `graphics_brands` VALUES (1,'','NVIDIA'),(2,'','AMD');
/*!40000 ALTER TABLE `graphics_brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graphics_types`
--

DROP TABLE IF EXISTS `graphics_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `graphics_types` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(2500) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `graphics_brand_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8ucmuqieqfyfwl3qwmxofbul4` (`name`),
  KEY `FKpsm0ams7xw6es62rpeol3vbd9` (`graphics_brand_id`),
  CONSTRAINT `FKpsm0ams7xw6es62rpeol3vbd9` FOREIGN KEY (`graphics_brand_id`) REFERENCES `graphics_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graphics_types`
--

LOCK TABLES `graphics_types` WRITE;
/*!40000 ALTER TABLE `graphics_types` DISABLE KEYS */;
INSERT INTO `graphics_types` VALUES (1,'','GeForce',1),(3,'','RTX',1),(4,'','Quadro',1),(5,'','Radeon',2);
/*!40000 ALTER TABLE `graphics_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS `offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `offices` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `address_line` varchar(255) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `state` varchar(50) DEFAULT NULL,
  `territory` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` VALUES (1,'Sai Gon, Vietnam','Sai Gon','Vietnam','0982584208','Vietnam','Level 5');
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_details` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `price_each` decimal(10,2) NOT NULL,
  `quantity_order` bigint NOT NULL,
  `order_id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKjyu2qbqt8gnvno9oe9j2s2ldk` (`order_id`),
  KEY `FK4q98utpd73imf4yhttm3w0eax` (`product_id`),
  CONSTRAINT `FK4q98utpd73imf4yhttm3w0eax` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FKjyu2qbqt8gnvno9oe9j2s2ldk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
INSERT INTO `order_details` VALUES (2,123.00,123,2,10),(5,123.00,123,2,10),(15,1150.00,2,11,10),(18,1150.00,5,13,10),(21,1150.00,5,14,10),(29,1150.00,1,23,10),(31,1150.00,2,25,10),(40,1150.00,5,33,10),(41,1150.00,5,34,10),(43,1150.00,1,36,10),(47,1150.00,1,38,10),(48,1150.00,1,39,10),(51,1150.00,1,42,10),(54,1150.00,1,45,10),(58,1150.00,1,10,10),(59,1150.00,5,12,10),(60,1150.00,1,48,10),(62,9050.00,1,49,19),(63,1150.00,2,50,10),(65,850.00,1,50,18),(66,652.50,1,51,17),(67,652.50,2,52,17),(68,3955.00,1,52,20),(69,850.00,1,53,18),(70,890.00,1,53,25),(71,1150.00,1,53,10),(72,850.00,1,26,18),(76,1561.50,2,54,24),(77,900.00,3,55,21),(79,860.00,2,55,27),(81,652.50,2,56,17),(82,890.00,1,56,25),(83,2280.00,1,17,22),(84,652.50,2,57,17),(85,850.00,2,57,18),(86,3955.00,1,17,20),(87,652.50,1,58,17),(88,826.50,1,58,29),(89,652.50,2,59,17),(90,900.00,1,60,21),(91,826.50,1,61,29),(92,652.50,1,62,17),(93,826.50,2,63,29),(94,652.50,1,65,17),(95,3955.00,1,71,20),(96,826.50,2,76,29),(97,826.50,1,77,29),(98,890.00,1,78,25),(99,860.00,2,78,27),(100,900.00,1,79,21),(101,1368.00,2,80,23),(103,3955.00,1,81,20),(104,1561.50,5,82,24),(105,3955.00,2,83,20),(107,3955.00,2,85,20),(108,1561.50,3,86,24),(109,900.00,2,87,21),(111,1368.00,1,88,23),(112,3955.00,1,89,20),(113,652.50,1,89,17),(114,652.50,1,90,17);
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `comments` varchar(255) DEFAULT NULL,
  `order_date` datetime(6) NOT NULL,
  `required_date` datetime(6) NOT NULL,
  `shipped_date` datetime(6) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `updated_at` datetime(6) DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `order_code` varchar(255) NOT NULL,
  `user_id` bigint NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `postal_code` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_dhk2umg8ijjkg4njg6891trit` (`order_code`),
  KEY `FK32ql8ubntj5uh44ph9659tiih` (`user_id`),
  CONSTRAINT `FK32ql8ubntj5uh44ph9659tiih` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (2,'ABC','2023-02-13 19:27:29.579000','2023-02-19 07:00:00.000000','2023-02-13 19:40:33.421000','Refund','2023-02-13 19:27:29.616000',1,0,'2023-03-15 22:10:12.324000',9,'RZQKNEKR',3,'',NULL,NULL,'','','',NULL,NULL),(4,'','2023-02-13 19:41:30.288000','2023-02-15 07:00:00.000000',NULL,'Refund','2023-02-13 19:41:30.288000',1,0,'2023-02-13 19:41:40.403000',1,'SLLXTCPP',3,'',NULL,NULL,'','','',NULL,NULL),(8,'','2023-02-24 19:53:18.814000','2023-02-24 07:00:00.000000','2023-03-12 10:04:45.294000','Refund','2023-02-24 19:53:18.814000',4,0,'2023-03-15 22:14:58.249000',9,'9m66wsgw',3,'',NULL,NULL,'','','',NULL,NULL),(10,'ABCXYZ','2023-02-24 19:58:46.381000','2023-02-24 07:00:00.000000','2023-03-13 11:38:47.549000','Completed','2023-02-24 19:58:46.381000',4,0,'2023-03-13 11:38:47.555000',3,'D1MV0T8N',3,'',NULL,NULL,'','','',NULL,NULL),(11,'','2023-02-24 19:59:52.149000','2023-02-24 07:00:00.000000','2023-03-12 10:04:14.659000','Refund','2023-02-24 19:59:52.149000',4,0,'2023-03-13 12:13:46.169000',3,'V6S6TRNV',3,'',NULL,NULL,'','','',NULL,NULL),(12,'','2023-02-24 20:06:16.345000','2023-02-24 07:00:00.000000','2023-03-15 21:37:00.126000','Completed','2023-02-24 20:06:16.345000',4,0,'2023-03-15 21:37:00.127000',9,'T2UFVLVC',3,'',NULL,NULL,'','','',NULL,NULL),(13,'','2023-02-24 20:18:54.883000','2023-02-24 07:00:00.000000',NULL,'Canceled','2023-02-24 20:18:54.883000',4,0,'2023-03-23 17:48:29.778000',3,'W2TRWDJP',3,'',NULL,NULL,'','','',NULL,NULL),(14,'','2023-02-24 20:34:05.880000','2023-02-24 07:00:00.000000','2023-03-12 12:10:15.970000','Completed','2023-02-24 20:34:05.880000',4,0,'2023-03-12 12:10:16.011000',3,'X5UIIPJP',3,'',NULL,NULL,'','','',NULL,NULL),(16,'','2023-02-26 15:53:06.932000','2023-02-26 07:00:00.000000','2023-03-15 20:08:37.108000','Completed','2023-02-26 15:53:06.932000',4,0,'2023-03-15 20:08:37.108000',3,'HGCA0SRN',3,'',NULL,NULL,'','','',NULL,NULL),(17,'','2023-02-26 15:55:46.024000','2023-02-26 07:00:00.000000','2023-03-17 22:01:39.783000','Completed','2023-02-26 15:55:46.024000',4,0,'2023-03-17 22:01:39.784000',3,'5MPD8FNA',3,'',NULL,NULL,'','','',NULL,NULL),(18,'','2023-02-26 16:15:15.884000','2023-02-26 07:00:00.000000','2023-03-12 10:04:54.301000','Completed','2023-02-26 16:15:15.884000',4,0,'2023-03-12 10:04:54.302000',3,'O42CB1VM',3,'',NULL,NULL,'','','',NULL,NULL),(19,NULL,'2023-02-26 16:17:12.288000','2023-02-26 16:17:12.286000',NULL,'Pending','2023-02-26 16:17:12.288000',4,0,'2023-02-26 16:17:12.288000',4,'USE932ZS',3,'',NULL,NULL,'','','',NULL,NULL),(20,'','2023-02-26 16:18:24.056000','2023-02-26 07:00:00.000000','2023-03-16 18:53:42.415000','Completed','2023-02-26 16:18:24.056000',4,0,'2023-03-16 18:53:42.425000',3,'3A353XPD',3,'',NULL,NULL,'','','',NULL,NULL),(21,NULL,'2023-02-26 16:19:21.519000','2023-02-26 16:19:21.518000',NULL,'Pending','2023-02-26 16:19:21.519000',4,0,'2023-02-26 16:19:21.519000',4,'BN77Z0D3',3,'',NULL,NULL,'','','',NULL,NULL),(22,'','2023-02-26 16:20:56.864000','2023-02-26 07:00:00.000000','2023-03-12 10:04:31.176000','Completed','2023-02-26 16:20:56.864000',4,0,'2023-03-12 10:04:31.177000',3,'LWV86XZH',3,'',NULL,NULL,'','','',NULL,NULL),(23,NULL,'2023-02-26 18:49:07.506000','2023-02-26 18:49:07.504000',NULL,'Pending','2023-02-26 18:49:07.506000',4,0,'2023-02-26 18:49:07.506000',4,'WNBDLM8T',3,'',NULL,NULL,'','','',NULL,NULL),(24,'','2023-03-04 15:48:46.570000','2023-03-04 07:00:00.000000','2023-03-12 12:10:27.042000','Completed','2023-03-04 15:48:46.570000',4,0,'2023-03-12 12:10:27.043000',3,'MZ8KOP8Q',3,'',NULL,NULL,'','','',NULL,NULL),(25,'','2023-03-06 10:37:06.220000','2023-03-06 07:00:00.000000','2023-03-06 10:37:46.163000','Completed','2023-03-06 10:37:06.220000',4,0,'2023-03-06 10:37:46.167000',3,'N3210CS9',3,'',NULL,NULL,'','','',NULL,NULL),(26,'','2023-03-09 06:54:54.410000','2023-03-08 07:00:00.000000',NULL,'Pending','2023-03-09 06:54:54.410000',8,0,'2023-03-16 22:48:36.534000',3,'CV10LVN3',3,'',NULL,NULL,'','','',NULL,NULL),(27,NULL,'2023-03-09 11:30:57.902000','2023-03-09 11:30:57.868000',NULL,'Pending','2023-03-09 11:30:57.902000',8,0,'2023-03-09 11:30:57.902000',8,'EB1VHBA6',3,'',NULL,NULL,'','','',NULL,NULL),(28,'','2023-03-09 11:40:53.828000','2023-03-09 07:00:00.000000','2023-03-09 11:52:37.759000','Completed','2023-03-09 11:40:53.828000',8,0,'2023-03-09 11:52:37.793000',3,'VYABCO63',3,'',NULL,NULL,'','','',NULL,NULL),(31,'','2023-03-10 12:55:45.438000','2023-03-10 07:00:00.000000','2023-03-10 12:56:25.331000','Completed','2023-03-10 12:55:45.438000',3,0,'2023-03-10 12:56:25.332000',3,'SWJ7V6H7',3,'',NULL,NULL,'','','',NULL,NULL),(33,'','2023-03-10 14:43:43.220000','2023-03-10 07:00:00.000000','2023-03-12 12:10:39.940000','Completed','2023-03-10 14:43:43.220000',3,0,'2023-03-12 12:10:39.941000',3,'JOR76CC1',3,'',NULL,NULL,'','','',NULL,NULL),(34,'','2023-03-10 14:50:41.811000','2023-03-10 07:00:00.000000','2023-03-10 16:48:24.095000','Refund','2023-03-10 14:50:41.811000',3,0,'2023-03-10 17:34:32.153000',3,'NJ5EWZ8X',3,'',NULL,NULL,'','','',NULL,NULL),(36,'','2023-03-12 00:33:15.152000','2023-03-11 07:00:00.000000','2023-03-12 10:05:12.893000','Completed','2023-03-12 00:33:15.152000',3,0,'2023-03-12 10:05:12.894000',3,'AMG28WYV',3,'',NULL,NULL,'','','',NULL,NULL),(38,'ABC','2023-03-12 22:17:09.878000','2023-03-14 07:00:00.000000',NULL,'Pending','2023-03-12 22:17:09.878000',3,0,'2023-03-12 22:17:09.878000',3,'P75WM1FB',3,'',NULL,NULL,'','','',NULL,NULL),(39,'ABC','2023-03-12 22:20:05.333000','2023-03-14 07:00:00.000000','2023-03-14 23:00:53.391000','Completed','2023-03-12 22:20:05.333000',3,0,'2023-03-14 23:00:53.392000',3,'ZP3IYX2E',3,'',NULL,NULL,'','','',NULL,NULL),(42,'ABC','2023-03-12 22:25:31.702000','2023-03-14 07:00:00.000000','2023-03-15 20:08:11.463000','Completed','2023-03-12 22:25:31.702000',3,0,'2023-03-15 20:08:11.472000',3,'G0WL7GEO',3,'',NULL,NULL,'','','',NULL,NULL),(45,'ABC','2023-03-12 22:28:56.466000','2023-03-14 07:00:00.000000','2023-03-15 20:08:56.376000','Completed','2023-03-12 22:28:56.466000',3,0,'2023-03-15 20:08:56.377000',3,'Q83HS3JE',3,'',NULL,NULL,'','','',NULL,NULL),(48,'','2023-03-13 19:15:47.998000','2023-03-13 07:00:00.000000','2023-03-14 10:04:41.940000','Completed','2023-03-13 19:15:47.998000',4,0,'2023-03-14 10:04:41.969000',3,'NS5TPUFZ',3,'',NULL,NULL,'','','',NULL,NULL),(49,'','2023-03-14 23:00:25.668000','2023-03-14 07:00:00.000000','2023-03-14 23:00:42.772000','Refund','2023-03-14 23:00:25.668000',3,0,'2023-03-15 22:15:33.454000',9,'ELZ4I2LO',3,'',NULL,NULL,'','','',NULL,NULL),(50,'','2023-03-15 20:10:33.731000','2023-03-15 07:00:00.000000','2023-03-15 20:10:50.646000','Completed','2023-03-15 20:10:33.731000',3,0,'2023-03-15 20:10:50.647000',3,'2T408EFE',3,'',NULL,NULL,'','','',NULL,NULL),(51,'','2023-03-15 21:38:46.520000','2023-03-16 07:00:00.000000','2023-03-16 18:53:50.929000','Completed','2023-03-15 21:38:46.520000',9,0,'2023-03-16 18:53:50.929000',3,'YUS92GW1',3,'',NULL,NULL,'','','',NULL,NULL),(52,'ABC','2023-03-15 21:44:57.451000','2023-03-17 07:00:00.000000','2023-03-15 21:46:58.933000','Completed','2023-03-15 21:44:57.451000',9,0,'2023-03-15 21:47:19.921000',9,'TMV473GA',3,'',NULL,NULL,'','','',NULL,NULL),(53,'','2023-03-16 18:54:28.699000','2023-03-16 07:00:00.000000','2023-03-16 18:55:18.205000','Completed','2023-03-16 18:54:28.699000',3,0,'2023-03-16 18:55:18.206000',3,'GC5G0DM2',3,'',NULL,NULL,'','','',NULL,NULL),(54,'ABC','2023-03-16 23:21:06.894000','2023-03-17 07:00:00.000000','2023-03-16 23:22:20.013000','Completed','2023-03-16 23:21:06.894000',3,0,'2023-03-16 23:22:20.014000',3,'G5L3TJUT',3,'',NULL,NULL,'','','',NULL,NULL),(55,'ABCXYZ','2023-03-16 23:22:03.417000','2023-03-17 07:00:00.000000','2023-03-16 23:22:57.215000','Completed','2023-03-16 23:22:03.417000',3,0,'2023-03-16 23:22:57.215000',3,'RUU7LRYD',3,'',NULL,NULL,'','','',NULL,NULL),(56,'','2023-03-17 09:41:46.839000','2023-03-17 07:00:00.000000','2023-03-17 16:30:09.985000','Completed','2023-03-17 09:41:46.839000',3,0,'2023-03-17 16:30:09.999000',3,'1JKMD4N4',3,'',NULL,NULL,'','','',NULL,NULL),(57,NULL,'2023-03-17 21:45:38.156000','2023-03-17 21:45:38.088000',NULL,'Pending','2023-03-17 21:45:38.156000',10,0,'2023-03-17 21:45:38.156000',10,'1IZNE1B4',3,'',NULL,NULL,'','','',NULL,NULL),(58,'','2023-03-17 22:18:10.749000','2023-03-17 07:00:00.000000','2023-03-17 22:19:34.509000','Completed','2023-03-17 22:18:10.749000',8,0,'2023-03-24 18:38:10.871000',9,'61BWRC6X',3,'',NULL,NULL,'','','',NULL,NULL),(59,NULL,'2023-03-20 21:42:07.039000','2023-03-20 21:42:07.005000',NULL,'Pending','2023-03-20 21:42:07.039000',8,0,'2023-03-20 21:42:07.039000',8,'V5VBKQO8',8,'',NULL,NULL,'','','',NULL,NULL),(60,NULL,'2023-03-20 23:23:35.299000','2023-03-20 23:23:35.276000',NULL,'Pending','2023-03-20 23:23:35.299000',8,0,'2023-03-20 23:23:35.299000',8,'F9Y16P8Q',8,'',NULL,NULL,'','','',NULL,NULL),(61,'','2023-03-21 08:00:28.938000','2023-03-21 07:00:00.000000','2023-03-22 15:37:38.752000','Completed','2023-03-21 08:00:28.938000',8,0,'2023-03-22 15:37:38.758000',3,'A0P8KRQL',8,'',NULL,NULL,'','','',NULL,NULL),(62,NULL,'2023-03-21 09:17:22.853000','2023-03-21 09:17:22.836000',NULL,'Pending','2023-03-21 09:17:22.853000',4,0,'2023-03-21 09:17:22.853000',4,'JRIGBTBA',4,'',NULL,NULL,'','','',NULL,NULL),(63,'','2023-03-21 20:08:43.916000','2023-03-21 07:00:00.000000',NULL,'Pending','2023-03-21 20:08:43.916000',4,0,'2023-03-23 17:10:17.984000',3,'1A1W9KIS',4,'HN','','','Bac','Luong','0982584208','',''),(65,'','2023-03-21 20:38:47.750000','2023-03-21 07:00:00.000000','2023-03-22 14:53:22.558000','Completed','2023-03-21 20:38:47.750000',4,0,'2023-03-22 14:53:22.558000',3,'94X4KI7E',4,'',NULL,NULL,'','','',NULL,NULL),(71,'','2023-03-22 10:29:32.230000','2023-03-23 07:00:00.000000',NULL,'Pending','2023-03-22 10:29:32.230000',3,0,'2023-03-22 10:29:32.230000',3,'L4R57KV8',4,'',NULL,NULL,'','','',NULL,NULL),(76,'','2023-03-22 13:38:15.777000','2023-03-22 07:00:00.000000','2023-03-22 14:53:15.081000','Completed','2023-03-22 13:38:15.777000',4,0,'2023-03-22 14:53:15.082000',3,'4492S3TM',8,'',NULL,NULL,'','','',NULL,NULL),(77,'abc','2023-03-22 17:11:25.740000','2023-03-23 07:00:00.000000',NULL,'Pending','2023-03-22 17:11:25.740000',3,0,'2023-03-22 18:10:49.730000',3,'5B8Q31P9',12,'',NULL,NULL,'','','',NULL,NULL),(78,'','2023-03-22 17:16:00.561000','2023-03-23 07:00:00.000000','2023-03-23 08:19:07.636000','Completed','2023-03-22 17:16:00.561000',3,0,'2023-03-23 08:20:25.355000',3,'25V47JG0',13,'Sai Gon, Vietnam','','','Vac C','Le','1234567890','',''),(79,NULL,'2023-03-22 18:52:06.194000','2023-03-22 18:52:06.153000',NULL,'Pending','2023-03-22 18:52:06.194000',4,0,'2023-03-22 18:52:06.194000',4,'FFGJADVA',4,'HN','','','Bac','Luong','0982584208','',''),(80,'','2023-03-22 19:06:04.258000','2023-03-23 07:00:00.000000','2023-03-22 19:23:19.576000','Completed','2023-03-22 19:06:04.258000',3,0,'2023-03-22 19:23:19.577000',3,'WPTRGE2F',16,'Sai Gon, Vietnam','Sai Gon','Vietnam','Vac C','Le','0389374857','200000','Vietnam'),(81,'abc','2023-03-22 19:07:18.623000','2023-03-16 07:00:00.000000','2023-03-22 19:23:11.601000','Completed','2023-03-22 19:07:18.623000',3,0,'2023-03-22 19:23:11.602000',3,'5N28CBMF',4,'HN','HN','Vietnam','Bac','Luong','0123456789','200000','Vietnam'),(82,'abc','2023-03-23 08:41:23.958000','2023-03-24 07:00:00.000000','2023-03-23 08:43:48.628000','Completed','2023-03-23 08:41:23.958000',3,0,'2023-03-23 08:43:48.629000',3,'SS5YBHYZ',10,'HN','HN','Vietnam','Van D','Nguyen','0123456780','200000','Vietnam'),(83,'','2023-03-23 08:43:27.144000','2023-03-25 07:00:00.000000','2023-03-23 08:43:40.368000','Completed','2023-03-23 08:43:27.144000',3,0,'2023-03-23 08:43:40.373000',3,'2K85BBWJ',8,'Sai Gon, Vietnam','Sai Gon','Vietnam','Vac C','Le','0123456789','100000','Vietnam'),(85,NULL,'2023-03-23 17:39:11.744000','2023-03-23 17:39:11.741000',NULL,'Pending','2023-03-23 17:39:11.744000',17,0,'2023-03-23 17:39:11.744000',17,'SFNCVYB9',17,'Sai Gon, Vietnam','Sai Gon','Vietnam','Bac','Luong','0483659375','','Vietnam'),(86,'','2023-03-24 10:55:03.333000','2023-03-25 07:00:00.000000','2023-03-24 10:55:16.974000','Completed','2023-03-24 10:55:03.333000',3,0,'2023-03-24 10:55:16.978000',3,'MJYDYXO3',10,'HN','','','Van D','Nguyen','0123456780','',''),(87,'','2023-03-24 11:09:33.226000','2023-03-24 07:00:00.000000','2023-03-24 11:22:40.157000','Completed','2023-03-24 11:09:33.226000',15,0,'2023-03-24 11:22:40.158000',3,'G68S0Y54',15,'Da Nang','Da Nang','VN','A','Tran','0387494759','',''),(88,'','2023-03-24 15:08:18.782000','2023-03-25 07:00:00.000000',NULL,'Pending','2023-03-24 15:08:18.782000',3,0,'2023-03-24 18:25:58.626000',9,'WN793G31',10,'HN','','','Van D','Nguyen','0123456780','',''),(89,'','2023-03-24 18:16:30.553000','2023-03-24 07:00:00.000000',NULL,'Canceled','2023-03-24 18:16:30.553000',21,0,'2023-03-24 18:25:38.108000',9,'SC10KLTY',21,'Hue','Hue','Vietnam','M','Nguyen','0483746393','','Vietnam'),(90,NULL,'2023-03-24 21:03:04.573000','2023-03-24 21:03:04.534000',NULL,'Pending','2023-03-24 21:03:04.573000',4,0,'2023-03-24 21:03:04.573000',4,'HE9WO9R1',4,'Sai Gon, Vietnam','Sai Gon','Vietnam','Bac','Luong','0982584208','','Vietnam');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payments` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `ammount` decimal(10,2) NOT NULL,
  `check_number` varchar(50) NOT NULL,
  `payment_date` datetime(6) NOT NULL,
  `user_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_f4yt4cw24uc889fkfc6melvrf` (`check_number`),
  KEY `FKj94hgy9v5fw1munb90tar2eje` (`user_id`),
  CONSTRAINT `FKj94hgy9v5fw1munb90tar2eje` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,123.00,'2GDPYMFW','2023-02-13 23:07:06.855000',8),(2,12345.00,'iw3jk842','2023-02-14 05:59:34.389000',3),(3,1234.56,'5qu7qksh','2023-03-22 18:02:52.663000',10);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processor_brands`
--

DROP TABLE IF EXISTS `processor_brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `processor_brands` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(2500) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_qhtkrtkk3mdcg5u1n4qu7fefu` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processor_brands`
--

LOCK TABLES `processor_brands` WRITE;
/*!40000 ALTER TABLE `processor_brands` DISABLE KEYS */;
INSERT INTO `processor_brands` VALUES (1,'','Apple'),(2,'abc','Intel'),(5,'','AMD');
/*!40000 ALTER TABLE `processor_brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processor_collections`
--

DROP TABLE IF EXISTS `processor_collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `processor_collections` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(2500) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `processor_brand_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_992jn7p2pbjbox1e8wx3liadv` (`name`),
  KEY `FKd1jufriqtf0x9y4pfsglf68vs` (`processor_brand_id`),
  CONSTRAINT `FKd1jufriqtf0x9y4pfsglf68vs` FOREIGN KEY (`processor_brand_id`) REFERENCES `processor_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processor_collections`
--

LOCK TABLES `processor_collections` WRITE;
/*!40000 ALTER TABLE `processor_collections` DISABLE KEYS */;
INSERT INTO `processor_collections` VALUES (1,'','Core i7',2),(3,'','Core i5',2),(4,'','Xeon',2),(5,'','Core i3',2),(6,'','Core i9',2),(7,'','Ryzen 3',5),(8,'','Ryzen 5',5),(9,'','Ryzen 7',5),(10,'','Ryzen 9',5),(11,'','Threadripper',5);
/*!40000 ALTER TABLE `processor_collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processors`
--

DROP TABLE IF EXISTS `processors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `processors` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `base_frequency` decimal(10,2) DEFAULT NULL,
  `cache` tinyint DEFAULT NULL,
  `graphics_frequency` decimal(10,2) DEFAULT NULL,
  `max_turbo_frequency` decimal(10,2) DEFAULT NULL,
  `processor_graphics` varchar(100) DEFAULT NULL,
  `processor_number` varchar(50) NOT NULL,
  `total_cores` tinyint DEFAULT NULL,
  `total_threads` tinyint DEFAULT NULL,
  `processor_brand_id` bigint NOT NULL,
  `processor_collection_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_obe943fwnct5c0dbqopr5fwbw` (`processor_number`),
  KEY `FKoc2q92xbc9047aqlddkcttv0w` (`processor_brand_id`),
  KEY `FK4msgi9rhrbg1g6i2ijj0dih4d` (`processor_collection_id`),
  CONSTRAINT `FK4msgi9rhrbg1g6i2ijj0dih4d` FOREIGN KEY (`processor_collection_id`) REFERENCES `processor_collections` (`id`),
  CONSTRAINT `FKoc2q92xbc9047aqlddkcttv0w` FOREIGN KEY (`processor_brand_id`) REFERENCES `processor_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processors`
--

LOCK TABLES `processors` WRITE;
/*!40000 ALTER TABLE `processors` DISABLE KEYS */;
INSERT INTO `processors` VALUES (1,NULL,NULL,NULL,NULL,'','M1',NULL,NULL,1,NULL),(2,NULL,NULL,NULL,NULL,'Apple M2','M2',NULL,NULL,1,NULL),(3,3.70,12,1.20,4.70,'Intel UHD 630','8700K',6,12,2,1),(5,NULL,8,1.30,4.20,'Intel Iris X','1135G7',4,8,2,3),(6,4.90,18,1.45,4.90,'Intel Core UHD','W-11855M',6,12,2,4),(7,2.30,30,1.55,5.00,'Intel UHD','12950HX',16,24,2,6),(8,NULL,NULL,NULL,NULL,'M2 Max','M2 Max',12,NULL,1,NULL),(9,2.00,16,2.00,4.50,'AMD Radeon Graphics','5825U',8,16,5,9),(10,2.70,12,1.45,4.50,'Intel UHD','11400H',6,12,2,3),(11,2.30,24,1.45,4.60,'Intel UHD','11800H',8,16,2,1),(12,3.30,16,1.90,4.50,'AMD Radeon Graphics','6600H',6,12,5,8),(13,NULL,12,1.20,4.40,'Intel Iris X','1235U',NULL,12,2,3),(14,NULL,24,1.45,5.00,'Intel Iris X','12900HK',NULL,20,2,6),(15,1.70,12,NULL,4.40,'Intel Iris Xe','1240P',12,16,2,3);
/*!40000 ALTER TABLE `processors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_brand_photos`
--

DROP TABLE IF EXISTS `product_brand_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_brand_photos` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `product_brand_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_2ec8notip7hgyo487bfg4rn0e` (`name`),
  KEY `FK55fk0v7l436pdvq1cq4xigf5l` (`product_brand_id`),
  CONSTRAINT `FK55fk0v7l436pdvq1cq4xigf5l` FOREIGN KEY (`product_brand_id`) REFERENCES `product_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_brand_photos`
--

LOCK TABLES `product_brand_photos` WRITE;
/*!40000 ALTER TABLE `product_brand_photos` DISABLE KEYS */;
INSERT INTO `product_brand_photos` VALUES (1,'f150b79f-5141-4ad1-adc4-81bedec7ebcc.png',1),(2,'667d34fd-4739-486f-b644-ba539db18ec5.png',2),(4,'dd29ba65-cd6c-42ca-86a2-579741c2e895.png',4),(6,'61fca6a9-2180-40b6-8eec-0b4b4c663f6f.png',6);
/*!40000 ALTER TABLE `product_brand_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_brands`
--

DROP TABLE IF EXISTS `product_brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_brands` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(2500) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_fd0ruooxwhchwd11qa2rcipbv` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_brands`
--

LOCK TABLES `product_brands` WRITE;
/*!40000 ALTER TABLE `product_brands` DISABLE KEYS */;
INSERT INTO `product_brands` VALUES (1,'','Apple'),(2,'','Dell'),(4,'','HP'),(6,'','Lenovo');
/*!40000 ALTER TABLE `product_brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_lines`
--

DROP TABLE IF EXISTS `product_lines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_lines` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(2500) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `product_brand_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_3p630b63mbek153t2e23tpgv1` (`name`),
  KEY `FKqius5g57n8wrgsj3rdd4g030q` (`product_brand_id`),
  CONSTRAINT `FKqius5g57n8wrgsj3rdd4g030q` FOREIGN KEY (`product_brand_id`) REFERENCES `product_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_lines`
--

LOCK TABLES `product_lines` WRITE;
/*!40000 ALTER TABLE `product_lines` DISABLE KEYS */;
INSERT INTO `product_lines` VALUES (1,'','Macbook Air',1),(3,'','Vostro',2),(4,'','Macbook Pro 13',1),(5,'','Macbook Pro 14',1),(6,'','Macbook Pro 16',1),(7,'','XPS',2),(8,'','Latitude',2),(9,'','Precision',2),(10,'','Inspiron',2),(11,'','Alienware',2),(12,'','Zbook',4),(13,'','Elitebook',4),(14,'','Spectre',4),(15,'','Envy',4),(16,'','Probook',4),(17,'','Pavilion',4),(18,'','ThinkPad',6),(19,'','IdeaPad',6),(20,'','Yoga',6),(21,'','Legion',6),(22,'','ThinkBook',6),(23,'','Omen',4),(24,'','Victus',4);
/*!40000 ALTER TABLE `product_lines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_photos`
--

DROP TABLE IF EXISTS `product_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_photos` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `product_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_np5kad91alen7hop6iki4w10n` (`name`),
  KEY `FKk6euo1c1uosxm44vy24qbw05j` (`product_id`),
  CONSTRAINT `FKk6euo1c1uosxm44vy24qbw05j` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_photos`
--

LOCK TABLES `product_photos` WRITE;
/*!40000 ALTER TABLE `product_photos` DISABLE KEYS */;
INSERT INTO `product_photos` VALUES (1,'59424450-0e74-4060-a78b-3372f0a0a5f0.png',19),(2,'6795c3dd-61e1-4904-9f44-41f1d50f6a73.png',19),(3,'80e3e57c-7c8b-4210-bda1-9b951f9aad2d.png',19),(4,'c105cec3-1fa8-4dfb-8060-e947b1e15792.png',19),(5,'09e375ca-23f5-4cad-854b-72caa378925d.png',19),(6,'62d7f52f-eaee-4b8f-8e49-410a229454c2.png',19),(7,'ffa50214-5f83-45d7-9a16-8d129a095bc5.webp',30),(8,'d67d5995-3bb6-42f0-ac82-d47767b92c17.webp',30),(9,'d07c6e50-29ec-4876-b25f-452edfa91a9d.webp',30),(10,'a9e128e7-5073-40e8-bc7c-8572b6b25c2e.webp',30),(11,'9dc7f628-9e63-4568-aa41-93ce9e71e111.webp',30),(13,'de1da4a5-3ae6-4e29-92d8-9c6ed0e90daa.webp',29),(14,'48ab9134-2199-40e3-8187-76144e4667f3.webp',29),(15,'497d9158-7ce2-4a9b-8a79-d08f7a5a40df.webp',29),(16,'618a17f1-c35a-4a08-88a5-ad1cb90b42a2.webp',29),(17,'8538a421-12ff-41b4-8275-5a0d322e3b35.webp',29),(18,'0471a033-fa5d-48da-ad6e-40ef5be4ba0b.webp',29),(19,'5b3a93cc-7e9e-487e-a310-7b56ac64e405.webp',28),(20,'d0c31ece-459a-48e0-a589-b66856329f55.webp',28),(21,'4bed7fa6-359f-444e-a59c-a0e474f600b7.webp',28),(22,'fc500e83-d494-4504-b9bb-a921551d2c67.webp',28),(23,'c91c005b-e149-4603-81ab-92a3835576ec.webp',27),(24,'934a69b9-0c1a-400a-87ec-7c1e1f88ca28.webp',27),(25,'29b95c04-5474-49b7-954f-fd0468a866a9.webp',27),(26,'8b27be04-4a79-407c-a982-e58e1c8b1674.webp',27),(27,'f7e70158-8bc0-49bf-9c3b-68b138684b99.webp',22),(28,'8123c550-9888-4784-919a-f1f24361fb22.webp',22),(29,'813f49af-2a46-41bb-bc27-a65762189476.webp',22),(30,'8dcc448d-e4ef-4e37-9d74-04498b129318.webp',21),(31,'9c3e19e4-8537-4dfe-9c27-f875c39b6e50.webp',21),(32,'4794c7dc-24d7-4e8b-babf-065aa02eca32.webp',21),(33,'7ae74885-32d4-4b98-b747-b0005a8d44ad.webp',20),(34,'bf2e8a63-4f25-4215-b663-17afaf926cc7.webp',20),(35,'8a2a1b37-9612-4e0a-bed4-071b3a91bd35.webp',20),(36,'c45741e6-7b88-42c8-bd52-0d5e95abf00a.webp',18),(37,'d7da1f56-ad51-42ef-8bdd-19e425efe5e9.webp',18),(38,'af9f6f8f-b5e3-444b-a351-b480aa29c7c0.webp',18),(39,'110d64a8-8270-45f6-893e-4a085cbc4bb4.webp',18),(40,'fa2d06c9-660f-45b9-9db3-d51d0927811f.webp',17),(41,'f0945b9b-b079-4455-b979-8deb982f13c8.webp',17),(42,'b6f34248-8502-48fc-a6ba-740f67df72bb.webp',17),(43,'caf6ab7c-ffc2-4ca1-8d92-ecd1dd79f6b6.webp',10),(44,'b0865876-4b61-4439-b1a2-029f06c68035.webp',10),(45,'680810cf-e5be-4cb4-8f91-67ac23e84dac.webp',10),(46,'756713af-35dc-4284-b95b-0fab6b87c7ea.webp',26),(47,'c6e5200b-7d1d-40ea-9941-652320265d0d.webp',26),(48,'b7d55b3a-cc80-4e63-869f-99a0c7955f85.webp',26),(49,'f8ec8307-8239-42bd-90a8-c34edb6886bb.webp',26),(51,'8ebbf37e-a4b4-4e05-9fff-ce8ead3ef29a.webp',25),(52,'37950727-150a-4ff3-bae7-9df8b30014eb.webp',25),(53,'0eccd06b-bfd5-4dff-ab33-e3696280d632.webp',25),(54,'3801ac2f-d16f-4861-946f-81dabb0abcf8.webp',25),(56,'171192b1-e124-4815-bb7a-b2812c79f491.webp',24),(57,'eadbbf03-b17f-477f-be46-cbc72596a0a7.webp',24),(58,'a7332bd1-1142-4f67-88ca-b6b1afe03557.webp',24),(59,'09446339-5d56-4503-909e-1e8a4ee4aff0.webp',24),(60,'b09c8de0-5e82-4bbe-a2d5-876775195b77.webp',23),(61,'bce051e4-b4b6-4a7b-b327-84d3ac89ced8.webp',23),(62,'28c41cae-e431-4b6c-af50-4d50f90edd64.webp',23),(63,'f10c5fbc-10fa-4d7a-b022-f2f641c1135c.webp',23);
/*!40000 ALTER TABLE `product_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `updated_at` datetime(6) DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `backlit_keyboard` tinyint(1) DEFAULT '0',
  `battery_cells` tinyint DEFAULT NULL,
  `battery_life` tinyint DEFAULT NULL,
  `battery_type` varchar(50) NOT NULL,
  `bluetooth` varchar(50) DEFAULT NULL,
  `buy_price` decimal(10,2) NOT NULL,
  `color` varchar(50) NOT NULL,
  `display_type` varchar(50) DEFAULT NULL,
  `front_facing_camera` tinyint(1) DEFAULT '0',
  `headphone_jack` tinyint(1) DEFAULT '0',
  `number_of_memory_slots` tinyint DEFAULT NULL,
  `number_of_memory_sticks_included` tinyint DEFAULT NULL,
  `number_ofusbports` tinyint DEFAULT NULL,
  `numeric_keyboard` tinyint(1) DEFAULT '0',
  `operating_system` varchar(100) NOT NULL,
  `power_supply` int DEFAULT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_height` decimal(10,2) NOT NULL,
  `product_description` varchar(2500) DEFAULT NULL,
  `product_depth` decimal(10,2) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_weight` decimal(10,2) NOT NULL,
  `product_width` decimal(10,2) NOT NULL,
  `quantity_in_stock` bigint NOT NULL,
  `release_date` datetime(6) DEFAULT NULL,
  `screen_resolutionx` int NOT NULL,
  `screen_resolutiony` int NOT NULL,
  `screen_size` decimal(10,2) NOT NULL,
  `screen_type` varchar(50) DEFAULT NULL,
  `ssd_type` varchar(50) DEFAULT NULL,
  `storage_type` varchar(50) NOT NULL,
  `system_memory` int NOT NULL,
  `system_memory_speed` int DEFAULT NULL,
  `total_storage_capacity` int NOT NULL,
  `touch_screen` tinyint(1) NOT NULL DEFAULT '0',
  `type_of_memory` varchar(50) NOT NULL,
  `warranty` int DEFAULT NULL,
  `wifi` varchar(50) DEFAULT NULL,
  `graphics_id` bigint DEFAULT NULL,
  `processor_id` bigint NOT NULL,
  `product_brand_id` bigint NOT NULL,
  `product_line_id` bigint DEFAULT NULL,
  `discount_percentage` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_922x4t23nx64422orei4meb2y` (`product_code`),
  KEY `FKar4rtpnm25sy5k8m2gese85mg` (`graphics_id`),
  KEY `FKka6sj6v2cyo1d0iatb4od7w23` (`processor_id`),
  KEY `FKpvjh287e2rwiadduhvqv7rqwh` (`product_brand_id`),
  KEY `FK1eicg1yvaxh1gqdp2lsda7vlv` (`product_line_id`),
  CONSTRAINT `FK1eicg1yvaxh1gqdp2lsda7vlv` FOREIGN KEY (`product_line_id`) REFERENCES `product_lines` (`id`),
  CONSTRAINT `FKar4rtpnm25sy5k8m2gese85mg` FOREIGN KEY (`graphics_id`) REFERENCES `graphics` (`id`),
  CONSTRAINT `FKka6sj6v2cyo1d0iatb4od7w23` FOREIGN KEY (`processor_id`) REFERENCES `processors` (`id`),
  CONSTRAINT `FKpvjh287e2rwiadduhvqv7rqwh` FOREIGN KEY (`product_brand_id`) REFERENCES `product_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (10,'2023-02-09 18:36:14.664000',1,0,'2023-03-25 13:00:48.813000',3,0,NULL,NULL,'','',1150.00,'White','',0,0,NULL,NULL,NULL,0,'',NULL,'MLXW3SA',1.60,'',21.20,'M2 2022',1.50,30.40,122,'2023-02-01 07:00:00.000000',1920,1080,13.30,'','','SSD',8,NULL,256,0,'DDR4',NULL,'',NULL,2,1,1,NULL),(17,'2023-03-14 13:25:39.750000',3,0,'2023-03-25 13:00:16.500000',3,1,NULL,NULL,'Lithium-polymer','v5.0',870.00,'Gray','IPS',1,1,1,1,2,0,'macOS',NULL,'MGND3SA',0.41,'',21.24,'M1 2020',1.29,30.41,89,'2020-01-01 07:00:00.000000',2560,1600,13.30,'LED','NVMe','SSD',8,NULL,256,0,'DDR4',NULL,'802.11ax',NULL,1,1,1,25),(18,'2023-03-14 15:18:00.240000',3,0,'2023-03-25 12:59:56.532000',3,1,3,NULL,'Lithium-ion','v5.0',850.00,'Yellow','IPS',1,1,1,1,2,0,'Windows',65,'4U6P0PA',1.69,'',19.45,'13 ba1537TU',1.23,30.60,95,'2021-01-01 07:00:00.000000',1920,1080,13.30,'LED','NVMe','SSD',8,3200,256,0,'DDR4',NULL,'802.11ax',NULL,5,4,15,NULL),(19,'2023-03-14 22:48:41.380000',3,0,'2023-03-25 10:36:45.600000',3,1,6,NULL,'Lithium-ion','v5.0',9050.00,'Black','IPS',1,1,4,1,3,1,'Windows',NULL,'P9XK5',2.86,'',26.50,'7770',3.05,39.80,11,'2022-01-01 07:00:00.000000',3840,2160,17.30,'LED','NVMe','SSD',128,3600,4096,0,'DDR5',12,'802.11ax',5,7,2,9,NULL),(20,'2023-03-15 18:44:40.239000',3,0,'2023-03-25 12:59:31.757000',3,1,NULL,22,'Lithium-polymer','v5.0',3955.00,'Gray','OLED',1,1,NULL,NULL,3,0,'macOS',140,'MNWA3SA',1.68,'',24.81,'M2 Max 2023',2.16,35.57,42,'2023-01-01 07:00:00.000000',3456,2234,16.20,'LED','','SSD',32,NULL,1024,0,'DDR5',12,'802.11ax',NULL,8,1,6,NULL),(21,'2023-03-15 19:02:10.265000',3,0,'2023-03-25 12:58:37.421000',3,1,NULL,NULL,'','v5.1',900.00,'Black','IPS',1,1,2,1,3,0,'Windows',65,'21EB0063VN',1.79,'',22.07,'E14 G4 R7',1.64,32.40,93,'2022-01-01 07:00:00.000000',1920,1080,14.00,'LED','NVMe','SSD',8,3200,512,0,'DDR4',NULL,'802.11ax',NULL,9,6,18,NULL),(22,'2023-03-16 15:58:14.883000',3,0,'2023-03-25 12:58:01.340000',3,1,NULL,18,'Lithium-polymer','v5.0',2280.00,'Gray','OLED',1,1,1,1,2,0,'macOS',65,'MPHE3SA',1.55,'',22.12,'M2 Pro 2023',1.60,31.26,152,'2023-01-01 07:00:00.000000',3024,1964,14.20,'LED','','SSD',16,NULL,512,0,'DDR5',12,'802.11ax',NULL,2,1,5,NULL),(23,'2023-03-16 16:14:01.641000',3,0,'2023-03-25 13:28:13.772000',3,1,6,NULL,'Lithium-polymer','v5.0',1520.00,'Black','IPS',1,1,2,2,3,1,'Windows',200,'5Z9Q9PA',2.30,'',24.80,'16-b0178TX',2.30,36.92,47,'2023-01-01 07:00:00.000000',1920,1080,16.10,'LED','NVMe','SSD',16,2933,1024,0,'DDR4',12,'802.11ax',6,10,4,23,10),(24,'2023-03-16 16:28:48.979000',3,0,'2023-03-25 13:27:51.122000',3,1,6,NULL,'','v5.0',1735.00,'Black','IPS',1,1,2,2,3,1,'Windows',NULL,'5Z9Q7PA',2.30,'',24.80,'16-b0176TX',2.30,36.92,40,'2023-01-01 07:00:00.000000',1920,1080,16.10,'LED','NVMe','SSD',16,2933,1024,0,'DDR4',12,'802.11ax',7,11,4,23,10),(25,'2023-03-16 16:52:38.311000',3,0,'2023-03-25 13:28:29.253000',3,1,NULL,NULL,'','v5.3',890.00,'Black','IPS',1,1,2,1,2,1,'Windows',70,'7C140PA',2.35,'',26.00,'16-e1107AX',2.40,37.00,97,'2022-01-01 07:00:00.000000',1920,1080,16.10,'LED','NVMe','SSD',8,4800,512,0,'DDR5',12,'802.11ax',6,12,4,24,NULL),(26,'2023-03-16 17:23:03.401000',3,0,'2023-03-25 13:28:21.022000',3,1,NULL,NULL,'Lithium-polymer','v5.0',1130.00,'Black','IPS',1,1,2,2,3,1,'Windows',70,'5Z9R2PA',2.35,'',26.00,'16-d0291TX',2.46,37.00,120,'2021-01-01 07:00:00.000000',1920,1080,16.10,'LED','NVMe','SSD',8,3200,512,0,'DDR4',12,'802.11ax',6,11,4,24,NULL),(27,'2023-03-16 17:37:05.572000',3,0,'2023-03-25 12:54:53.933000',3,1,3,NULL,'','v5.2',860.00,'Black','IPS',1,1,2,1,4,0,'Windows',NULL,'6M0Y9PA',1.99,'',23.39,'450 G9',1.73,35.94,196,'2022-01-01 07:00:00.000000',1920,1080,15.60,'LED','NVMe','SSD',8,3200,512,0,'DDR4',12,'802.11ax',NULL,13,4,16,NULL),(28,'2023-03-17 19:09:02.247000',3,0,'2023-03-25 12:54:00.730000',3,1,6,NULL,'','v5.2',3385.00,'Gray','OLED',1,1,2,2,1,0,'Windows',130,'70295790',1.90,'',23.00,'9520',1.96,34.40,50,'2023-03-01 07:00:00.000000',3456,2160,15.60,'LED','NVMe','SSD',16,4800,512,1,'DDR5',12,'802.11ax',6,14,2,7,5),(29,'2023-03-17 19:33:17.624000',3,0,'2023-03-25 12:53:25.228000',3,0,3,NULL,'','v5.0',870.00,'Black','WVA',1,1,2,1,3,1,'Windows',65,'P112F002BBL',1.89,'',23.50,'3510',1.70,35.85,193,'2021-01-01 07:00:00.000000',1920,1080,15.60,'LED','NVMe','SSD',8,3200,512,0,'DDR4',12,'802.11ac',3,5,2,3,5),(30,'2023-03-17 19:51:17.479000',3,0,'2023-03-25 12:11:10.557000',3,1,3,NULL,'','v5.2',2040.00,'Silver','OLED',1,1,1,1,NULL,0,'Windows',60,'70295789',1.60,'',19.90,'9320',1.27,29.50,50,'2023-03-01 07:00:00.000000',3456,2234,13.40,'LED','NVMe','SSD',16,5200,512,1,'DDR5',12,'802.11ax',NULL,15,2,7,NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_SELLER'),(3,'ROLE_CUSTOMER');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `token` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `updated_at` datetime(6) DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `token` varchar(5000) DEFAULT NULL,
  `token_exp_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` VALUES (1,'2023-02-08 19:58:25.261000',1,1,'2023-02-08 19:58:25.261000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzY3MjUxMDUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.V23OOFkyEbnZzcKO8QO3Su2VTzxe1DwHHEgm1fVHnFs','2023-02-18 19:58:25.256000'),(2,'2023-02-11 06:25:09.035000',1,1,'2023-02-11 06:25:09.035000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzY5MzU1MDksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.gtA0Dq9fwFW1Mcm5O_iI050__Mtx81QeoO0pCXEvWj8','2023-02-21 06:25:09.028000'),(3,'2023-02-15 17:12:41.402000',1,1,'2023-02-15 17:12:41.402000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzczMTk5NjEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.-mB-We444lJ2CdnVU36Dm7SJb59rQABbYRPsk5eOd5g','2023-02-25 17:12:41.368000'),(4,'2023-02-15 18:16:51.168000',1,1,'2023-02-15 18:16:51.168000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzczMjM4MTEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.JNNVmjk3ij8SjW5p4UMRXod_pVxKUtRqlu7CnJhJNp8','2023-02-25 18:16:51.142000'),(5,'2023-02-15 21:11:14.178000',1,1,'2023-02-15 21:11:14.178000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzczMzQyNzQsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.YpkxVSGdAr_lqZI5FPsXcGuLU3nyHRpkxTmN1jY4244','2023-02-25 21:11:14.150000'),(6,'2023-02-15 21:13:01.746000',1,1,'2023-02-15 21:13:01.746000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzczMzQzODEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.T2EGyTA-v71XhqzM_0pgn1PSKUmwbI8s9SmSJNUQfkM','2023-02-25 21:13:01.744000'),(7,'2023-02-15 23:38:05.502000',1,1,'2023-02-15 23:38:05.502000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzczNDMwODUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.GOju90ADAha_SFGb7OqLknWQVX2F4YIkW49b_6ZrqQw','2023-02-25 23:38:05.478000'),(8,'2023-02-16 00:03:47.301000',1,1,'2023-02-16 00:03:47.301000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzczNDQ2MjcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9DVVNUT01FUiJdfX0.Dmo1N3nH8AHaDfQkZMJkyetnh0s1eLtZDkHuK96m5jQ','2023-02-26 00:03:47.300000'),(9,'2023-02-16 00:03:48.426000',1,1,'2023-02-16 00:03:48.426000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzczNDQ2MjgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9DVVNUT01FUiJdfX0.7oEKyjsot6VqDkSD4SJ6L6_WHrr0OTAOT7fI5MBII30','2023-02-26 00:03:48.426000'),(10,'2023-02-16 00:03:49.143000',1,1,'2023-02-16 00:03:49.143000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzczNDQ2MjksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9DVVNUT01FUiJdfX0.vI6UQPET5EJuUOnj3KgLSwrv-M7_mus_9V5VdpeheKw','2023-02-26 00:03:49.143000'),(11,'2023-02-16 00:04:32.532000',1,1,'2023-02-16 00:04:32.532000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzczNDQ2NzIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.SlVOfrYkI7EwwTYsGmwNr3hW3HykNLj_iEc71tJs0u8','2023-02-26 00:04:32.532000'),(12,'2023-02-16 00:20:31.078000',1,1,'2023-02-16 00:20:31.078000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzczNDU2MzEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.-m9hsRBFIy2-zmP9g08WtgSotZqSu5j0yuYrbuKB6U4','2023-02-26 00:20:31.051000'),(13,'2023-02-16 17:49:59.437000',1,1,'2023-02-16 17:49:59.437000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzc0MDg1OTksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.4dYo12EG8bm4cAjJnFmjL1JyRSa3VARcthBXigHyknM','2023-02-26 17:49:59.418000'),(14,'2023-02-16 19:37:37.155000',1,0,'2023-02-16 19:37:37.155000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzc0MTUwNTcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.01exwJdrOE71zsyf-euojGMzg3G_R3tIN2DnbuptDgE','2023-02-26 19:37:37.146000'),(15,'2023-02-17 09:21:51.958000',1,0,'2023-02-17 09:21:51.958000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzc0NjQ1MTEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.p5YfIqW_u4VuydChUJasl0CVIA3IYjBpJn4-p0FJg0g','2023-02-27 09:21:51.926000'),(16,'2023-02-22 20:15:08.454000',1,0,'2023-02-22 20:15:08.454000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzc5MzU3MDgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.09A14wl78p7r24LREl0qhR0u8ClWalRswF2tvlkO6Ew','2023-03-04 20:15:08.428000'),(17,'2023-02-22 20:16:13.532000',1,0,'2023-02-22 20:16:13.532000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzc5MzU3NzMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.QyL0nT1q0OGT04LRQkdCd-H9AWja1mZAXBQqq0Z2neY','2023-03-04 20:16:13.531000'),(18,'2023-02-22 20:30:24.311000',1,0,'2023-02-22 20:30:24.311000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzc5MzY2MjQsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.MEBI_SugSFRUseXkjDORcQvS0gJlHtmYVPRK4cupSdI','2023-03-04 20:30:24.310000'),(19,'2023-02-22 20:35:20.030000',1,0,'2023-02-22 20:35:20.030000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzc5MzY5MjAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMFpDS3d0VUl0Q3Qwa1NUMm16RUpvLjRFRXdxXC84XC9tXC8weU4wbGtvYXFrY0pkNDFjdDdiWXkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEsImVtYWlsIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdfX0.WwC-8rxdJYnkAaZg-V1wV304N3-U0mNhDNHAyoP0GeM','2023-03-04 20:35:20.030000'),(20,'2023-02-23 13:06:25.828000',3,1,'2023-02-23 13:06:25.828000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzc5OTYzODUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.vO_c_GVfW9LanBP4nWNe5ieOxGdbiA5YoL-adGWQWNc','2023-03-05 13:06:25.823000'),(21,'2023-02-23 17:10:09.703000',4,1,'2023-02-23 17:10:09.703000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzgwMTEwMDksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkYkw0dE5jXC9mQWhaMnRLS0hOOVZaYWVlZnZrcmd0WVhmOVU4LmVPTEJKZ1M0ODk2M1VTU1hTIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDk4MjU4NDIwOCJ9fQ.QfHQRY0IWvHwOa208ndayGBEUdg0tl7--o-5Xg_zifE','2023-03-05 17:10:09.698000'),(22,'2023-02-23 18:13:41.241000',4,1,'2023-02-23 18:13:41.241000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzgwMTQ4MjEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkYkw0dE5jXC9mQWhaMnRLS0hOOVZaYWVlZnZrcmd0WVhmOVU4LmVPTEJKZ1M0ODk2M1VTU1hTIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDk4MjU4NDIwOCJ9fQ.uJEWoXc9jgi-onNvLWwBRIc0ZTz1XpMha98rbjD0f-A','2023-03-05 18:13:41.240000'),(23,'2023-02-24 15:16:20.181000',3,1,'2023-02-24 15:16:20.181000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzgwOTA1ODAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.kE_mxa9NtCIpam_gsYZqHh41AzmJGO0lYAgpQtLWENM','2023-03-06 15:16:20.160000'),(24,'2023-02-24 16:35:25.381000',4,1,'2023-02-24 16:35:25.381000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzgwOTUzMjUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkYkw0dE5jXC9mQWhaMnRLS0hOOVZaYWVlZnZrcmd0WVhmOVU4LmVPTEJKZ1M0ODk2M1VTU1hTIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDk4MjU4NDIwOCJ9fQ.IlB56Ru-7fSparWog_BHh7WyYkUDXDtb10gRKY6naGQ','2023-03-06 16:35:25.345000'),(25,'2023-02-24 16:44:22.680000',4,1,'2023-02-24 16:44:22.680000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzgwOTU4NjIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkYkw0dE5jXC9mQWhaMnRLS0hOOVZaYWVlZnZrcmd0WVhmOVU4LmVPTEJKZ1M0ODk2M1VTU1hTIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDk4MjU4NDIwOCJ9fQ.Qt7kLrl2F6_lkd_Pi2QxpWgTQVGWsPlzm_3f8ZsQwQM','2023-03-06 16:44:22.679000'),(26,'2023-02-24 18:33:31.249000',3,1,'2023-02-24 18:33:31.249000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzgxMDI0MTEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.ytC-Ctc6IPsnPeySXk4v-JmBgfBuWxjKwLUucF4VupU','2023-03-06 18:33:31.230000'),(27,'2023-02-24 19:51:06.574000',4,1,'2023-02-24 19:51:06.574000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzgxMDcwNjYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkYkw0dE5jXC9mQWhaMnRLS0hOOVZaYWVlZnZrcmd0WVhmOVU4LmVPTEJKZ1M0ODk2M1VTU1hTIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDk4MjU4NDIwOCJ9fQ.0Fl2GXHeEMda1WEp4CT8JYYperoyKTeA8q_yzmV3ffk','2023-03-06 19:51:06.554000'),(28,'2023-02-24 19:54:05.865000',3,1,'2023-02-24 19:54:05.865000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzgxMDcyNDUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.UJ7rzWf_8ORx3hM28gnB7q615TP2DKtJaSGP9bjrmbw','2023-03-06 19:54:05.864000'),(29,'2023-02-25 10:17:58.166000',4,1,'2023-02-25 10:17:58.166000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzgxNTkwNzgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkYkw0dE5jXC9mQWhaMnRLS0hOOVZaYWVlZnZrcmd0WVhmOVU4LmVPTEJKZ1M0ODk2M1VTU1hTIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDk4MjU4NDIwOCJ9fQ.4jbRqxSjMexEQSE2xNcn_7n5HiqsDQStsDvzd4vIsOI','2023-03-07 10:17:58.135000'),(30,'2023-02-26 13:52:54.253000',4,1,'2023-02-26 13:52:54.253000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzgyNTgzNzQsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkYkw0dE5jXC9mQWhaMnRLS0hOOVZaYWVlZnZrcmd0WVhmOVU4LmVPTEJKZ1M0ODk2M1VTU1hTIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDk4MjU4NDIwOCJ9fQ.5IRKwOx0RUKGv9cK9kQpMJNlfSg_q1ne-CUYQYbl3K8','2023-03-08 13:52:54.233000'),(31,'2023-02-26 15:30:23.423000',4,1,'2023-02-26 15:30:23.423000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzgyNjQyMjMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkYkw0dE5jXC9mQWhaMnRLS0hOOVZaYWVlZnZrcmd0WVhmOVU4LmVPTEJKZ1M0ODk2M1VTU1hTIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDk4MjU4NDIwOCJ9fQ.W9E3kow8JzSm0QrSjTeLcxn4Jlv8SqQfVr9o1PaP-dQ','2023-03-08 15:30:23.422000'),(32,'2023-02-27 18:05:59.058000',3,1,'2023-02-27 18:05:59.058000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzgzNTk5NTksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.TjqtiQydB_3CFXtykc11sd09vifpU_AmpdwIphsU-k8','2023-03-09 18:05:59.023000'),(33,'2023-02-28 17:48:15.606000',3,1,'2023-02-28 17:48:15.606000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzg0NDUyOTUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.NHEZ40CQppWDPa_VN1NVk9EX0YwkcYGbH7hjg027QOQ','2023-03-10 17:48:15.576000'),(34,'2023-03-03 19:47:07.777000',4,1,'2023-03-03 19:47:07.777000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzg3MTE2MjcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkYkw0dE5jXC9mQWhaMnRLS0hOOVZaYWVlZnZrcmd0WVhmOVU4LmVPTEJKZ1M0ODk2M1VTU1hTIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDk4MjU4NDIwOCJ9fQ.JJaFzyNuJypuOvCo8cHeaDU_vqtIntS4LSsqH1W_ZUI','2023-03-13 19:47:07.755000'),(35,'2023-03-03 20:12:27.496000',4,1,'2023-03-03 20:12:27.496000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzg3MTMxNDcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkYkw0dE5jXC9mQWhaMnRLS0hOOVZaYWVlZnZrcmd0WVhmOVU4LmVPTEJKZ1M0ODk2M1VTU1hTIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDk4MjU4NDIwOCJ9fQ.O4iSSAhrgULSmekKSpHwv9bbCt5Pwg-BmtIcMvqEIBs','2023-03-13 20:12:27.495000'),(36,'2023-03-03 20:13:18.677000',4,1,'2023-03-03 20:13:18.677000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzg3MTMxOTgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkOXg0c1gwWmFWcFpPeE9zazM0TjRpLkRIbUNhT3lTZjhEbzJiT29tXC90UjhMYlwvT21mTlJTRyIsImRlbGV0ZWQiOmZhbHNlLCJpZCI6NCwiYXV0aG9yaXRpZXMiOlsiUk9MRV9DVVNUT01FUiJdLCJ1c2VybmFtZSI6IjA5ODI1ODQyMDgifX0.OyQ8uA5yiIRztEF31Os0Zu1ZJxf_NdPif-zfYLFXRQo','2023-03-13 20:13:18.676000'),(37,'2023-03-03 20:17:18.642000',4,1,'2023-03-03 20:17:18.642000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzg3MTM0MzgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkczcxQlNJOTY1OWJCMVUyZVo0MHl4T0NxNkwuYWNDQlFyWGRVMHFybVBsakt6ZjJNdHJNZXUiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4In19.BmtRh0JS_9wC0utgC6rTkFonlHfEiBDyMmqzDR8kRoQ','2023-03-13 20:17:18.641000'),(38,'2023-03-03 20:22:40.153000',4,1,'2023-03-03 20:22:40.153000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzg3MTM3NjAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkczcxQlNJOTY1OWJCMVUyZVo0MHl4T0NxNkwuYWNDQlFyWGRVMHFybVBsakt6ZjJNdHJNZXUiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4In19.yL_m1hAM7sYJXkSTPUGfxec-zCKW_3vVp_FuD3ZOo6U','2023-03-13 20:22:40.153000'),(39,'2023-03-03 20:24:28.068000',4,1,'2023-03-03 20:24:28.068000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzg3MTM4NjgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkczcxQlNJOTY1OWJCMVUyZVo0MHl4T0NxNkwuYWNDQlFyWGRVMHFybVBsakt6ZjJNdHJNZXUiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4In19.ES7vD6c-9AuaGFUehVJ9V7TfY5XktHWSmiebyFvAFJ4','2023-03-13 20:24:28.067000'),(40,'2023-03-03 20:24:59.059000',4,1,'2023-03-03 20:24:59.059000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzg3MTM4OTksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTHV6RE9pLlBNZzl6QlBFUzd3MmlyZTJpbkhwNnVNUFF1eVVYaW1BYTZJN1E4OEZwcHd2aDYiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4In19.qbz3IJgvoxyxeBc-alh76omH6bySiRhyYxgJ5aV4cIc','2023-03-13 20:24:59.058000'),(41,'2023-03-03 20:25:49.082000',4,1,'2023-03-03 20:25:49.082000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzg3MTM5NDksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTHV6RE9pLlBNZzl6QlBFUzd3MmlyZTJpbkhwNnVNUFF1eVVYaW1BYTZJN1E4OEZwcHd2aDYiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4In19.mkEsmMIBZzKVpX_vebEiM9KQwCQJsHrYZrmnhWx3Rb4','2023-03-13 20:25:49.082000'),(42,'2023-03-04 13:07:34.759000',4,1,'2023-03-04 13:07:34.759000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzg3NzQwNTQsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTHV6RE9pLlBNZzl6QlBFUzd3MmlyZTJpbkhwNnVNUFF1eVVYaW1BYTZJN1E4OEZwcHd2aDYiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4In19.OU5pWhgzyWVhv7Pz4waDBQa1CFbPNhfdx8YQlM0pngA','2023-03-14 13:07:34.735000'),(43,'2023-03-05 10:21:37.299000',3,1,'2023-03-05 10:21:37.299000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzg4NTA0OTcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.kDK0B36NIC8FaGBH4fWoiFestmR8o0kOQ3HqjGwraeY','2023-03-15 10:21:37.276000'),(44,'2023-03-06 10:36:47.781000',4,1,'2023-03-06 10:36:47.781000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzg5Mzc4MDcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTHV6RE9pLlBNZzl6QlBFUzd3MmlyZTJpbkhwNnVNUFF1eVVYaW1BYTZJN1E4OEZwcHd2aDYiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4In19.5ZP7c0dfgTBXVg-NEI8SuL6Uz1ZD1JqwcoSmpLZIexM','2023-03-16 10:36:47.753000'),(45,'2023-03-07 05:56:38.072000',3,1,'2023-03-07 05:56:38.072000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzkwMDczOTgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.NaK6_xZJl1zAoV2IP6FLXHV5zO0s_c5Sp3d10Kf6DQA','2023-03-17 05:56:38.067000'),(46,'2023-03-07 15:10:39.394000',3,1,'2023-03-07 15:10:39.394000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzkwNDA2MzksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.aabKjKIMQqB32x6Re--5row_77BYjvyeGxM8fJoFO5A','2023-03-17 15:10:39.353000'),(47,'2023-03-07 15:14:38.832000',3,1,'2023-03-07 15:14:38.832000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzkwNDA4NzgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.YIBBPSg8pyrDk4U5CuOr9sRTuUqBHJUXVmgznTtIkuE','2023-03-17 15:14:38.831000'),(48,'2023-03-09 06:15:17.051000',5,0,'2023-03-09 06:15:17.051000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzkxODEzMTcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkQWw5NHBSTWFLUGIweUNVdFRLUnA2LnJaZW54cGxBaDdmdjBjQWI3cXh1TFlRNzc5MjNvUmUiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjUsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwMTIzNDU2Nzg5In19.mwCVISSXfHn3U5Ut163T2cIzPGjbCe00trOQX9slZEs','2023-03-19 06:15:17.047000'),(49,'2023-03-09 06:42:46.036000',5,0,'2023-03-09 06:42:46.036000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzkxODI5NjYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkQWw5NHBSTWFLUGIweUNVdFRLUnA2LnJaZW54cGxBaDdmdjBjQWI3cXh1TFlRNzc5MjNvUmUiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjUsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwMTIzNDU2Nzg5In19.yKt-re45BKxVcA-1PZAs9d9lmtksA0sybRyPwESEMx8','2023-03-19 06:42:46.035000'),(50,'2023-03-09 06:54:38.837000',8,1,'2023-03-09 06:54:38.837000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzkxODM2NzgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkbW43SUZWVlJJM09Sam14RUtPVXp4LkVPRS44cXI0ejFrMnNlbU1wd244eTNqM0hHSDdtTUMiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjgsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwMTIzNDU2Nzg5In19.7IGed1MEmP7qA7uQm0AGJAKWgc3gcTk34G7YrFBMxTc','2023-03-19 06:54:38.832000'),(51,'2023-03-09 11:41:57.381000',3,1,'2023-03-09 11:41:57.381000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzkyMDA5MTcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.bTy52C3yibdPOOJmO07wv6zQhnMRbL1abs2y-Z8Fk1w','2023-03-19 11:41:57.374000'),(52,'2023-03-09 17:07:41.910000',3,1,'2023-03-09 17:07:41.910000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzkyMjA0NjEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.suUuIjpdAX-FkI9zv3nzeg1YsmuazpTe-4EWjtn7s9w','2023-03-19 17:07:41.883000'),(53,'2023-03-09 18:47:40.631000',3,1,'2023-03-09 18:47:40.631000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzkyMjY0NjAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.OsU9keTbqNiA5MnjPYzsTLYVBpL3NAr1nQPdwILV4Sw','2023-03-19 18:47:40.601000'),(54,'2023-03-12 12:50:17.140000',3,1,'2023-03-12 12:50:17.140000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk0NjQyMTcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.O67MERT5k8yIfGlTJ29JwKLAwgvLlL0m3SFSvees-ag','2023-03-22 12:50:17.134000'),(55,'2023-03-13 14:39:12.615000',3,1,'2023-03-13 14:39:12.615000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk1NTcxNTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkaUdHVVhiT2F6V0lrVWpLTVB4WkZJdUxiYVVHMjVBUGY5ZVJQejRLbVwvMXFkVTNhWDdBelB1IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjozLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sInVzZXJuYW1lIjoibHVvbmd2YW5iYWNhYmN4eXpAZ21haWwuY29tIn19.i0psEyiqlN_UbWf7-sgmUFN8WI9SiLmbnt8xuBXCxxw','2023-03-23 14:39:12.589000'),(56,'2023-03-13 14:49:21.448000',3,0,'2023-03-13 14:49:21.448000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk1NTc3NjEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.GENn_hc2l0UdrQh6D_N7nh1O2s4bhGUOuvcydy5QNU4','2023-03-23 14:49:21.447000'),(57,'2023-03-13 15:18:30.414000',9,1,'2023-03-13 15:18:30.414000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk1NTk1MTAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMk05d2JzODZXMnd4RDFRdGJJaGFDTzlwUnlIRUtJSjQ4S1RcL0NuMWlnODZSOHBXSVpGUHY2IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo5LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoidmFuY0BnbWFpbC5jb20ifX0.jksi8iZ65EkPhRUAsTGZwfU-WWeVs10WWyeX4ISqIco','2023-03-23 15:18:30.414000'),(58,'2023-03-13 15:18:31.612000',9,1,'2023-03-13 15:18:31.612000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk1NTk1MTEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMk05d2JzODZXMnd4RDFRdGJJaGFDTzlwUnlIRUtJSjQ4S1RcL0NuMWlnODZSOHBXSVpGUHY2IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo5LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoidmFuY0BnbWFpbC5jb20ifX0.og1ceP31pAGdRZAFvZDlqbgfQNjC7bDK05lU6cfT1FM','2023-03-23 15:18:31.612000'),(59,'2023-03-13 15:18:34.292000',9,1,'2023-03-13 15:18:34.292000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk1NTk1MTQsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMk05d2JzODZXMnd4RDFRdGJJaGFDTzlwUnlIRUtJSjQ4S1RcL0NuMWlnODZSOHBXSVpGUHY2IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo5LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoidmFuY0BnbWFpbC5jb20ifX0.4IuQe1HxWghcSKKFy9LLsv2VUh0MsuUH38DxUfoFYVM','2023-03-23 15:18:34.291000'),(60,'2023-03-13 15:18:44.152000',3,0,'2023-03-13 15:18:44.152000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk1NTk1MjQsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.oUg0tOVOuZgqMV5wexPBxfp6eXGmqR5GYJOOENcxb6w','2023-03-23 15:18:44.152000'),(61,'2023-03-13 16:03:52.576000',4,1,'2023-03-13 16:03:52.576000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk1NjIyMzIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTHV6RE9pLlBNZzl6QlBFUzd3MmlyZTJpbkhwNnVNUFF1eVVYaW1BYTZJN1E4OEZwcHd2aDYiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfU0VMTEVSIl0sInVzZXJuYW1lIjoiMDk4MjU4NDIwOCJ9fQ.5JGURQ-MxUklTxeiOgnA1DNoLU0coljYbYizqC4NGFI','2023-03-23 16:03:52.555000'),(62,'2023-03-13 16:05:16.861000',4,1,'2023-03-13 16:05:16.861000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk1NjIzMTYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTHV6RE9pLlBNZzl6QlBFUzd3MmlyZTJpbkhwNnVNUFF1eVVYaW1BYTZJN1E4OEZwcHd2aDYiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfU0VMTEVSIl0sInVzZXJuYW1lIjoiMDk4MjU4NDIwOCJ9fQ.8hg_4SvVpJkc5O2TXXwj7GGuu43n1zTW6iEHRyUiPjo','2023-03-23 16:05:16.860000'),(63,'2023-03-13 20:29:22.305000',3,0,'2023-03-13 20:29:22.305000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk1NzgxNjIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.IfcICifQZ3pyLTQ4V4YBdD7rpOAEQy8Va65YWZ3Kf2s','2023-03-23 20:29:22.299000'),(64,'2023-03-14 08:44:03.380000',3,0,'2023-03-14 08:44:03.380000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk2MjIyNDMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0._KsCaiJbWrDyiuEkCH7iajgL2_44TPVuwCf1wmzqCd4','2023-03-24 08:44:03.344000'),(65,'2023-03-15 21:36:11.134000',9,1,'2023-03-15 21:36:11.134000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk3NTQ5NzEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMk05d2JzODZXMnd4RDFRdGJJaGFDTzlwUnlIRUtJSjQ4S1RcL0NuMWlnODZSOHBXSVpGUHY2IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo5LCJhdXRob3JpdGllcyI6WyJST0xFX1NFTExFUiJdLCJ1c2VybmFtZSI6InZhbmNAZ21haWwuY29tIn19.ORzkCVzB5JvhAAz53VO7dHMClQlA9FOCy2Fyz6hwNWg','2023-03-25 21:36:11.129000'),(66,'2023-03-16 08:20:50.235000',3,0,'2023-03-16 08:20:50.235000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk3OTM2NTAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.619vEZFPT4tFPjLbJc9B6DUJYGxD03Rkdt9W9zn2k8c','2023-03-26 08:20:50.213000'),(67,'2023-03-16 10:38:58.022000',3,0,'2023-03-16 10:38:58.022000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk4MDE5MzgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.PyImRsLXcyDTj5w_s6calTvebRQb_V-hmX1VG2prVcU','2023-03-26 10:38:58.021000'),(68,'2023-03-17 15:23:35.968000',3,0,'2023-03-17 15:23:35.968000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk5MDU0MTUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.fAetu5FY8P0s57ZtuDaxjBh5AOr3cTZY1jLy5z-Y2NY','2023-03-27 15:23:35.950000'),(69,'2023-03-17 16:29:11.578000',3,0,'2023-03-17 16:29:11.578000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk5MDkzNTEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.fiPp5aow155iMyWY3WQBoe914vKkPgXj2hOS-Jx37iU','2023-03-27 16:29:11.577000'),(70,'2023-03-17 16:38:11.518000',8,0,'2023-03-17 16:38:11.518000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk5MDk4OTEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkbW43SUZWVlJJM09Sam14RUtPVXp4LkVPRS44cXI0ejFrMnNlbU1wd244eTNqM0hHSDdtTUMiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjgsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwMTIzNDU2Nzg5In19.dMuSHz7-_6XKY9WEwEfq6CFzavcmu7tUJ9D3KpO9dS8','2023-03-27 16:38:11.517000'),(71,'2023-03-17 21:43:32.801000',10,1,'2023-03-17 21:43:32.801000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk5MjgyMTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkeXhPT0pMNHdtdktJcVc5dHJIXC9TMS5WXC92cVJMZ3lYdW0ubWNteXlzZnNycEtuMVdcLzZNMmUiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjEwLCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDEyMzQ1Njc4MCJ9fQ.ZLyIgSVeKmYjqj6LK70AU-ODsIgYtxP5tepm89EEboA','2023-03-27 21:43:32.795000'),(72,'2023-03-17 21:46:51.283000',3,0,'2023-03-17 21:46:51.283000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk5Mjg0MTEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.A_e9Jt3aAqF9ZKsL31DcDPz0QnO_2zwL9xJ3p9aVs_s','2023-03-27 21:46:51.282000'),(73,'2023-03-17 21:47:23.183000',9,1,'2023-03-17 21:47:23.183000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk5Mjg0NDMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMk05d2JzODZXMnd4RDFRdGJJaGFDTzlwUnlIRUtJSjQ4S1RcL0NuMWlnODZSOHBXSVpGUHY2IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo5LCJhdXRob3JpdGllcyI6WyJST0xFX1NFTExFUiJdLCJ1c2VybmFtZSI6InZhbmNAZ21haWwuY29tIn19.eGR7hpRhYshravKcdVpL96QXjCJAqKgWzlZw9AWn5HM','2023-03-27 21:47:23.182000'),(74,'2023-03-17 22:17:48.380000',8,0,'2023-03-17 22:17:48.380000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Nzk5MzAyNjgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkbW43SUZWVlJJM09Sam14RUtPVXp4LkVPRS44cXI0ejFrMnNlbU1wd244eTNqM0hHSDdtTUMiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjgsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwMTIzNDU2Nzg5In19.TdWxLaKuGa2Vxf5XbCLj8WcdDMocs_tByX66lpCJ-2E','2023-03-27 22:17:48.379000'),(75,'2023-03-20 22:58:25.728000',3,0,'2023-03-20 22:58:25.728000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAxOTE5MDUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.ovZYVUB4HA5EiweWyEr3GWNbv-3Ikgv68Nj-p8rkQwM','2023-03-30 22:58:25.689000'),(76,'2023-03-20 23:21:18.348000',8,0,'2023-03-20 23:21:18.348000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAxOTMyNzgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkbW43SUZWVlJJM09Sam14RUtPVXp4LkVPRS44cXI0ejFrMnNlbU1wd244eTNqM0hHSDdtTUMiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjgsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwMTIzNDU2Nzg5In19.cJd83n5jw_rEpwohbrZTjsibsljrw_V4HMYdgnxsieg','2023-03-30 23:21:18.289000'),(77,'2023-03-21 07:48:04.853000',3,0,'2023-03-21 07:48:04.853000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAyMjM2ODQsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.yUjpIO8TM8OHqt751fdxa36ZJRiw4PXzuot4z_2xWAk','2023-03-31 07:48:04.832000'),(78,'2023-03-21 08:11:37.678000',3,0,'2023-03-21 08:11:37.678000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAyMjUwOTcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.EbWP9BPG6p9OYfUmwWVFDhhwB1EhkhWhQLD32VW4xmc','2023-03-31 08:11:37.673000'),(79,'2023-03-21 08:57:19.614000',3,0,'2023-03-21 08:57:19.614000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAyMjc4MzksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.W0kA8_dm8nktYYRUSt6zCrRjRDu-TBqBz1O-jyz-uIw','2023-03-31 08:57:19.613000'),(80,'2023-03-21 09:16:48.096000',4,1,'2023-03-21 09:16:48.096000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAyMjkwMDgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTHV6RE9pLlBNZzl6QlBFUzd3MmlyZTJpbkhwNnVNUFF1eVVYaW1BYTZJN1E4OEZwcHd2aDYiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4In19.JpNyedqA1zjSlyLQxEF2jrXWowLb5f6UJjRuyPKGw9M','2023-03-31 09:16:48.072000'),(81,'2023-03-21 17:19:57.738000',3,0,'2023-03-21 17:19:57.738000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAyNTc5OTcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.f60MGfRXd9l9AqHQBPLwL38KKdIKGst19oDadr6eW4s','2023-03-31 17:19:57.713000'),(82,'2023-03-22 17:13:27.508000',12,0,'2023-03-22 17:13:27.508000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAzNDQwMDcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMmZTYXpnLnBBclwvRUFOLmNwclRPaS5pYkZGYWxEZ2hcL29yTVhMR3pnWDVTR2tSdklJQ1RtdSIsImRlbGV0ZWQiOmZhbHNlLCJpZCI6MTIsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwODI5NDc0OTMyIn19.YCEC7AAnORwR2ooXaLR5vVKyQ3fLMDmX06Ea9QS5eAo','2023-04-01 17:13:27.501000'),(83,'2023-03-22 17:18:23.438000',13,0,'2023-03-22 17:18:23.438000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAzNDQzMDMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkVVd2QnJ6QUcuVDNwalBSMy5uZzRndXNGaXdLbXZYTFR6bURUU1pGMkl1TzEuM2FcL1NObzZtIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjoxMywiYXV0aG9yaXRpZXMiOlsiUk9MRV9DVVNUT01FUiJdLCJ1c2VybmFtZSI6IjA5Mzg0NzM5NDg1In19.AOHi3qZByWPsDdFTRQQmEQXAlFnd9T1csij3CrUoxpM','2023-04-01 17:18:23.436000'),(84,'2023-03-22 17:36:30.836000',14,0,'2023-03-22 17:36:30.836000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAzNDUzOTAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkQW9sd1Q1dm1RYkFaN1ltN1picnl2dVFIbUNUcldDaHhiM2dObmRMczBqbjh6NkdaSGdBREMiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjE0LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDM4NDkzNzQ4MzcifX0.jadVPybloqe4eiGP-qEsNAU8vPZMhbNZlHLYprO1QgQ','2023-04-01 17:36:30.829000'),(85,'2023-03-22 17:37:38.750000',15,0,'2023-03-22 17:37:38.750000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAzNDU0NTgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWU45cEhGNmRvbUJaeHN5cjQ3c2h1T01rY3dlZFNSbEszR1pcL3BuUmU1d09Iby44OFB0bEJxIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjoxNSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9DVVNUT01FUiJdLCJ1c2VybmFtZSI6IjAzODc0OTQ3NTkifX0.fP7JvEtXlO-wab1HYGc98SzxG24xf4opqIcn9E8FEoc','2023-04-01 17:37:38.749000'),(86,'2023-03-22 18:39:00.249000',3,0,'2023-03-22 18:39:00.249000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAzNDkxNDAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.HNvNBW70uA9lIAS84EuU7Yy8PZp6bLCWPTv_gNLHWvA','2023-04-01 18:39:00.219000'),(87,'2023-03-22 18:40:06.395000',4,1,'2023-03-22 18:40:06.395000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODAzNDkyMDYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTHV6RE9pLlBNZzl6QlBFUzd3MmlyZTJpbkhwNnVNUFF1eVVYaW1BYTZJN1E4OEZwcHd2aDYiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4In19.wxzMfEDdoMQqMZrofunkKiBLglt9w4oAKAxIJVR_LJA','2023-04-01 18:40:06.395000'),(88,'2023-03-23 08:52:15.338000',9,0,'2023-03-23 08:52:15.338000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA0MDAzMzUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMk05d2JzODZXMnd4RDFRdGJJaGFDTzlwUnlIRUtJSjQ4S1RcL0NuMWlnODZSOHBXSVpGUHY2IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo5LCJhdXRob3JpdGllcyI6WyJST0xFX1NFTExFUiJdLCJ1c2VybmFtZSI6InZhbmNAZ21haWwuY29tIn19.8Zk0XrdIEX8FNY1TEoAyyB16OX8I6fYE8RDN3vmwNr0','2023-04-02 08:52:15.332000'),(89,'2023-03-23 09:53:12.735000',3,0,'2023-03-23 09:53:12.735000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA0MDM5OTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20ifX0.5bXy8PXEfm8V8Eu3qFanoDlv1je2zjaogubPO7E4xXc','2023-04-02 09:53:12.734000'),(90,'2023-03-23 16:25:58.746000',4,1,'2023-03-23 16:25:58.746000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA0Mjc1NTgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTHV6RE9pLlBNZzl6QlBFUzd3MmlyZTJpbkhwNnVNUFF1eVVYaW1BYTZJN1E4OEZwcHd2aDYiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4IiwiYWN0aXZhdGVkIjpmYWxzZX19.1OzEhDZGRePTaSwveehR2ID5ZEvVKrR2XFlKiXwoo7A','2023-04-02 16:25:58.723000'),(91,'2023-03-23 16:34:21.997000',4,1,'2023-03-23 16:34:21.997000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA0MjgwNjEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTHV6RE9pLlBNZzl6QlBFUzd3MmlyZTJpbkhwNnVNUFF1eVVYaW1BYTZJN1E4OEZwcHd2aDYiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4IiwiYWN0aXZhdGVkIjp0cnVlfX0.Ksv8-X431aRuiM3u4JCn2x6T9E4jIoISgp1xd-dIhlI','2023-04-02 16:34:21.962000'),(92,'2023-03-23 16:35:17.561000',4,1,'2023-03-23 16:35:17.561000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA0MjgxMTcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTHV6RE9pLlBNZzl6QlBFUzd3MmlyZTJpbkhwNnVNUFF1eVVYaW1BYTZJN1E4OEZwcHd2aDYiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4IiwiYWN0aXZhdGVkIjpmYWxzZX19.mhG-DcyUosFcNYIaYLW9lyaMCKSRGE_Q4Z-8r5CR36E','2023-04-02 16:35:17.559000'),(93,'2023-03-23 16:41:13.911000',4,1,'2023-03-23 16:41:13.911000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA0Mjg0NzMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkTHV6RE9pLlBNZzl6QlBFUzd3MmlyZTJpbkhwNnVNUFF1eVVYaW1BYTZJN1E4OEZwcHd2aDYiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQ1VTVE9NRVIiXSwidXNlcm5hbWUiOiIwOTgyNTg0MjA4IiwiYWN0aXZhdGVkIjp0cnVlfX0.hPwAV1rFTtYYB62mHTaxXZbxERhyFA4-pUKKyW_vlqI','2023-04-02 16:41:13.890000'),(94,'2023-03-23 16:42:38.824000',3,0,'2023-03-23 16:42:38.824000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA0Mjg1NTgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20iLCJhY3RpdmF0ZWQiOnRydWV9fQ.3rwGOuiMFDtap4004M2ENTA0bpyucAxsHgf8_biA1vA','2023-04-02 16:42:38.823000'),(95,'2023-03-23 16:54:12.635000',17,0,'2023-03-23 16:54:12.635000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA0MjkyNTIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkM2ZvQ3hvT2RYdFp5Y0guSUZDZTUwdTAwT1FFLnlNa1dhTk9ncDZWZ3N1N2o4Um5jVXJkUnkiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjE3LCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDQ4MzY1OTM3NSIsImFjdGl2YXRlZCI6dHJ1ZX19.-s41Atd2QtffS2hDi_Gg54NxnPiBHTxUVwDGG4iw3QE','2023-04-02 16:54:12.628000'),(96,'2023-03-24 10:50:57.650000',9,0,'2023-03-24 10:50:57.650000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA0OTM4NTcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMk05d2JzODZXMnd4RDFRdGJJaGFDTzlwUnlIRUtJSjQ4S1RcL0NuMWlnODZSOHBXSVpGUHY2IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo5LCJhdXRob3JpdGllcyI6WyJST0xFX1NFTExFUiJdLCJ1c2VybmFtZSI6InZhbmNAZ21haWwuY29tIiwiYWN0aXZhdGVkIjp0cnVlfX0.foxyN-LoNrIp8pcVRPbsNStflaWVZlD6fjmFLsXAT0k','2023-04-03 10:50:57.622000'),(97,'2023-03-24 10:52:27.502000',3,0,'2023-03-24 10:52:27.502000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA0OTM5NDcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20iLCJhY3RpdmF0ZWQiOnRydWV9fQ.7_BLeZB1-SIkR6NqIxLvHQ8lcbgdTIXdUONLe7yYHv4','2023-04-03 10:52:27.501000'),(98,'2023-03-24 11:07:41.311000',15,0,'2023-03-24 11:07:41.311000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA0OTQ4NjEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkWU45cEhGNmRvbUJaeHN5cjQ3c2h1T01rY3dlZFNSbEszR1pcL3BuUmU1d09Iby44OFB0bEJxIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjoxNSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9DVVNUT01FUiJdLCJ1c2VybmFtZSI6IjAzODc0OTQ3NTkiLCJhY3RpdmF0ZWQiOnRydWV9fQ.4p3rp24RkvCrtpX4OstIipjAPteA88FAJHbZsts03vo','2023-04-03 11:07:41.310000'),(99,'2023-03-24 12:21:28.858000',3,0,'2023-03-24 12:21:28.858000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA0OTkyODgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkdkNGSnhNYmFRaGttZ2JwYU42TmJVdUlpdFRYV3o3ajc1NGRKellJcnc5NlNKMzJ6T2JYYVciLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20iLCJhY3RpdmF0ZWQiOnRydWV9fQ.n95K_eRIWe2RKDRH210n_5t21pWTf90Gn6rl1Bow7iM','2023-04-03 12:21:28.856000'),(100,'2023-03-24 16:42:43.551000',3,0,'2023-03-24 16:42:43.551000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MTQ5NjMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkLjRtQjI3aXp0RjRlWXNCWjNFQVVpT0ZhRUtMYURvaWpTWnpSOWdmeEwza3RJell5dnFcL1wvQyIsImRlbGV0ZWQiOmZhbHNlLCJpZCI6MywiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiJdLCJ1c2VybmFtZSI6Imx1b25ndmFuYmFjYWJjeHl6QGdtYWlsLmNvbSIsImFjdGl2YXRlZCI6dHJ1ZX19.WOyEjpBh1E7pn9JWTSBnuacQrcJaUf2RGk0vs97bKFs','2023-04-03 16:42:43.545000'),(101,'2023-03-24 16:44:32.440000',3,0,'2023-03-24 16:44:32.440000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MTUwNzIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMWhKQjJFeFYwT3NpZDlsMEFEanoyZUsyMEhMdlFqSTc4bmpsei52NkgxdTlqTEJEaklFci4iLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20iLCJhY3RpdmF0ZWQiOnRydWV9fQ.2tILZf1NqrzBbTY0UrxeLbIzemYL36PODsuDO0ed71Y','2023-04-03 16:44:32.440000'),(102,'2023-03-24 17:24:36.474000',9,0,'2023-03-24 17:24:36.474000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MTc0NzYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMk05d2JzODZXMnd4RDFRdGJJaGFDTzlwUnlIRUtJSjQ4S1RcL0NuMWlnODZSOHBXSVpGUHY2IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo5LCJhdXRob3JpdGllcyI6WyJST0xFX1NFTExFUiJdLCJ1c2VybmFtZSI6InZhbmNAZ21haWwuY29tIiwiYWN0aXZhdGVkIjp0cnVlfX0.pb1XqxMxIPyappTxATo2Knskto_8rF7hvdR3q90dJSo','2023-04-03 17:24:36.469000'),(103,'2023-03-24 17:36:03.125000',3,0,'2023-03-24 17:36:03.125000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MTgxNjMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMWhKQjJFeFYwT3NpZDlsMEFEanoyZUsyMEhMdlFqSTc4bmpsei52NkgxdTlqTEJEaklFci4iLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20iLCJhY3RpdmF0ZWQiOnRydWV9fQ._5sIhv0nyvYkAxOTJCrsmR0J13ScCnR_Mc1fW1YuUCs','2023-04-03 17:36:03.119000'),(104,'2023-03-24 17:38:22.670000',3,0,'2023-03-24 17:38:22.670000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MTgzMDIsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMWhKQjJFeFYwT3NpZDlsMEFEanoyZUsyMEhMdlFqSTc4bmpsei52NkgxdTlqTEJEaklFci4iLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20iLCJhY3RpdmF0ZWQiOnRydWV9fQ.VSTGZVIYc97g5ZkuKOWmSLa2fJI0NSoVkTtn71wZpQE','2023-04-03 17:38:22.669000'),(105,'2023-03-24 17:50:19.804000',21,0,'2023-03-24 17:50:19.804000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MTkwMTksInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkZmVVRW42NUsyRGIwLlFXNnN0Y29QdWI0NTAyNmxEamFXcTBZb1FjR3ZONURldUc0eHdDR2UiLCJkZWxldGVkIjpmYWxzZSwiaWQiOjIxLCJhdXRob3JpdGllcyI6WyJST0xFX0NVU1RPTUVSIl0sInVzZXJuYW1lIjoiMDQ4Mzc0NjM5MyIsImFjdGl2YXRlZCI6dHJ1ZX19.OyO2TrCF4NW4J5WDP7_yEtAJC_jfCgyVI_wwhJeXemU','2023-04-03 17:50:19.803000'),(106,'2023-03-24 18:05:24.425000',9,0,'2023-03-24 18:05:24.425000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MTk5MjQsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMk05d2JzODZXMnd4RDFRdGJJaGFDTzlwUnlIRUtJSjQ4S1RcL0NuMWlnODZSOHBXSVpGUHY2IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo5LCJhdXRob3JpdGllcyI6WyJST0xFX1NFTExFUiJdLCJ1c2VybmFtZSI6InZhbmNAZ21haWwuY29tIiwiYWN0aXZhdGVkIjp0cnVlfX0.2hmCW7c-23TNfrOxtjNoXq5DyWV7oCZrGT7OUTmWhpQ','2023-04-03 18:05:24.424000'),(107,'2023-03-24 18:20:36.599000',3,0,'2023-03-24 18:20:36.599000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MjA4MzYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMWhKQjJFeFYwT3NpZDlsMEFEanoyZUsyMEhMdlFqSTc4bmpsei52NkgxdTlqTEJEaklFci4iLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20iLCJhY3RpdmF0ZWQiOnRydWV9fQ.HjELPyVTnd7A_hnf85IQUBUzmLOGJCT6n9_-EWmydc8','2023-04-03 18:20:36.598000'),(108,'2023-03-24 18:20:58.773000',4,1,'2023-03-24 18:20:58.773000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MjA4NTgsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkN0dBYS4xRUFsTEl4Tmd5S3ZmaUttZUFcL3labXdPcTN1cHhabTl2YnY0ZWcuNThOd1pNaUFlIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX1NFTExFUiJdLCJ1c2VybmFtZSI6IjA5ODI1ODQyMDgiLCJhY3RpdmF0ZWQiOnRydWV9fQ.lgBaYb3FzfvnfH72gdbYyLD_tOkub17DFFxfSTIxXug','2023-04-03 18:20:58.772000'),(109,'2023-03-24 18:21:40.242000',9,0,'2023-03-24 18:21:40.242000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MjA5MDAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMk05d2JzODZXMnd4RDFRdGJJaGFDTzlwUnlIRUtJSjQ4S1RcL0NuMWlnODZSOHBXSVpGUHY2IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo5LCJhdXRob3JpdGllcyI6WyJST0xFX1NFTExFUiJdLCJ1c2VybmFtZSI6InZhbmNAZ21haWwuY29tIiwiYWN0aXZhdGVkIjp0cnVlfX0._9qvalwyBM8rKYKbWSdm7Sd3ZnAIm2QZx094JnLZIto','2023-04-03 18:21:40.241000'),(110,'2023-03-24 18:22:06.294000',4,1,'2023-03-24 18:22:06.294000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MjA5MjYsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkN0dBYS4xRUFsTEl4Tmd5S3ZmaUttZUFcL3labXdPcTN1cHhabTl2YnY0ZWcuNThOd1pNaUFlIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX1NFTExFUiJdLCJ1c2VybmFtZSI6IjA5ODI1ODQyMDgiLCJhY3RpdmF0ZWQiOnRydWV9fQ.w4AGceDFOnut2o6EapwFZRTziVH7rhy1e3fg1SKv5Q8','2023-04-03 18:22:06.294000'),(111,'2023-03-24 18:23:57.265000',4,1,'2023-03-24 18:23:57.265000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MjEwMzcsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkN0dBYS4xRUFsTEl4Tmd5S3ZmaUttZUFcL3labXdPcTN1cHhabTl2YnY0ZWcuNThOd1pNaUFlIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX1NFTExFUiJdLCJ1c2VybmFtZSI6IjA5ODI1ODQyMDgiLCJhY3RpdmF0ZWQiOnRydWV9fQ.oSqCDuIpRwxxGSEtzJpX8zdLmxt5pA4nbs-0WIfemB8','2023-04-03 18:23:57.264000'),(112,'2023-03-24 18:24:25.543000',4,0,'2023-03-24 18:24:25.543000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MjEwNjUsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkRWtBTDhwZ0poTGpBbjBSU1U1UHdPLksyOFpJY3dEQmZZNGgwek1xOXFIbTMxdHlXTFwvelZPIiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo0LCJhdXRob3JpdGllcyI6WyJST0xFX1NFTExFUiJdLCJ1c2VybmFtZSI6IjA5ODI1ODQyMDgiLCJhY3RpdmF0ZWQiOnRydWV9fQ.seC2yZPkPyne9mS5myzOxxDH7J4mbbqs8jezVflKMOU','2023-04-03 18:24:25.542000'),(113,'2023-03-24 20:13:50.861000',3,0,'2023-03-24 20:13:50.861000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1Mjc2MzAsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMWhKQjJFeFYwT3NpZDlsMEFEanoyZUsyMEhMdlFqSTc4bmpsei52NkgxdTlqTEJEaklFci4iLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20iLCJhY3RpdmF0ZWQiOnRydWV9fQ.oetVsaUFvkiHJZ7hVBgE9W8RS6ZqNOD4lv5Dcq_SVsQ','2023-04-03 20:13:50.791000'),(114,'2023-03-24 21:04:23.945000',9,0,'2023-03-24 21:04:23.945000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MzA2NjMsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMk05d2JzODZXMnd4RDFRdGJJaGFDTzlwUnlIRUtJSjQ4S1RcL0NuMWlnODZSOHBXSVpGUHY2IiwiZGVsZXRlZCI6ZmFsc2UsImlkIjo5LCJhdXRob3JpdGllcyI6WyJST0xFX1NFTExFUiJdLCJ1c2VybmFtZSI6InZhbmNAZ21haWwuY29tIiwiYWN0aXZhdGVkIjp0cnVlfX0.pv2kH3hGcnb0-fg9rb1xZd-0ldeMaobaa6PGEJIuJvQ','2023-04-03 21:04:23.944000'),(115,'2023-03-24 21:11:51.695000',3,0,'2023-03-24 21:11:51.695000',0,'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2ODA1MzExMTEsInVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkMWhKQjJFeFYwT3NpZDlsMEFEanoyZUsyMEhMdlFqSTc4bmpsei52NkgxdTlqTEJEaklFci4iLCJkZWxldGVkIjpmYWxzZSwiaWQiOjMsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwidXNlcm5hbWUiOiJsdW9uZ3ZhbmJhY2FiY3h5ekBnbWFpbC5jb20iLCJhY3RpdmF0ZWQiOnRydWV9fQ.eEr3e5nQAqWFyMZH3WIHKH-9tVYZCyWAjPSMnTlQ--w','2023-04-03 21:11:51.694000');
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_photos`
--

DROP TABLE IF EXISTS `user_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_photos` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `user_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_sen33i3kjbtt7ipwshfhb0f1n` (`name`),
  KEY `FKisphkumx3d7jq9tnqan3vj7dy` (`user_id`),
  CONSTRAINT `FKisphkumx3d7jq9tnqan3vj7dy` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_photos`
--

LOCK TABLES `user_photos` WRITE;
/*!40000 ALTER TABLE `user_photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `user_id` bigint NOT NULL,
  `role_id` int NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKh8ciramu9cc9q3qcqiv4ue8a6` (`role_id`),
  CONSTRAINT `FKh8ciramu9cc9q3qcqiv4ue8a6` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `FKhfh9dx7w3ubf1co1vdev94g3f` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (3,1),(4,2),(9,2),(20,2),(8,3),(10,3),(11,3),(12,3),(13,3),(14,3),(15,3),(16,3),(17,3),(18,3),(19,3),(21,3);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `updated_at` datetime(6) DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `postal_code` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `activated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_r43af9ap4edm43mmtq01oddj6` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'2023-02-23 12:54:57.107000',0,0,'2023-03-24 17:13:53.966000',3,'Bac','Luong','$2a$10$1hJB2ExV0Osid9l0ADjz2eK20HLvQjI78njlz.v6H1u9jLBDjIEr.','luongvanbacabcxyz@gmail.com','Sai Gon, Vietnam','Sai Gon','Vietnam','200000','Vietnam',1),(4,'2023-02-23 17:09:26.597000',0,0,'2023-03-24 18:24:15.766000',4,'Bac','Luong','$2a$10$EkAL8pgJhLjAn0RSU5PwO.K28ZIcwDBfY4h0zMq9qHm31tyWL/zVO','0982584208','HN','','Vietnam','','',1),(8,'2023-03-09 06:53:54.585000',0,0,'2023-03-20 21:31:27.660000',8,'Vac C','Le','$2a$10$mn7IFVVRI3ORjmxEKOUzx.EOE.8qr4z1k2semMpwn8y3j3HGH7mMC','0123456789','Sai Gon, Vietnam','Sai Gon','Vietnam','100000','Vietnam',1),(9,'2023-03-13 15:18:10.613000',0,0,'2023-03-14 10:31:41.038000',3,'Van C','Le','$2a$10$2M9wbs86W2wxD1QtbIhaCO9pRyHEKIJ48KT/Cn1ig86R8pWIZFPv6','vanc@gmail.com','HN',NULL,NULL,NULL,NULL,1),(10,'2023-03-17 21:43:24.936000',0,0,'2023-03-17 21:43:24.936000',0,'Van D','Nguyen','$2a$10$yxOOJL4wmvKIqW9trH/S1.V/vqRLgyXum.mcmyysfsrpKn1W/6M2e','0123456780','HN',NULL,NULL,NULL,NULL,1),(11,'2023-03-21 20:27:41.591000',3,0,'2023-03-22 17:09:32.044000',0,'E','Luong','$2a$10$nQSBRzi2kq.qEhoGq1RK4O5V1nlrzhPjCWJdhPzBrTantHsCYYtKu','1234567890','HN','','','','',1),(12,'2023-03-22 17:11:25.716000',3,0,'2023-03-22 17:13:41.547000',12,'M','Nguyen','$2a$10$2fSazg.pAr/EAN.cprTOi.ibFFalDgh/orMXLGzgX5SGkRvIICTmu','0829474932','HN','','','','',1),(13,'2023-03-22 17:16:00.554000',3,0,'2023-03-22 17:22:26.211000',0,'Bac','Le','$2a$10$ebyNhYWAw18rTEc/durH.uIvZK2nA5zsEFecItZgO5YsN5yb8IgB6','09384739485','HN','','','','',1),(14,'2023-03-22 17:33:07.294000',0,0,'2023-03-22 17:36:12.438000',0,'Bac','Luong','$2a$10$AolwT5vmQbAZ7Ym7ZbryvuQHmCTrWChxb3gNndLs0jn8z6GZHgADC','03849374837',NULL,NULL,NULL,NULL,NULL,1),(15,'2023-03-22 17:37:18.066000',0,0,'2023-03-24 11:09:06.474000',15,'A','Tran','$2a$10$YN9pHF6domBZxsyr47shuOMkcwedSRlK3GZ/pnRe5wOHo.88PtlBq','0387494759','Da Nang','Da Nang','VN','','',1),(16,'2023-03-22 19:06:04.206000',3,0,'2023-03-22 19:06:04.206000',3,'Vac C','Le','$2a$10$YN9pHF6domBZxsyr47shuOMkcwedSRlK3GZ/pnRe5wOHo.88PtlBq','0389374857',NULL,NULL,NULL,NULL,NULL,1),(17,'2023-03-23 16:49:38.783000',3,0,'2023-03-23 17:37:10.935000',17,'Bac','Luong','$2a$10$3foCxoOdXtZycH.IFCe50u00OQE.yMkWaNOgp6Vgsu7j8RncUrdRy','0483659375','Sai Gon, Vietnam','Sai Gon','Vietnam','','Vietnam',1),(18,'2023-03-24 17:12:37.847000',3,0,'2023-03-24 17:12:56.955000',3,'S','Le','$2a$10$PZowiRCsvRcqbUMyUHvHWewpH9lRqGAfswkUK6gNTj2VCeLRPHikS','0843749384','Da Nang','Da Nang','Vietnam','200000','Vietnam',1),(19,'2023-03-24 17:30:03.944000',9,0,'2023-03-24 18:05:54.701000',9,'X','Le','no-password','0847394857','HN','HN','Vietnam','100000','Vietnam',0),(20,'2023-03-24 17:38:11.287000',0,0,'2023-03-24 17:38:57.020000',3,'Van','Bac','$2a$10$5mnu6vV3rBwaJXG9CpGYP.SprOfvO1O35LYC9LZ0tZh377es97r8q','baclv91@gmail.com','','','','','',1),(21,'2023-03-24 17:49:58.809000',0,0,'2023-03-24 17:53:46.837000',21,'M','Nguyen','$2a$10$feUEn65K2Db0.QW6stcoPub45026lDjaWq0YoQcGvN5DeuG4xwCGe','0483746393','Hue','Hue','Vietnam','','Vietnam',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-03-25 13:37:53

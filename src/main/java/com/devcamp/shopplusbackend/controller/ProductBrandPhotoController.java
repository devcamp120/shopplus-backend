package com.devcamp.shopplusbackend.controller;


import com.devcamp.shopplusbackend.entity.ProductBrandPhoto;
import com.devcamp.shopplusbackend.model.ResponseFile;
import com.devcamp.shopplusbackend.service.ProductBrandPhotoService;
import com.devcamp.shopplusbackend.service.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin
@RestController
public class ProductBrandPhotoController {
    private static final Logger logger = LoggerFactory.getLogger(ProductBrandPhotoController.class);

    @Autowired
    private ProductBrandPhotoService productBrandPhotoService;

    @Autowired
    private StorageService storageService;

    @GetMapping("/product-brand-photos/{filename}")
    public ResponseEntity<Resource> downloadProductBrandPhoto(@PathVariable String filename) {
        try {
            Resource resource = storageService.loadAsResource("/product-brand-photos/", filename);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"").body(resource);
        } catch (Exception e) {
            logger.error("Failed to download file: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/product-brand-photos")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseFile> uploadProductBrandPhoto(@RequestParam Long productBrandId, @RequestParam("file") MultipartFile file) {
        try {
            ResponseFile responseFile = productBrandPhotoService.storeProductBrandPhoto(productBrandId, file);
            if (responseFile != null) {
                return new ResponseEntity<>(responseFile, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to upload file: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/product-brand-photos/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Long> deleteProductBrandPhoto(@PathVariable Long id) {
        try {
            Long deletedId = productBrandPhotoService.deleteProductBrandPhoto(id);
            return new ResponseEntity<>(deletedId, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Failed to Delete specified ProductBrandPhoto: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.Order;
import com.devcamp.shopplusbackend.entity.OrderDetail;
import com.devcamp.shopplusbackend.model.RequestOrder;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
public class OrderController {
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    //Lấy danh sách đơn hàng
    @GetMapping("/orders")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public ResponseEntity<Result> getOrders(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "-1") Long customerId, @RequestParam(defaultValue = "2021-01-01") String startOrderDateString, @RequestParam(defaultValue = "2050-01-01") String endOrderDateString, @RequestParam(defaultValue = "2021-01-01") String startRequiredDateString, @RequestParam(defaultValue = "2050-01-01") String endRequiredDateString, @RequestParam(defaultValue = "All") String status, @RequestParam(defaultValue = "0") String sortBy, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<Order> orders = orderService.getOrders(keyword, customerId, startOrderDateString, endOrderDateString, startRequiredDateString, endRequiredDateString, status, sortBy, page, size);
            Result result = new Result(orders.getTotalPages(), orders.getNumberOfElements(), orders.getTotalElements(), orders.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get orders: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy danh sách đơn hàng bởi người dùng
    @GetMapping("/orders/orders-by-user")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER', 'CUSTOMER')")
    public ResponseEntity<Result> getOrdersByUser(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            Page<Order> orders = orderService.getOrdersByUser(username, page, size);
            Result result = new Result(orders.getTotalPages(), orders.getNumberOfElements(), orders.getTotalElements(), orders.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get orders: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy danh sách đơn hàng bởi select tìm kiếm
    @GetMapping("/orders/select-search")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public ResponseEntity<List<Order>> selectSearchOrders(@RequestParam String keyword) {
        try {
            List<Order> orders = orderService.selectSearchOrders(keyword);
            return new ResponseEntity<>(orders, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get orders: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy thông tin đơn hàng theo id
    @GetMapping("/orders/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public ResponseEntity<Order> getOrderById(@PathVariable Long id) {
        try {
            Order order = orderService.getOrderById(id);
            if (order != null) {
                return new ResponseEntity<>(order, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get order: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy thông tin đơn hàng bởi người dùng
    @GetMapping("/orders/{orderCode}/order-by-user")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER', 'CUSTOMER')")
    public ResponseEntity<Order> getOrderByUser(@PathVariable String orderCode) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            Order order = orderService.getOrderByUser(username, orderCode);
            if (order != null) {
                return new ResponseEntity<>(order, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get order: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy danh sách chi tiết đơn hàng
    @GetMapping("/orders/{orderId}/order-details")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public ResponseEntity<Set<OrderDetail>> getOrderDetails(@PathVariable Long orderId) {
        try {
            Set<OrderDetail> orderDetails = orderService.getOrderDetails(orderId);
            if (orderDetails != null) {
                return new ResponseEntity<>(orderDetails, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get orderDetails: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy danh sách chi tiết đơn hàng bởi người dùng
    @GetMapping("/orders/{orderCode}/order-details-by-user")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER', 'CUSTOMER')")
    public ResponseEntity<Set<OrderDetail>> getOrderDetailsByUser(@PathVariable String orderCode) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            Set<OrderDetail> orderDetails = orderService.getOrderDetailsByUser(username, orderCode);
            if (orderDetails != null) {
                return new ResponseEntity<>(orderDetails, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get orderDetails: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy tổng số đơn hàng theo thời gian
    @GetMapping("/orders/total-orders")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Long> getTotalOrders(@RequestParam String startDateString, @RequestParam String endDateString) {
        try {
            Long totalOrders = orderService.getTotalOrders(startDateString, endDateString);
            return new ResponseEntity<>(totalOrders, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get totalOrders: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy tổng doanh thu theo thời gian
    @GetMapping("/orders/total-sales")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<BigDecimal> getSales(@RequestParam String startDateString, @RequestParam String endDateString) {
        try {
            BigDecimal totalSales = orderService.getTotalSales(startDateString, endDateString);
            return new ResponseEntity<>(totalSales, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get totalSales: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Tạo mới đơn hàng bởi quản trị viên
    @PostMapping("/orders")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public ResponseEntity<Order> createOrder(@RequestBody RequestOrder requestOrder) {
        try {
            Order order = orderService.createOrder(requestOrder);
            return new ResponseEntity<>(order, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Failed to Create specified Order: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Tạo mới đơn hàng bởi người dùng
    @PostMapping("/orders/create-order-by-user")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER', 'CUSTOMER')")
    public ResponseEntity<Order> createOrderByUser(@RequestBody RequestOrder requestOrder) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            Order order = orderService.createOrderByUser(username, requestOrder);
            if (order != null) {
                return new ResponseEntity<>(order, HttpStatus.CREATED);
            }
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            logger.error("Failed to Create specified Order: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Chỉnh sửa đơn hàng bởi quản trị viên
    @PutMapping("/orders/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public ResponseEntity<Order> updateOrder(@PathVariable Long id, @RequestBody RequestOrder requestOrder) {
        try {
            Order order = orderService.updateOrder(id, requestOrder);
            if (order != null) {
                return new ResponseEntity<>(order, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Update specified Order: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xóa đơn hàng bởi quản trị viên
    @DeleteMapping("/orders/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public ResponseEntity<Order> deleteOrder(@PathVariable Long id) {
        try {
            boolean isDeleted = orderService.deleteOrder(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Delete specified Order: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xóa đơn hàng bởi người dùng
    @DeleteMapping("/orders/{orderCode}/delete-by-user")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER', 'CUSTOMER')")
    public ResponseEntity<Order> deleteOrderByUser(@PathVariable String orderCode) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            boolean isDeleted = orderService.deleteOrderByUser(username, orderCode);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Delete specified Order: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

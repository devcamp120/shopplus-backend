package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.Graphics;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.GraphicsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class GraphicsController {
    private static final Logger logger = LoggerFactory.getLogger(GraphicsController.class);

    @Autowired
    private GraphicsService graphicsService;

    @GetMapping("/graphics")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Result> getGraphics(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<Graphics> graphics = graphicsService.getGraphics(keyword, page, size);
            Result result = new Result(graphics.getTotalPages(), graphics.getNumberOfElements(), graphics.getTotalElements(), graphics.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get graphics: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/graphics/select-search")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<Graphics>> selectSearchGraphics(@RequestParam String keyword) {
        try {
            List<Graphics> graphics = graphicsService.selectSearchGraphics(keyword);
            return new ResponseEntity<>(graphics, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get graphics: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/graphics/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Graphics> getGraphicsById(@PathVariable Long id) {
        try {
            Graphics graphics = graphicsService.getGraphicsById(id);
            if (graphics != null) {
                return new ResponseEntity<>(graphics, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get graphics: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/graphics")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Graphics> createGraphics(@RequestBody Graphics pGraphics) {
        try {
            Graphics graphics = graphicsService.createGraphics(pGraphics);
            return new ResponseEntity<>(graphics, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Failed to Create specified Graphics: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/graphics/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateGraphics(@PathVariable Long id, @RequestBody Graphics pGraphics) {
        try {
            Graphics graphics = graphicsService.updateGraphics(id, pGraphics);
            if (graphics != null) {
                return new ResponseEntity<>(graphics, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Update specified Graphics: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/graphics/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Graphics> deleteGraphics(@PathVariable Long id) {
        try {
            boolean isDeleted = graphicsService.deleteGraphics(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Delete specified Graphics: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/graphics/gpu-name/{gpuName}/exists")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Boolean> checkByGpuName(@PathVariable String gpuName) {
        try {
            boolean check = graphicsService.existsByGpuName(gpuName);
            return new ResponseEntity<>(check, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

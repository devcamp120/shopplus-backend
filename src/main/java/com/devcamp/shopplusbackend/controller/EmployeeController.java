package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.Employee;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class EmployeeController {
    private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    private EmployeeService employeeService;

    //Lấy danh sách nhân viên
    @GetMapping("/employees")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Result> getEmployees(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<Employee> employees = employeeService.getEmployees(keyword, page, size);
            Result result = new Result(employees.getTotalPages(), employees.getNumberOfElements(), employees.getTotalElements(), employees.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get employees: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy danh sách nhân viên bởi select tìm kiếm
    @GetMapping("/employees/select-search")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public ResponseEntity<List<Employee>> selectSearchEmployees(@RequestParam String keyword) {
        try {
            List<Employee> employees = employeeService.selectSearchEmployees(keyword);
            return new ResponseEntity<>(employees, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get employees: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy thông tin nhân viên theo id
    @GetMapping("/employees/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable Long id) {
        try {
            Employee employee = employeeService.getEmployeeById(id);
            if (employee != null) {
                return new ResponseEntity<>(employee, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get employee: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Tạo thông tin nhân viên
    @PostMapping("/employees")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee pEmployee) {
        try {
            Employee employee = employeeService.createEmployee(pEmployee);
            return new ResponseEntity<>(employee, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Failed to Create specified Employee: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Chỉnh sửa thông tin nhân viên
    @PutMapping("/employees/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateEmployee(@PathVariable Long id, @RequestBody Employee pEmployee) {
        try {
            Employee employee = employeeService.updateEmployee(id, pEmployee);
            if (employee != null) {
                return new ResponseEntity<>(employee, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Update specified Employee: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xóa thông tin nhân viên
    @DeleteMapping("/employees/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable Long id) {
        try {
            boolean isDeleted = employeeService.deleteEmployee(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Delete specified Employee: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Kiểm tra email nhân viên đã tồn tại hay chưa
    @GetMapping("/employees/email/{email}/exists")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Boolean> checkByPhoneNumber(@PathVariable String email) {
        try {
            boolean check = employeeService.existsByEmail(email);
            return new ResponseEntity<>(check, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

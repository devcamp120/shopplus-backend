package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.Product;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@CrossOrigin
@RestController
public class ProductController {
	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private ProductService productService;

	//Lấy danh sách sản phẩm
	@GetMapping("/products")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Result> getProducts(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "-1") Long productBrandId, @RequestParam(defaultValue = "-1") Long processorBrandId, @RequestParam(defaultValue = "1") int enabled, @RequestParam(defaultValue = "0") String sortBy, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
		try {
			Page<Product> products = productService.getProducts(keyword, productBrandId, processorBrandId, enabled, sortBy, page, size);
			Result result = new Result(products.getTotalPages(), products.getNumberOfElements(), products.getTotalElements(), products.getContent());
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Cannot get products: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Lấy danh sách sản phẩm bởi select tìm kiếm
	@GetMapping("/products/search")
	public ResponseEntity<Result> searchProducts(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "-1") Long productBrandId, @RequestParam(defaultValue = "-1") Long productLineId, @RequestParam(defaultValue = "12") BigDecimal minScreenSize, @RequestParam(defaultValue = "20") BigDecimal maxScreenSize, @RequestParam(defaultValue = "0") BigDecimal minPrice, @RequestParam(defaultValue = "50000") BigDecimal maxPrice, @RequestParam(defaultValue = "-1") Integer memory, @RequestParam(defaultValue = "-1") Integer storageSize, @RequestParam(defaultValue = "0") String sortBy,  @RequestParam(defaultValue = "0") int page,  @RequestParam(defaultValue = "12") int size) {
		try {
			Page<Product> products = productService.searchProducts(keyword, productBrandId, productLineId, minScreenSize, maxScreenSize, minPrice, maxPrice, memory, storageSize, sortBy, page, size);
			Result result = new Result(products.getTotalPages(), products.getNumberOfElements(), products.getTotalElements(), products.getContent());
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Cannot get products: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Lấy top sản phẩm theo số lượng đơn hàng bởi người dùng
	@GetMapping("/products/best-sellers")
	public ResponseEntity<Result> bestSellersProducts(@RequestParam String startDateString, @RequestParam String endDateString, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
		try {
			Page<Product> products = productService.bestSellersProducts(startDateString, endDateString, page, size);
			Result result = new Result(products.getTotalPages(), products.getNumberOfElements(), products.getTotalElements(), products.getContent());
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Cannot get products: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Lấy top sản phẩm theo số lượng đơn hàng bởi quản trị viên
	@GetMapping("/products/top-selling-by-total-orders")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Result> topSellingProductsByTotalOrders(@RequestParam String startDateString, @RequestParam String endDateString, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "20") int size) {
		try {
			Page<Product> products = productService.topSellingProductsByTotalOrders(startDateString, endDateString, page, size);
			Result result = new Result(products.getTotalPages(), products.getNumberOfElements(), products.getTotalElements(), products.getContent());
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Cannot get products: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Lấy top sản phẩm theo doanh thu bởi quản trị viên
	@GetMapping("/products/top-selling-by-total-sales")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Result> topSellingProductsByTotalSales(@RequestParam String startDateString, @RequestParam String endDateString, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "20") int size) {
		try {
			Page<Product> products = productService.topSellingProductsByTotalSales(startDateString, endDateString, page, size);
			Result result = new Result(products.getTotalPages(), products.getNumberOfElements(), products.getTotalElements(), products.getContent());
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Cannot get products: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Lấy danh sách sản phẩm bởi select tìm kiếm
	@GetMapping("/products/select-search")
	@PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
	public ResponseEntity<List<Product>> selectSearchProducts(@RequestParam String keyword) {
		try {
			List<Product> products = productService.selectSearchProducts(keyword);
			return new ResponseEntity<>(products, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Cannot get products: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Lấy thông tin sản phẩm theo id
	@GetMapping("/products/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable Long id) {
		try {
			Product product = productService.getProductById(id);
			if (product != null) {
				return new ResponseEntity<>(product, HttpStatus.OK);
			}
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			logger.error("Cannot get product: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Lấy tổng số đơn hàng của một sản phẩm theo thời gian
	@GetMapping("/products/total-orders-product")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Long> getTotalOrders(@RequestParam String startDateString, @RequestParam String endDateString, @RequestParam Long productId) {
		try {
			Long totalOrders = productService.getTotalOrders(startDateString, endDateString, productId);
			return new ResponseEntity<>(totalOrders, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Cannot get totalOrders: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Lấy tổng doanh thu của một sản phẩm theo thời gian
	@GetMapping("/products/total-sales-product")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<BigDecimal> getTotalSales(@RequestParam String startDateString, @RequestParam String endDateString, @RequestParam Long productId) {
		try {
			BigDecimal totalSales = productService.getTotalSales(startDateString, endDateString, productId);
			return new ResponseEntity<>(totalSales, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Cannot get totalSales: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Tạo thông tin sản phẩm
	@PostMapping("/products")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Product> createProduct(@RequestBody Product pProduct) {
		try {
			Product product = productService.createProduct(pProduct);
			return new ResponseEntity<>(product, HttpStatus.CREATED);
		} catch (Exception e) {
			logger.error("Failed to Create specified Product: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Chỉnh sửa thông tin sản phẩm
	@PutMapping("/products/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Object> updateProduct(@PathVariable Long id, @RequestBody Product pProduct) {
		try {
			Product product = productService.updateProduct(id, pProduct);
			if (product != null) {
				return new ResponseEntity<>(product, HttpStatus.OK);
			}
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			logger.error("Failed to Update specified Product: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Xóa thông tin sản phẩm
	@DeleteMapping("/products/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Product> deleteProduct(@PathVariable Long id) {
		try {
			boolean isDeleted = productService.deleteProduct(id);
			if (isDeleted) {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			logger.error("Failed to Delete specified Product: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Enable/Disable sản phẩm
	@PatchMapping("products/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Product> disableOrEnableProduct(@PathVariable Long id) {
		try {
			boolean isDisabledOrEnabled = productService.disableOrEnableProduct(id);
			if (isDisabledOrEnabled) {
				return new ResponseEntity<>(null, HttpStatus.OK);
			}
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			logger.error("Failed to Enable / Disable specified Product: {}", e.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Kiểm tra mã sản phẩm đã tồn tại hay chưa
	@GetMapping("/products/productCode/{productCode}/exists")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Boolean> checkByProductCode(@PathVariable String productCode) {
		try {
			boolean check = productService.existsByProductCode(productCode);
			return new ResponseEntity<>(check, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.*;
import com.devcamp.shopplusbackend.entity.GraphicsBrand;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.GraphicsBrandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
public class GraphicsBrandController {
    private static final Logger logger = LoggerFactory.getLogger(GraphicsBrandController.class);

    @Autowired
    private GraphicsBrandService graphicsBrandService;

    @GetMapping("/graphics-brands/all")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<GraphicsBrand>> getAllGraphicsBrands() {
        try {
            List<GraphicsBrand> graphicsBrands = graphicsBrandService.getAllGraphicsBrands();
            return new ResponseEntity<>(graphicsBrands, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get graphicsBrands: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/graphics-brands")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Result> getGraphicsBrands(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<GraphicsBrand> graphicsBrands = graphicsBrandService.getGraphicsBrands(keyword, page, size);
            Result result = new Result(graphicsBrands.getTotalPages(), graphicsBrands.getNumberOfElements(), graphicsBrands.getTotalElements(), graphicsBrands.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get graphicsBrands: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/graphics-brands/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<GraphicsBrand> getGraphicsBrandById(@PathVariable Long id) {
        try {
            GraphicsBrand graphicsBrand = graphicsBrandService.getGraphicsBrandById(id);
            if (graphicsBrand != null) {
                return new ResponseEntity<>(graphicsBrand, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get graphicsBrand: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/graphics-brands/{id}/graphics-types")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Set<GraphicsType>> getGraphicsTypes(@PathVariable Long id) {
        try {
            Set<GraphicsType> graphicsTypes = graphicsBrandService.getGraphicsTypes(id);
            if (graphicsTypes != null) {
                return new ResponseEntity<>(graphicsTypes, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get graphicsTypes: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/graphics-brands")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<GraphicsBrand> createGraphicsBrand(@RequestBody GraphicsBrand pGraphicsBrand) {
        try {
            GraphicsBrand graphicsBrand = graphicsBrandService.createGraphicsBrand(pGraphicsBrand);
            return new ResponseEntity<>(graphicsBrand, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Failed to Create specified GraphicsBrand: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/graphics-brands/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateGraphicsBrand(@PathVariable Long id, @RequestBody GraphicsBrand pGraphicsBrand) {
        try {
            GraphicsBrand graphicsBrand = graphicsBrandService.updateGraphicsBrand(id, pGraphicsBrand);
            if (graphicsBrand != null) {
                return new ResponseEntity<>(graphicsBrand, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Update specified GraphicsBrand: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/graphics-brands/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<GraphicsBrand> deleteGraphicsBrand(@PathVariable Long id) {
        try {
            boolean isDeleted = graphicsBrandService.deleteGraphicsBrand(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Delete specified GraphicsBrand: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/graphics-brands/name/{name}/exists")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Boolean> checkByName(@PathVariable String name) {
        try {
            boolean check = graphicsBrandService.existsByName(name);
            return new ResponseEntity<>(check, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

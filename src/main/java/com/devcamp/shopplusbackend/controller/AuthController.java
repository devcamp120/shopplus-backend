package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.Token;
import com.devcamp.shopplusbackend.entity.User;
import com.devcamp.shopplusbackend.model.*;
import com.devcamp.shopplusbackend.security.JwtUtil;
import com.devcamp.shopplusbackend.security.UserPrincipal;
import com.devcamp.shopplusbackend.service.TokenService;
import com.devcamp.shopplusbackend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@CrossOrigin
@RestController
public class AuthController {
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private JwtUtil jwtUtil;

    // Đăng ký tài khoản
    @PostMapping("/auth/register")
    public ResponseEntity<User> register(@RequestBody RequestUser requestUser) {
        try {
            User user = userService.registerUser(requestUser);
            if (user != null) {
                return new ResponseEntity<>(user, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            logger.error("Failed to Register: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Đăng nhập
    @PostMapping("/auth/login")
    public ResponseEntity<LoginResult> login(@RequestBody RequestUser requestUser) {
        try {
            UserPrincipal userPrincipal = userService.findByUsername(requestUser.getUsername());
            if (null == userPrincipal || !encoder.matches(requestUser.getPassword(), userPrincipal.getPassword()) || !userPrincipal.isAccountNonLocked() || !userPrincipal.isEnabled()) {
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            }
            Token token = new Token();
            token.setToken(jwtUtil.generateToken(userPrincipal));
            token.setTokenExpDate(jwtUtil.generateExpirationDate());
            token.setCreatedBy(userPrincipal.getId());
            tokenService.createToken(token);
            User user = userService.getUserById(userPrincipal.getId());
            Set<String> strRoles = new HashSet<>();
            user.getRoles().forEach(role -> strRoles.add(role.getRoleKey()));
            LoginResult loginResult = new LoginResult(token.getToken(), strRoles);
            return new ResponseEntity<>(loginResult, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Failed to Login: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy thông tin người dùng
    @GetMapping("/auth/user-info")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER', 'CUSTOMER')")
    public ResponseEntity<UserInfo> getUserInfo() {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            UserInfo userInfo = userService.getUserInfoByUsername(username);
            return new ResponseEntity<>(userInfo, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get user info: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Chỉnh sửa thông tin người dùng
    @PostMapping("/auth/edit-profile")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER', 'CUSTOMER')")
    public ResponseEntity<UserInfo> editProfile(@Valid @RequestBody RequestUser userEdit) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            UserInfo userInfo = userService.updateProfile(username, userEdit);
            if (userInfo != null) {
                return new ResponseEntity<>(userInfo, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            logger.error("Failed to Register: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Đổi mật khẩu
    @PutMapping("/auth/change-password")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER', 'CUSTOMER')")
    public ResponseEntity<Boolean> changePassword(@RequestParam String currentPassword, @RequestParam String newPassword) {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            boolean isChanged = userService.changePassword(username, currentPassword, newPassword);
            if (isChanged) {
                tokenService.deleteTokenByCreatedBy(userService.findByUsername(username).getId());
            }
            return new ResponseEntity<>(isChanged, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.Office;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.OfficeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class OfficeController {
    private static final Logger logger = LoggerFactory.getLogger(OfficeController.class);

    @Autowired
    private OfficeService officeService;

    @GetMapping("/offices/all")
    public ResponseEntity<List<Office>> getAllOffices() {
        try {
            List<Office> offices = officeService.getAllOffices();
            return new ResponseEntity<>(offices, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get offices: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/offices")
    public ResponseEntity<Result> getOffices(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<Office> offices = officeService.getOffices(keyword, page, size);
            Result result = new Result(offices.getTotalPages(), offices.getNumberOfElements(), offices.getTotalElements(), offices.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get offices: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/offices/{id}")
    public ResponseEntity<Office> getOfficeById(@PathVariable Long id) {
        try {
            Office office = officeService.getOfficeById(id);
            if (office != null) {
                return new ResponseEntity<>(office, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get office: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/offices")
    public ResponseEntity<Office> createOffice(@RequestBody Office pOffice) {
        try {
            Office office = officeService.createOffice(pOffice);
            return new ResponseEntity<>(office, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Failed to Create specified Office: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/offices/{id}")
    public ResponseEntity<Object> updateOffice(@PathVariable Long id, @RequestBody Office pOffice) {
        try {
            Office office = officeService.updateOffice(id, pOffice);
            if (office != null) {
                return new ResponseEntity<>(office, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Update specified Office: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/offices/{id}")
    public ResponseEntity<Office> deleteOffice(@PathVariable Long id) {
        try {
            boolean isDeleted = officeService.deleteOffice(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Delete specified Office: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/offices/phone/{phone}/exists")
    public ResponseEntity<Boolean> checkByPhone(@PathVariable String phone) {
        try {
            boolean check = officeService.existsByPhone(phone);
            return new ResponseEntity<>(check, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.User;
import com.devcamp.shopplusbackend.model.RequestUser;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.TokenService;
import com.devcamp.shopplusbackend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@CrossOrigin
@RestController
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    //Lấy danh sách tài khoản người dùng
    @GetMapping("/users")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public ResponseEntity<Result> getUsers(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<User> users = userService.getUsers(keyword, page, size);
            Result result = new Result(users.getTotalPages(), users.getNumberOfElements(), users.getTotalElements(), users.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get users: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy danh sách người dùng theo số tiền mua hàng
    @GetMapping("/users/customer-type")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Result> getUsersCustomerType(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "-1") BigDecimal totalSales, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<User> users = userService.getUsersByCustomerType(keyword, totalSales, page, size);
            Result result = new Result(users.getTotalPages(), users.getNumberOfElements(), users.getTotalElements(), users.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get users: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy danh sách người dùng bởi select tìm kiếm
    @GetMapping("/users/select-search")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public ResponseEntity<List<User>> selectSearchCustomers(@RequestParam String keyword) {
        try {
            List<User> users = userService.selectSearchCustomers(keyword);
            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get users: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //Lấy thông tin tài khoản theo id
    @GetMapping("/users/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public ResponseEntity<User> getUserById(@PathVariable Long id) {
        try {
            User user = userService.getUserById(id);
            if (user != null) {
                return new ResponseEntity<>(user, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get user: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy số lượng tài khoản đã đăng ký theo thời gian
    @GetMapping("/users/total-users")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Long> getTotalUsers(@RequestParam String startDateString, @RequestParam String endDateString) {
        try {
            Long totalUsers = userService.getTotalUsers(startDateString, endDateString);
            return new ResponseEntity<>(totalUsers, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get totalUsers: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Tạo người dùng bởi admin
    @PostMapping("/users/create-user-by-admin")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<User> createUserByAdmin(@RequestBody RequestUser requestUser) {
        try {
            User user = userService.createUserByAdmin(requestUser);
            if (user != null) {
                return new ResponseEntity<>(user, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            logger.error("Failed to create user: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Tạo người dùng bởi seller
    @PostMapping("/users/create-user-by-seller")
    @PreAuthorize("hasRole('SELLER')")
    public ResponseEntity<User> createUserBySeller(@RequestBody RequestUser requestUser) {
        try {
            User user = userService.createUserBySeller(requestUser);
            if (user != null) {
                return new ResponseEntity<>(user, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            logger.error("Failed to create user: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Chỉnh sửa thông tin người dùng bởi admin
    @PutMapping("/users/{id}/update-user-by-admin")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<User> updateUserByAdmin(@PathVariable Long id, @RequestBody RequestUser requestUser) {
        try {
            User user = userService.updateUserByAdmin(id, requestUser);
            if (user != null) {
                return new ResponseEntity<>(user, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            logger.error("Failed to edit user: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Chỉnh sửa thông tin người dùng bởi seller
    @PutMapping("/users/{id}/update-user-by-seller")
    @PreAuthorize("hasRole('SELLER')")
    public ResponseEntity<User> updateUserBySeller(@PathVariable Long id, @RequestBody RequestUser requestUser) {
        try {
            User user = userService.updateUserBySeller(id, requestUser);
            if (user != null) {
                return new ResponseEntity<>(user, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            logger.error("Failed to edit user: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Enable/Disable tài khoản
    @PatchMapping("/users/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<User> disableOrEnableUser(@PathVariable Long id) {
        try {
            boolean isDisabledOrEnabled = userService.disableOrEnableUser(id);
            if (isDisabledOrEnabled) {
                tokenService.deleteTokenByCreatedBy(id);
                return new ResponseEntity<>(null, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Enable / Disable specified User: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Kiểm tra username tồn tại hay chưa
    @GetMapping("/users/username/{username}/exists")
    public ResponseEntity<Boolean> checkByUsername(@PathVariable String username) {
        try {
            boolean check = userService.existsByUsername(username);
            return new ResponseEntity<>(check, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

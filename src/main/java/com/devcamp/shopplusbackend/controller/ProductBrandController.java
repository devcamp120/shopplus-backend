package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.ProductBrand;
import com.devcamp.shopplusbackend.entity.ProductLine;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.ProductBrandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
public class ProductBrandController {
    private static final Logger logger = LoggerFactory.getLogger(ProductBrandController.class);

    @Autowired
    private ProductBrandService productBrandService;

    @GetMapping("/product-brands/all")
    public ResponseEntity<List<ProductBrand>> getAllProductBrands() {
        try {
            List<ProductBrand> productBrands = productBrandService.getAllProductBrands();
            return new ResponseEntity<>(productBrands, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get productBrands: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/product-brands")
    public ResponseEntity<Result> getProductBrands(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<ProductBrand> productBrands = productBrandService.getProductBrands(keyword, page, size);
            Result result = new Result(productBrands.getTotalPages(), productBrands.getNumberOfElements(), productBrands.getTotalElements(), productBrands.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get productBrands: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/product-brands/{id}")
    public ResponseEntity<ProductBrand> getProductBrandById(@PathVariable Long id) {
        try {
            ProductBrand productBrand = productBrandService.getProductBrandById(id);
            if (productBrand != null) {
                return new ResponseEntity<>(productBrand, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get productBrand: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/product-brands/{id}/product-lines")
    public ResponseEntity<Set<ProductLine>> getProductLines(@PathVariable Long id) {
        try {
            Set<ProductLine> productLines = productBrandService.getProductLines(id);
            if (productLines != null) {
                return new ResponseEntity<>(productLines, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get productLines: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/product-brands")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProductBrand> createProductBrand(@RequestBody ProductBrand pProductBrand) {
        try {
            ProductBrand productBrand = productBrandService.createProductBrand(pProductBrand);
            return new ResponseEntity<>(productBrand, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Failed to Create specified ProductBrand: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/product-brands/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateProductBrand(@PathVariable Long id, @RequestBody ProductBrand pProductBrand) {
        try {
            ProductBrand productBrand = productBrandService.updateProductBrand(id, pProductBrand);
            if (productBrand != null) {
                return new ResponseEntity<>(productBrand, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Update specified ProductBrand: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/product-brands/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProductBrand> deleteProductBrand(@PathVariable Long id) {
        try {
            boolean isDeleted = productBrandService.deleteProductBrand(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Delete specified ProductBrand: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/product-brands/name/{name}/exists")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Boolean> checkByName(@PathVariable String name) {
        try {
            boolean check = productBrandService.existsByName(name);
            return new ResponseEntity<>(check, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

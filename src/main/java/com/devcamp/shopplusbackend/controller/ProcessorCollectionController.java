package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.Processor;
import com.devcamp.shopplusbackend.entity.ProcessorCollection;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.ProcessorCollectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@CrossOrigin
@RestController
public class ProcessorCollectionController {
    private static final Logger logger = LoggerFactory.getLogger(ProcessorCollectionController.class);

    @Autowired
    private ProcessorCollectionService processorCollectionService;

    @GetMapping("/processor-collections")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Result> getProcessorCollections(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<ProcessorCollection> processorCollections = processorCollectionService.getProcessorCollections(keyword, page, size);
            Result result = new Result(processorCollections.getTotalPages(), processorCollections.getNumberOfElements(), processorCollections.getTotalElements(), processorCollections.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get processorCollections: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/processor-collections/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProcessorCollection> getProcessorCollectionById(@PathVariable Long id) {
        try {
            ProcessorCollection processorCollection = processorCollectionService.getProcessorCollectionById(id);
            if (processorCollection != null) {
                return new ResponseEntity<>(processorCollection, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get processorCollection: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/processor-collections/{id}/processors")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Set<Processor>> getProcessors(@PathVariable Long id) {
        try {
            Set<Processor> processors = processorCollectionService.getProcessors(id);
            if (processors != null) {
                return new ResponseEntity<>(processors, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get processors: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/processor-collections")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProcessorCollection> createProcessorCollection(@RequestBody ProcessorCollection pProcessorCollection) {
        try {
            ProcessorCollection processorCollection = processorCollectionService.createProcessorCollection(pProcessorCollection);
            return new ResponseEntity<>(processorCollection, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Failed to Create specified ProcessorCollection: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/processor-collections/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateProcessorCollection(@PathVariable Long id, @RequestBody ProcessorCollection pProcessorCollection) {
        try {
            ProcessorCollection processorCollection = processorCollectionService.updateProcessorCollection(id, pProcessorCollection);
            if (processorCollection != null) {
                return new ResponseEntity<>(processorCollection, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Update specified ProcessorCollection: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/processor-collections/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProcessorCollection> deleteProcessorCollection(@PathVariable Long id) {
        try {
            boolean isDeleted = processorCollectionService.deleteProcessorCollection(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Delete specified ProcessorCollection: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/processor-collections/name/{name}/exists")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Boolean> checkByName(@PathVariable String name) {
        try {
            boolean check = processorCollectionService.existsByName(name);
            return new ResponseEntity<>(check, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

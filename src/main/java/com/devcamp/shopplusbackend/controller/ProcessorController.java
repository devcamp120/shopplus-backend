package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.Processor;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.ProcessorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class ProcessorController {
    private static final Logger logger = LoggerFactory.getLogger(ProcessorController.class);

    @Autowired
    private ProcessorService processorService;

    @GetMapping("/processors")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Result> getProcessors(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<Processor> processors = processorService.getProcessors(keyword, page, size);
            Result result = new Result(processors.getTotalPages(), processors.getNumberOfElements(), processors.getTotalElements(), processors.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get processors: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/processors/select-search")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<Processor>> selectSearchProcessors(@RequestParam String keyword) {
        try {
            List<Processor> processors = processorService.selectSearchProcessors(keyword);
            return new ResponseEntity<>(processors, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get processors: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/processors/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Processor> getProcessorById(@PathVariable Long id) {
        try {
            Processor processor = processorService.getProcessorById(id);
            if (processor != null) {
                return new ResponseEntity<>(processor, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get processor: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/processors")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Processor> createProcessor(@RequestBody Processor pProcessor) {
        try {
            Processor processor = processorService.createProcessor(pProcessor);
            return new ResponseEntity<>(processor, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Failed to Create specified Processor: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/processors/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateProcessor(@PathVariable Long id, @RequestBody Processor pProcessor) {
        try {
            Processor processor = processorService.updateProcessor(id, pProcessor);
            if (processor != null) {
                return new ResponseEntity<>(processor, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Update specified Processor: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/processors/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Processor> deleteProcessor(@PathVariable Long id) {
        try {
            boolean isDeleted = processorService.deleteProcessor(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Delete specified Processor: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/processors/processor-number/{processorNumber}/exists")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Boolean> checkByName(@PathVariable String processorNumber) {
        try {
            boolean check = processorService.existsByProcessorNumber(processorNumber);
            return new ResponseEntity<>(check, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

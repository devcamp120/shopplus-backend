package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.ProductPhoto;
import com.devcamp.shopplusbackend.model.ResponseFile;
import com.devcamp.shopplusbackend.service.ProductPhotoService;
import com.devcamp.shopplusbackend.service.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@CrossOrigin
@RestController
public class ProductPhotoController {
    private static final Logger logger = LoggerFactory.getLogger(ProductPhotoController.class);

    @Autowired
    private ProductPhotoService productPhotoService;

    @Autowired
    private StorageService storageService;

    //Download hình ảnh của sản phẩm
    @GetMapping("/product-photos/{filename}")
    public ResponseEntity<Resource> downloadProductPhoto(@PathVariable String filename) {
        try {
            Resource resource = storageService.loadAsResource("/product-photos/", filename);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"").body(resource);
        } catch (Exception e) {
            logger.error("Failed to download file: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Upload hình ảnh của sản phẩm
    @PostMapping("/product-photos")
    public ResponseEntity<List<ResponseFile>> uploadProductPhotos(@RequestParam Long productId, @RequestParam("files") MultipartFile[] files) {
        try {
            List<ResponseFile> responseFiles = productPhotoService.storeProductPhotos(productId, files);
            if (responseFiles != null) {
                return new ResponseEntity<>(responseFiles, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to upload files: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xóa hình ảnh của sản phẩm
    @DeleteMapping("/product-photos")
    public ResponseEntity<List<Long>> deleteProductPhotos(@RequestBody List<Long> ids) {
        try {
            List<Long> deletedIds = productPhotoService.deleteProductPhotos(ids);
            return new ResponseEntity<>(deletedIds, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Failed to Delete specified ProductPhotos: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

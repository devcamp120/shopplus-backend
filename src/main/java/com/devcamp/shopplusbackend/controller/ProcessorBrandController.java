package com.devcamp.shopplusbackend.controller;

import com.devcamp.shopplusbackend.entity.ProcessorBrand;
import com.devcamp.shopplusbackend.entity.ProcessorCollection;
import com.devcamp.shopplusbackend.model.Result;
import com.devcamp.shopplusbackend.service.ProcessorBrandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
public class ProcessorBrandController {
    private static final Logger logger = LoggerFactory.getLogger(ProcessorBrandController.class);

    @Autowired
    private ProcessorBrandService processorBrandService;

    @GetMapping("/processor-brands/all")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<ProcessorBrand>> getAllProcessorBrands() {
        try {
            List<ProcessorBrand> processorBrands = processorBrandService.getAllProcessorBrands();
            return new ResponseEntity<>(processorBrands, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get processorBrands: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/processor-brands")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Result> getProcessorBrands(@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        try {
            Page<ProcessorBrand> processorBrands = processorBrandService.getProcessorBrands(keyword, page, size);
            Result result = new Result(processorBrands.getTotalPages(), processorBrands.getNumberOfElements(), processorBrands.getTotalElements(), processorBrands.getContent());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Cannot get processorBrands: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/processor-brands/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProcessorBrand> getProcessorBrandById(@PathVariable Long id) {
        try {
            ProcessorBrand processorBrand = processorBrandService.getProcessorBrandById(id);
            if (processorBrand != null) {
                return new ResponseEntity<>(processorBrand, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get processorBrand: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/processor-brands/{id}/processor-collections")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Set<ProcessorCollection>> getProcessorCollections(@PathVariable Long id) {
        try {
            Set<ProcessorCollection> processorCollections = processorBrandService.getProcessorCollections(id);
            if (processorCollections != null) {
                return new ResponseEntity<>(processorCollections, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Cannot get processorCollections: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/processor-brands")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProcessorBrand> createProcessorBrand(@RequestBody ProcessorBrand pProcessorBrand) {
        try {
            ProcessorBrand processorBrand = processorBrandService.createProcessorBrand(pProcessorBrand);
            return new ResponseEntity<>(processorBrand, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Failed to Create specified ProcessorBrand: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/processor-brands/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateProcessorBrand(@PathVariable Long id, @RequestBody ProcessorBrand pProcessorBrand) {
        try {
            ProcessorBrand processorBrand = processorBrandService.updateProcessorBrand(id, pProcessorBrand);
            if (processorBrand != null) {
                return new ResponseEntity<>(processorBrand, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Update specified ProcessorBrand: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/processor-brands/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ProcessorBrand> deleteProcessorBrand(@PathVariable Long id) {
        try {
            boolean isDeleted = processorBrandService.deleteProcessorBrand(id);
            if (isDeleted) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Failed to Delete specified ProcessorBrand: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/processor-brands/name/{name}/exists")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Boolean> checkByName(@PathVariable String name) {
        try {
            boolean check = processorBrandService.existsByName(name);
            return new ResponseEntity<>(check, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

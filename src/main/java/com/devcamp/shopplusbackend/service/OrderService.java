package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.*;
import com.devcamp.shopplusbackend.model.OrderStatus;
import com.devcamp.shopplusbackend.model.ProductSelected;
import com.devcamp.shopplusbackend.model.RequestOrder;
import com.devcamp.shopplusbackend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    //Lấy danh sách đơn hàng
    public Page<Order> getOrders(String keyword, Long customerId, String startOrderDateString, String endOrderDateString, String startRequiredDateString, String endRequiredDateString, String status, String sortBy, int page, int size) {
        Pageable pageable = switch (sortBy) {
            case "1" -> PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "orderDate"));
            case "2" -> PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "orderDate"));
            case "3" -> PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "requiredDate"));
            case "4" -> PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "requiredDate"));
            default -> PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdAt"));
        };
        java.sql.Date startOrderDate = java.sql.Date.valueOf(LocalDate.parse(startOrderDateString));
        java.sql.Date endOrderDate = java.sql.Date.valueOf(LocalDate.parse(endOrderDateString).plusDays(1));
        java.sql.Date startRequiredDate = java.sql.Date.valueOf(LocalDate.parse(startRequiredDateString));
        java.sql.Date endRequiredDate = java.sql.Date.valueOf(LocalDate.parse(endRequiredDateString).plusDays(1));
        return orderRepository.findOrders(keyword, customerId, startOrderDate, endOrderDate, startRequiredDate, endRequiredDate, status, pageable);
    }

    //Lấy danh sách đơn hàng bởi người dùng
    public Page<Order> getOrdersByUser(String username, int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdAt"));
        User user = userRepository.findByUsername(username);
        if (user != null) {
            return orderRepository.getOrdersByUser(user.getId(), pageable);
        }
        return null;
    }

    //Lấy danh sách đơn hàng bởi select tìm kiếm
    public List<Order> selectSearchOrders(String keyword) {
        return orderRepository.selectSearchOrders(keyword);
    }

    public Order getOrderById(Long id) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        return orderOptional.orElse(null);
    }

    //Lấy thông tin đơn hàng bởi người dùng
    public Order getOrderByUser(String username, String orderCode) {
        User user = userRepository.findByUsername(username);
        Order order = orderRepository.findByOrderCode(orderCode);
        if (Objects.equals(user.getId(), order.getUser().getId())) {
            return order;
        }
        return null;
    }

    //Lấy danh sách chi tiết đơn hàng
    public Set<OrderDetail> getOrderDetails(Long orderId) {
        Optional<Order> orderOptional = orderRepository.findById(orderId);
        return orderOptional.map(Order::getOrderDetails).orElse(null);
    }

    //Lấy danh sách chi tiết đơn hàng bởi người dùng
    public Set<OrderDetail> getOrderDetailsByUser(String username, String orderCode) {
        User user = userRepository.findByUsername(username);
        Order order = orderRepository.findByOrderCode(orderCode);
        if (Objects.equals(user.getId(), order.getUser().getId())) {
            return order.getOrderDetails();
        }
        return null;
    }

    //Lấy tổng số đơn hàng theo thời gian
    public Long getTotalOrders(String startDateString, String endDateString) {
        java.sql.Date startDate = java.sql.Date.valueOf(LocalDate.parse(startDateString));
        java.sql.Date endDate = java.sql.Date.valueOf(LocalDate.parse(endDateString).plusDays(1));
        return orderRepository.getTotalOrders(startDate, endDate);
    }

    //Lấy tổng doanh thu theo thời gian
    public BigDecimal getTotalSales(String startDateString, String endDateString) {
        java.sql.Date startDate = java.sql.Date.valueOf(LocalDate.parse(startDateString));
        java.sql.Date endDate = java.sql.Date.valueOf(LocalDate.parse(endDateString).plusDays(1));
        return orderRepository.getTotalSales(startDate, endDate);
    }

    //Tạo mới đơn hàng bởi quản trị viên
    public Order createOrder(RequestOrder requestOrder) {
        List<ProductSelected> productsSelected = requestOrder.getProductsSelected();
        Order pOrder = requestOrder.getOrder();
        Optional<User> userOptional = userRepository.findById(requestOrder.getOrder().getUser().getId());
        if (userOptional.isPresent() && productsSelected.size() > 0) {
            pOrder.setId(null);
            pOrder.setOrderCode(orderCodeGenerator());
            pOrder.setOrderDate(new Date());
            pOrder.setShippedDate(null);
            pOrder.setStatus(OrderStatus.Pending);
            pOrder.setUser(userOptional.get());
            Order _order = orderRepository.save(pOrder);
            productsSelected.forEach(productSelected -> {
                Optional<Product> productOptional = productRepository.findById(productSelected.getProductId());
                if (productOptional.isPresent()) {
                    Product product = productOptional.get();
                    if (!product.isDeleted() && productSelected.getQuantityOrder() <= product.getQuantityInStock() && productSelected.getQuantityOrder() > 0) {
                        OrderDetail orderDetail = new OrderDetail();
                        orderDetail.setOrder(_order);
                        orderDetail.setProduct(product);
                        orderDetail.setQuantityOrder(productSelected.getQuantityOrder());
                        orderDetail.setPriceEach(product.getPriceEach());
                        orderDetailRepository.save(orderDetail);
                        productRepository.changeQuantityInStock(product.getId(), -productSelected.getQuantityOrder());
                    }
                }
            });
            return _order;
        }
        return null;
    }

    //Tạo mới đơn hàng bởi người dùng
    public Order createOrderByUser(String username, RequestOrder requestOrder) {
        Order pOrder = requestOrder.getOrder();
        User user = userRepository.findByUsername(username);
        List<ProductSelected> productsSelected = requestOrder.getProductsSelected();
        if (user != null && productsSelected.size() > 0) {
            pOrder.setId(null);
            pOrder.setOrderCode(orderCodeGenerator());
            pOrder.setOrderDate(new Date());
            pOrder.setRequiredDate(new Date());
            pOrder.setShippedDate(null);
            pOrder.setStatus(OrderStatus.Pending);
            pOrder.setUser(user);
            Order savedOrder = orderRepository.save(pOrder);
            productsSelected.forEach(productSelected -> {
                Optional<Product> productOptional = productRepository.findById(productSelected.getProductId());
                if (productOptional.isPresent()) {
                    Product product = productOptional.get();
                    if (!product.isDeleted() && productSelected.getQuantityOrder() <= product.getQuantityInStock() && productSelected.getQuantityOrder() > 0 && productSelected.getQuantityOrder() <= 5) {
                        OrderDetail orderDetail = new OrderDetail();
                        orderDetail.setOrder(savedOrder);
                        orderDetail.setProduct(product);
                        orderDetail.setQuantityOrder(productSelected.getQuantityOrder());
                        orderDetail.setPriceEach(product.getPriceEach());
                        orderDetailRepository.save(orderDetail);
                        productRepository.changeQuantityInStock(product.getId(), -productSelected.getQuantityOrder());
                    }
                }
            });
            return savedOrder;
        }
        return null;
    }

    //Chỉnh sửa đơn hàng bởi quản trị viên
    public Order updateOrder(Long id, RequestOrder requestOrder) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        Order pOrder = requestOrder.getOrder();
        List<Long> orderDetailsDeleted = requestOrder.getOrderDetailsDeleted();
        List<ProductSelected> productsSelected = requestOrder.getProductsSelected();
        Optional<User> userOptional = userRepository.findById(requestOrder.getOrder().getUser().getId());
        if (userOptional.isPresent() && orderOptional.isPresent()) {
            Order order = orderOptional.get();
            if (order.getStatus() == OrderStatus.Pending) {
                order.setUser(userOptional.get());
                order.setRequiredDate(pOrder.getRequiredDate());
                order.setFirstName(pOrder.getFirstName());
                order.setLastName(pOrder.getLastName());
                order.setPhoneNumber(pOrder.getPhoneNumber());
                order.setAddress(pOrder.getAddress());
                order.setCity(pOrder.getCity());
                order.setState(pOrder.getState());
                order.setPostalCode(pOrder.getPostalCode());
                order.setCountry(pOrder.getCountry());
                order.setComments(pOrder.getComments());
                if (pOrder.getStatus() == OrderStatus.Pending || pOrder.getStatus() == OrderStatus.Completed) {
                    order.getOrderDetails().forEach(orderDetail -> {
                        orderDetailsDeleted.forEach(orderDetailDeleted -> {
                            if (Objects.equals(orderDetail.getId(), orderDetailDeleted)) {
                                productRepository.changeQuantityInStock(orderDetail.getProduct().getId(), orderDetail.getQuantityOrder());
                                orderDetailRepository.deleteById(orderDetail.getId());
                            }
                        });
                    });
                    productsSelected.forEach(productSelected -> {
                        Optional<Product> productOptional = productRepository.findById(productSelected.getProductId());
                        if (productOptional.isPresent()) {
                            Product product = productOptional.get();
                            if (!product.isDeleted() && productSelected.getQuantityOrder() <= product.getQuantityInStock() && productSelected.getQuantityOrder() > 0) {
                                OrderDetail orderDetail = new OrderDetail();
                                orderDetail.setOrder(order);
                                orderDetail.setProduct(product);
                                orderDetail.setQuantityOrder(productSelected.getQuantityOrder());
                                orderDetail.setPriceEach(product.getPriceEach());
                                orderDetailRepository.save(orderDetail);
                                productRepository.changeQuantityInStock(product.getId(), -productSelected.getQuantityOrder());
                            }
                        }
                    });
                }
                if (pOrder.getStatus() != OrderStatus.Refund) {
                    order.setStatus(pOrder.getStatus());
                }
                if (pOrder.getStatus() == OrderStatus.Completed) {
                    order.setShippedDate(new Date());
                } else if (pOrder.getStatus() == OrderStatus.Canceled) {
                    order.getOrderDetails().forEach(orderDetail -> {
                        productRepository.changeQuantityInStock(orderDetail.getProduct().getId(), orderDetail.getQuantityOrder());
                    });
                }
            } else if (order.getStatus() == OrderStatus.Completed && pOrder.getStatus() == OrderStatus.Refund) {
                order.setStatus(OrderStatus.Refund);
                order.getOrderDetails().forEach(orderDetail -> {
                    productRepository.changeQuantityInStock(orderDetail.getProduct().getId(), orderDetail.getQuantityOrder());
                });
            }
            order.setOrderDetails(new HashSet<>());
            return orderRepository.save(order);
        }
        return null;
    }

    //Xóa đơn hàng bởi quản trị viên
    public boolean deleteOrder(Long id) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (orderOptional.isPresent()) {
            OrderStatus orderStatus = orderOptional.get().getStatus();
            if (orderStatus == OrderStatus.Pending) {
                Set<OrderDetail> orderDetails = orderOptional.get().getOrderDetails();
                orderDetails.forEach(orderDetail -> {
                    productRepository.changeQuantityInStock(orderDetail.getProduct().getId(), orderDetail.getQuantityOrder());
                    orderDetailRepository.deleteById(orderDetail.getId());
                });
                orderRepository.deleteById(id);
                return true;
            } else if (orderStatus == OrderStatus.Canceled) {
                orderRepository.deleteById(id);
                return true;
            }
        }
        return false;
    }

    //Xóa đơn hàng bởi người dùng
    public boolean deleteOrderByUser(String username, String orderCode) {
        User user = userRepository.findByUsername(username);
        Order order = orderRepository.findByOrderCode(orderCode);
        if (order != null) {
            if (Objects.equals(user.getId(), order.getUser().getId()) && order.getStatus() == OrderStatus.Pending) {
                Set<OrderDetail> orderDetails = order.getOrderDetails();
                orderDetails.forEach(orderDetail -> {
                    productRepository.changeQuantityInStock(orderDetail.getProduct().getId(), orderDetail.getQuantityOrder());
                    orderDetailRepository.deleteById(orderDetail.getId());
                });
                orderRepository.deleteById(order.getId());
                return true;
            }
        }
        return false;
    }

    //Tạo mã đơn hàng
    private String orderCodeGenerator() {
        String AlphaNumericString = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int length = 8;
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int index = (int) (AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }
}

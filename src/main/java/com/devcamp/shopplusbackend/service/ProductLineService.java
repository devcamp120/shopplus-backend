package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.ProductLine;
import com.devcamp.shopplusbackend.repository.ProductLineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductLineService {
    @Autowired
    private ProductLineRepository productLineRepository;

    public Page<ProductLine> getProductLines(String keyword, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return productLineRepository.findProductLines(keyword, pageable);
    }

    public ProductLine getProductLineById(Long id) {
        Optional<ProductLine> productLineOptional = productLineRepository.findById(id);
        return productLineOptional.orElse(null);
    }

    public ProductLine createProductLine(ProductLine pProductLine) {
        pProductLine.setId(null);
        return productLineRepository.save(pProductLine);
    }

    public ProductLine updateProductLine(Long id, ProductLine pProductLine) {
        Optional<ProductLine> productLineOptional = productLineRepository.findById(id);
        if (productLineOptional.isPresent()) {
            pProductLine.setId(id);
            return productLineRepository.save(pProductLine);
        }
        return null;
    }

    public boolean deleteProductLine(Long id) {
        Optional<ProductLine> productLineOptional = productLineRepository.findById(id);
        if (productLineOptional.isPresent()) {
            productLineRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean existsByName(String name) {
        return productLineRepository.existsByName(name);
    }
}

package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.ProductPhoto;
import com.devcamp.shopplusbackend.entity.User;
import com.devcamp.shopplusbackend.entity.UserPhoto;
import com.devcamp.shopplusbackend.model.ResponseFile;
import com.devcamp.shopplusbackend.repository.UserPhotoRepository;
import com.devcamp.shopplusbackend.repository.UserRepository;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserPhotoService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserPhotoRepository userPhotoRepository;

    @Autowired
    private StorageService storageService;

    public ResponseFile storeUserPhoto(Long userId, MultipartFile file) {
        Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            if (user.getUserPhoto() != null) {
                UserPhoto userPhoto = user.getUserPhoto();
                userPhotoRepository.delete(userPhoto);
            }
            UserPhoto userPhoto = new UserPhoto();
            userPhoto.setUser(optionalUser.get());
            String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
            userPhoto.setName(UUID.randomUUID() + "." + fileType);
            UserPhoto photo = userPhotoRepository.save(userPhoto);
            storageService.store("/user-photos/", photo.getName(), file);
            ResponseFile responseFile = new ResponseFile();
            responseFile.setId(photo.getId());
            responseFile.setName(photo.getName());
            responseFile.setUrl(ServletUriComponentsBuilder.fromCurrentContextPath().path("/user-photos/").path(photo.getName()).toUriString());
            return responseFile;
        }
        return null;
    }

    public Long deleteUserPhoto(Long id) throws IOException {
        Optional<UserPhoto> optionalUserPhoto = userPhotoRepository.findById(id);
        if (optionalUserPhoto.isPresent()) {
            storageService.delete("/user-photos/", optionalUserPhoto.get().getName());
            userPhotoRepository.deleteById(id);
            return id;
        }
        return null;
    }
}

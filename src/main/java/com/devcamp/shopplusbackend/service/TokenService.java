package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.Token;

public interface TokenService {
    Token createToken(Token token);

    Token findByToken(String token);

    void deleteTokenByCreatedBy(Long userId);
}

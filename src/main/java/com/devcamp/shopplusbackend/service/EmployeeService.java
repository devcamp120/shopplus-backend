package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.Employee;
import com.devcamp.shopplusbackend.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public Page<Employee> getEmployees(String keyword, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return employeeRepository.findEmployees(keyword, pageable);
    }

    public List<Employee> selectSearchEmployees(String keyword) {
        return employeeRepository.selectSearchEmployees(keyword);
    }

    public Employee getEmployeeById(Long id) {
        Optional<Employee> employeeOptional = employeeRepository.findById(id);
        return employeeOptional.orElse(null);
    }

    public Employee createEmployee(Employee pEmployee) {
        pEmployee.setId(null);
        return employeeRepository.save(pEmployee);
    }

    public Employee updateEmployee(Long id, Employee pEmployee) {
        Optional<Employee> employeeOptional = employeeRepository.findById(id);
        if (employeeOptional.isPresent()) {
            pEmployee.setId(id);
            return employeeRepository.save(pEmployee);
        }
        return null;
    }

    public boolean deleteEmployee(Long id) {
        Optional<Employee> employeeOptional = employeeRepository.findById(id);
        if (employeeOptional.isPresent()) {
            employeeRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean existsByEmail(String email) {
        return employeeRepository.existsByEmail(email);
    }
}

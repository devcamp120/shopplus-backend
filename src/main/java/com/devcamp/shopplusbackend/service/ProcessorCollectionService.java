package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.Processor;
import com.devcamp.shopplusbackend.entity.ProcessorCollection;
import com.devcamp.shopplusbackend.repository.ProcessorCollectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class ProcessorCollectionService {
    @Autowired
    private ProcessorCollectionRepository processorCollectionRepository;

    public Page<ProcessorCollection> getProcessorCollections(String keyword, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return processorCollectionRepository.findProcessorCollections(keyword, pageable);
    }

    public ProcessorCollection getProcessorCollectionById(Long id) {
        Optional<ProcessorCollection> processorCollectionOptional = processorCollectionRepository.findById(id);
        return processorCollectionOptional.orElse(null);
    }

    public Set<Processor> getProcessors(Long id) {
        Optional<ProcessorCollection> processorCollectionOptional = processorCollectionRepository.findById(id);
        return processorCollectionOptional.map(ProcessorCollection::getProcessors).orElse(null);
    }

    public ProcessorCollection createProcessorCollection(ProcessorCollection pProcessorCollection) {
        pProcessorCollection.setId(null);
        return processorCollectionRepository.save(pProcessorCollection);
    }

    public ProcessorCollection updateProcessorCollection(Long id, ProcessorCollection pProcessorCollection) {
        Optional<ProcessorCollection> processorCollectionOptional = processorCollectionRepository.findById(id);
        if (processorCollectionOptional.isPresent()) {
            pProcessorCollection.setId(id);
            return processorCollectionRepository.save(pProcessorCollection);
        }
        return null;
    }

    public boolean deleteProcessorCollection(Long id) {
        Optional<ProcessorCollection> processorCollectionOptional = processorCollectionRepository.findById(id);
        if (processorCollectionOptional.isPresent()) {
            processorCollectionRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean existsByName(String name) {
        return processorCollectionRepository.existsByName(name);
    }
}
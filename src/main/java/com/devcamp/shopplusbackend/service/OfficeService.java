package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.Office;
import com.devcamp.shopplusbackend.repository.OfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OfficeService {
    @Autowired
    private OfficeRepository officeRepository;

    public List<Office> getAllOffices() {
        return officeRepository.findAll();
    }

    public Page<Office> getOffices(String keyword, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return officeRepository.findOffices(keyword, pageable);
    }

    public Office getOfficeById(Long id) {
        Optional<Office> officeOptional = officeRepository.findById(id);
        return officeOptional.orElse(null);
    }

    public Office createOffice(Office pOffice) {
        pOffice.setId(null);
        return officeRepository.save(pOffice);
    }

    public Office updateOffice(Long id, Office pOffice) {
        Optional<Office> officeOptional = officeRepository.findById(id);
        if (officeOptional.isPresent()) {
            pOffice.setId(id);
            return officeRepository.save(pOffice);
        }
        return null;
    }

    public boolean deleteOffice(Long id) {
        Optional<Office> officeOptional = officeRepository.findById(id);
        if (officeOptional.isPresent()) {
            officeRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean existsByPhone(String phone) {
        return officeRepository.existsByPhone(phone);
    }
}
package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.*;
import com.devcamp.shopplusbackend.repository.ProductBrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ProductBrandService {
    @Autowired
    private ProductBrandRepository productBrandRepository;

    public List<ProductBrand> getAllProductBrands() {
        return productBrandRepository.findAll();
    }

    public Page<ProductBrand> getProductBrands(String keyword, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return productBrandRepository.findProductBrands(keyword, pageable);
    }

    public ProductBrand getProductBrandById(Long id) {
        Optional<ProductBrand> productBrandOptional = productBrandRepository.findById(id);
        return productBrandOptional.orElse(null);
    }

    public Set<ProductLine> getProductLines(Long id) {
        Optional<ProductBrand> productBrandOptional = productBrandRepository.findById(id);
        return productBrandOptional.map(ProductBrand::getProductLines).orElse(null);
    }

    public ProductBrand createProductBrand(ProductBrand pProductBrand) {
        pProductBrand.setId(null);
        return productBrandRepository.save(pProductBrand);
    }

    public ProductBrand updateProductBrand(Long id, ProductBrand pProductBrand) {
        Optional<ProductBrand> productBrandOptional = productBrandRepository.findById(id);
        if (productBrandOptional.isPresent()) {
            pProductBrand.setId(id);
            return productBrandRepository.save(pProductBrand);
        }
        return null;
    }

    public boolean deleteProductBrand(Long id) {
        Optional<ProductBrand> productBrandOptional = productBrandRepository.findById(id);
        if (productBrandOptional.isPresent()) {
            productBrandRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean existsByName(String name) {
        return productBrandRepository.existsByName(name);
    }
}
package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.Graphics;
import com.devcamp.shopplusbackend.entity.GraphicsType;
import com.devcamp.shopplusbackend.repository.GraphicsTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class GraphicsTypeService {
    @Autowired
    private GraphicsTypeRepository graphicsTypeRepository;

    public Page<GraphicsType> getGraphicsTypes(String keyword, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return graphicsTypeRepository.findGraphicsTypes(keyword, pageable);
    }

    public GraphicsType getGraphicsTypeById(Long id) {
        Optional<GraphicsType> graphicsTypeOptional = graphicsTypeRepository.findById(id);
        return graphicsTypeOptional.orElse(null);
    }

    public Set<Graphics> getGraphics(Long id) {
        Optional<GraphicsType> graphicsTypeOptional = graphicsTypeRepository.findById(id);
        return graphicsTypeOptional.map(GraphicsType::getGraphics).orElse(null);
    }

    public GraphicsType createGraphicsType(GraphicsType pGraphicsType) {
        pGraphicsType.setId(null);
        return graphicsTypeRepository.save(pGraphicsType);
    }

    public GraphicsType updateGraphicsType(Long id, GraphicsType pGraphicsType) {
        Optional<GraphicsType> graphicsTypeOptional = graphicsTypeRepository.findById(id);
        if (graphicsTypeOptional.isPresent()) {
            pGraphicsType.setId(id);
            return graphicsTypeRepository.save(pGraphicsType);
        }
        return null;
    }

    public boolean deleteGraphicsType(Long id) {
        Optional<GraphicsType> graphicsTypeOptional = graphicsTypeRepository.findById(id);
        if (graphicsTypeOptional.isPresent()) {
            graphicsTypeRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean existsByName(String name) {
        return graphicsTypeRepository.existsByName(name);
    }
}
package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.Product;
import com.devcamp.shopplusbackend.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    //Lấy danh sách sản phẩm
    public Page<Product> getProducts(String keyword, Long productBrandId, Long processorBrandId, int enabled, String sortBy, int page, int size) {
        Pageable pageable = switch (sortBy) {
            case "1" -> PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "discountPercentage"));
            case "2" -> PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "discountPercentage"));
            case "3" -> PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "buyPrice"));
            case "4" -> PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "buyPrice"));
            case "5" -> PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "createdAt"));
            case "6" -> PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdAt"));
            case "7" -> PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "quantityInStock"));
            case "8" -> PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "quantityInStock"));
            default -> PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdAt"));
        };
        boolean deleted = enabled == 0;
        return productRepository.findProducts(keyword, productBrandId, processorBrandId, deleted, pageable);
    }

    public Page<Product> searchProducts(String keyword, Long productBrandId, Long productLineId, BigDecimal minScreenSize, BigDecimal maxScreenSize, BigDecimal minPrice, BigDecimal maxPrice, Integer memory, Integer storageSize, String sortBy, int page, int size) {
        Pageable pageable = switch (sortBy) {
            case "1" -> PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "discountPercentage"));
            case "2" -> PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "buyPrice"));
            case "3" -> PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "buyPrice"));
            case "4" -> PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdAt"));
            default -> PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "quantityInStock"));
        };
        return productRepository.searchProducts(keyword, productBrandId, productLineId, minScreenSize, maxScreenSize, minPrice, maxPrice, memory, storageSize, pageable);
    }

    //Lấy top sản phẩm theo số lượng đơn hàng bởi người dùng
    public Page<Product> bestSellersProducts(String startDateString, String endDateString, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Date startDate = Date.valueOf(LocalDate.parse(startDateString));
        Date endDate = Date.valueOf(LocalDate.parse(endDateString).plusDays(1));
        return productRepository.bestSellersProducts(startDate, endDate, pageable);
    }

    //Lấy top sản phẩm theo số lượng đơn hàng bởi quản trị viên
    public Page<Product> topSellingProductsByTotalOrders(String startDateString, String endDateString, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Date startDate = Date.valueOf(LocalDate.parse(startDateString));
        Date endDate = Date.valueOf(LocalDate.parse(endDateString).plusDays(1));
        return productRepository.topSellingProductsByTotalOrders(startDate, endDate, pageable);
    }

    //Lấy top sản phẩm theo doanh thu bởi quản trị viên
    public Page<Product> topSellingProductsByTotalSales(String startDateString, String endDateString, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Date startDate = Date.valueOf(LocalDate.parse(startDateString));
        Date endDate = Date.valueOf(LocalDate.parse(endDateString).plusDays(1));
        return productRepository.topSellingProductsByTotalSales(startDate, endDate, pageable);
    }

    //Lấy danh sách sản phẩm bởi select tìm kiếm
    public List<Product> selectSearchProducts(String keyword) {
        return productRepository.selectSearchProducts(keyword);
    }

    //Lấy thông tin sản phẩm theo id
    public Product getProductById(Long id) {
        Optional<Product> productOptional = productRepository.findById(id);
        return productOptional.orElse(null);
    }

    //Lấy tổng số đơn hàng của một sản phẩm theo thời gian
    public Long getTotalOrders(String startDateString, String endDateString, Long productId) {
        Date startDate = Date.valueOf(LocalDate.parse(startDateString));
        Date endDate = Date.valueOf(LocalDate.parse(endDateString).plusDays(1));
        return productRepository.getTotalOrders(startDate, endDate, productId);
    }

    //Lấy tổng doanh thu của một sản phẩm theo thời gian
    public BigDecimal getTotalSales(String startDateString, String endDateString, Long productId) {
        Date startDate = Date.valueOf(LocalDate.parse(startDateString));
        Date endDate = Date.valueOf(LocalDate.parse(endDateString).plusDays(1));
        return productRepository.getTotalSales(startDate, endDate, productId);
    }

    //Tạo thông tin sản phẩm
    public Product createProduct(Product pProduct) {
        pProduct.setId(null);
        
        return productRepository.save(pProduct);
    }

    //Chỉnh sửa thông tin sản phẩm
    public Product updateProduct(Long id, Product pProduct) {
        Optional<Product> productOptional = productRepository.findById(id);
        if (productOptional.isPresent()) {
            pProduct.setId(id);
            pProduct.setProductPhotos(productOptional.get().getProductPhotos());
            return productRepository.save(pProduct);
        }
        return null;
    }

    //Xóa thông tin sản phẩm
    public boolean deleteProduct(Long id) {
        Optional<Product> productOptional = productRepository.findById(id);
        if (productOptional.isPresent()) {
            productRepository.deleteById(id);
            return true;
        }
        return false;
    }

    //Enable/Disable sản phẩm
    public boolean disableOrEnableProduct(Long id) {
        Optional<Product> productOptional = productRepository.findById(id);
        if (productOptional.isPresent()) {
            boolean deleted = productOptional.get().isDeleted();
            productRepository.disableOrEnableProduct(id, !deleted);
            return true;
        }
        return false;
    }

    //Kiểm tra mã sản phẩm đã tồn tại hay chưa
    public boolean existsByProductCode(String productCode) {
        return productRepository.existsByProductCode(productCode);
    }
}

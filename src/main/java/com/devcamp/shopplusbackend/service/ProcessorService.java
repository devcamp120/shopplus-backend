package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.Processor;
import com.devcamp.shopplusbackend.repository.ProcessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProcessorService {
    @Autowired
    private ProcessorRepository processorRepository;

    public Page<Processor> getProcessors(String keyword, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return processorRepository.findProcessors(keyword, pageable);
    }

    public List<Processor> selectSearchProcessors(String keyword) {
        return processorRepository.selectSearchProcessors(keyword);
    }

    public Processor getProcessorById(Long id) {
        Optional<Processor> processorOptional = processorRepository.findById(id);
        return processorOptional.orElse(null);
    }

    public Processor createProcessor(Processor pProcessor) {
        pProcessor.setId(null);
        return processorRepository.save(pProcessor);
    }

    public Processor updateProcessor(Long id, Processor pProcessor) {
        Optional<Processor> processorOptional = processorRepository.findById(id);
        if (processorOptional.isPresent()) {
            pProcessor.setId(id);
            return processorRepository.save(pProcessor);
        }
        return null;
    }

    public boolean deleteProcessor(Long id) {
        Optional<Processor> processorOptional = processorRepository.findById(id);
        if (processorOptional.isPresent()) {
            processorRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean existsByProcessorNumber(String name) {
        return processorRepository.existsByProcessorNumber(name);
    }
}
package com.devcamp.shopplusbackend.service.impl;

import com.devcamp.shopplusbackend.entity.Role;
import com.devcamp.shopplusbackend.entity.User;
import com.devcamp.shopplusbackend.model.RequestUser;
import com.devcamp.shopplusbackend.model.UserInfo;
import com.devcamp.shopplusbackend.repository.RoleRepository;
import com.devcamp.shopplusbackend.repository.UserRepository;
import com.devcamp.shopplusbackend.security.UserPrincipal;
import com.devcamp.shopplusbackend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder encoder;

    //Lấy danh sách tài khoản người dùng
    @Override
    public Page<User> getUsers(String keyword, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return userRepository.findUsers(keyword, pageable);
    }

    //Lấy danh sách người dùng theo số tiền mua hàng
    @Override
    public Page<User> getUsersByCustomerType(String keyword, BigDecimal totalSales, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return userRepository.findUsersByCustomerType(keyword, totalSales, pageable);
    }

    //Lấy danh sách người dùng bởi select tìm kiếm
    @Override
    public List<User> selectSearchCustomers(String keyword) {
        return userRepository.selectSearchCustomers(keyword);
    }

    //Lấy thông tin tài khoản theo id
    @Override
    public User getUserById(Long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        return optionalUser.orElse(null);
    }

    //Lấy số lượng tài khoản đã đăng ký theo thời gian
    @Override
    public Long getTotalUsers(String startDateString, String endDateString) {
        Date startDate = java.sql.Date.valueOf(LocalDate.parse(startDateString));
        Date endDate = java.sql.Date.valueOf(LocalDate.parse(endDateString).plusDays(1));
        return userRepository.getTotalUsers(startDate, endDate);
    }

    // Đăng ký tài khoản
    @Override
    public User registerUser(RequestUser requestUser) {
        if (existsByUsername(requestUser.getUsername())) {
            logger.error("Error: Username is already taken!");
            return null;
        }
        User user = userRepository.findByUsername(requestUser.getUsername());
        Set<Role> roles = new HashSet<>();
        Optional<Role> optionalRole = roleRepository.findByRoleKey("ROLE_CUSTOMER");
        optionalRole.ifPresent(roles::add);
        if (user == null) {
            user = new User();
            user.setUsername(requestUser.getUsername());
        }
        if (!roles.isEmpty()) {
            user.setFirstName(requestUser.getFirstName());
            user.setLastName(requestUser.getLastName());
            user.setPassword(encoder.encode(requestUser.getPassword()));
            user.setAddress(requestUser.getAddress());
            user.setCity(requestUser.getCity());
            user.setState(requestUser.getState());
            user.setPostalCode(requestUser.getPostalCode());
            user.setCountry(requestUser.getCountry());
            user.setActivated(true);
            user.setRoles(roles);
            return userRepository.saveAndFlush(user);
        } else {
            logger.error("Error: Roles not found.");
            return null;
        }
    }

    //Tạo người dùng bởi admin
    @Override
    public User createUserByAdmin(RequestUser requestUser) {
        if (existsByUsername(requestUser.getUsername())) {
            logger.error("Error: Username is already taken!");
            return null;
        }
        User user = userRepository.findByUsername(requestUser.getUsername());
        Set<String> strRoles = requestUser.getStrRoles();
        Set<Role> roles = new HashSet<>();
        if (strRoles == null || strRoles.isEmpty()) {
            Optional<Role> optionalRole = roleRepository.findByRoleKey("ROLE_CUSTOMER");
            optionalRole.ifPresent(roles::add);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "ROLE_ADMIN" -> {
                        Optional<Role> optionalRoleAdmin = roleRepository.findByRoleKey("ROLE_ADMIN");
                        optionalRoleAdmin.ifPresent(roles::add);
                    }
                    case "ROLE_SELLER" -> {
                        Optional<Role> optionalRoleSeller = roleRepository.findByRoleKey("ROLE_SELLER");
                        optionalRoleSeller.ifPresent(roles::add);
                    }
                    default -> {
                        Optional<Role> optionalRoleCustomer = roleRepository.findByRoleKey("ROLE_CUSTOMER");
                        optionalRoleCustomer.ifPresent(roles::add);
                    }
                }
            });
        }
        if (user == null) {
            user = new User();
            user.setUsername(requestUser.getUsername());
        }
        if (!roles.isEmpty()) {
            user.setFirstName(requestUser.getFirstName());
            user.setLastName(requestUser.getLastName());
            user.setPassword(encoder.encode(requestUser.getPassword()));
            user.setAddress(requestUser.getAddress());
            user.setCity(requestUser.getCity());
            user.setState(requestUser.getState());
            user.setPostalCode(requestUser.getPostalCode());
            user.setCountry(requestUser.getCountry());
            user.setActivated(requestUser.isActivated());
            user.setRoles(roles);
            return userRepository.saveAndFlush(user);
        } else {
            logger.error("Error: Roles not found.");
            return null;
        }
    }

    //Tạo người dùng bởi seller
    @Override
    public User createUserBySeller(RequestUser requestUser) {
        if (existsByUsername(requestUser.getUsername())) {
            logger.error("Error: Username is already taken!");
            return null;
        }
        User user = userRepository.findByUsername(requestUser.getUsername());
        Set<Role> roles = new HashSet<>();
        Optional<Role> optionalRole = roleRepository.findByRoleKey("ROLE_CUSTOMER");
        optionalRole.ifPresent(roles::add);
        if (user == null) {
            user = new User();
            user.setUsername(requestUser.getUsername());
        }
        if (!roles.isEmpty()) {
            user.setFirstName(requestUser.getFirstName());
            user.setLastName(requestUser.getLastName());
            user.setPassword("no-password");
            user.setAddress(requestUser.getAddress());
            user.setCity(requestUser.getCity());
            user.setState(requestUser.getState());
            user.setPostalCode(requestUser.getPostalCode());
            user.setCountry(requestUser.getCountry());
            user.setActivated(false);
            user.setRoles(roles);
            return userRepository.saveAndFlush(user);
        } else {
            logger.error("Error: Roles not found.");
            return null;
        }
    }

    //Chỉnh sửa thông tin người dùng bởi admin
    @Override
    public User updateUserByAdmin(Long id, RequestUser requestUser) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            Set<String> strRoles = requestUser.getStrRoles();
            Set<Role> roles = new HashSet<>();
            if (strRoles == null || strRoles.isEmpty()) {
                Optional<Role> optionalRole = roleRepository.findByRoleKey("ROLE_CUSTOMER");
                optionalRole.ifPresent(roles::add);
            } else {
                strRoles.forEach(role -> {
                    switch (role) {
                        case "ROLE_ADMIN" -> {
                            Optional<Role> optionalRoleAdmin = roleRepository.findByRoleKey("ROLE_ADMIN");
                            optionalRoleAdmin.ifPresent(roles::add);
                        }
                        case "ROLE_SELLER" -> {
                            Optional<Role> optionalRoleSeller = roleRepository.findByRoleKey("ROLE_SELLER");
                            optionalRoleSeller.ifPresent(roles::add);
                        }
                        default -> {
                            Optional<Role> optionalRoleCustomer = roleRepository.findByRoleKey("ROLE_CUSTOMER");
                            optionalRoleCustomer.ifPresent(roles::add);
                        }
                    }
                });
            }
            if (!roles.isEmpty()) {
                user.setFirstName(requestUser.getFirstName());
                user.setLastName(requestUser.getLastName());
                if (!Objects.equals(requestUser.getPassword(), "")) {
                    user.setPassword(encoder.encode(requestUser.getPassword()));
                }
                user.setAddress(requestUser.getAddress());
                user.setCity(requestUser.getCity());
                user.setState(requestUser.getState());
                user.setPostalCode(requestUser.getPostalCode());
                user.setCountry(requestUser.getCountry());
                user.setActivated(requestUser.isActivated());
                user.setRoles(roles);
                return userRepository.saveAndFlush(user);
            } else {
                logger.error("Error: Roles not found.");
                return null;
            }
        } else {
            logger.error("Error: User not found!");
            return null;
        }
    }

    //Chỉnh sửa thông tin người dùng bởi seller
    @Override
    public User updateUserBySeller(Long id, RequestUser requestUser) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setFirstName(requestUser.getFirstName());
            user.setLastName(requestUser.getLastName());
            user.setAddress(requestUser.getAddress());
            user.setCity(requestUser.getCity());
            user.setState(requestUser.getState());
            user.setPostalCode(requestUser.getPostalCode());
            user.setCountry(requestUser.getCountry());
            return userRepository.saveAndFlush(user);
        } else {
            logger.error("Error: Roles not found.");
            return null;
        }
    }

    //Đổi mật khẩu
    @Override
    public boolean changePassword(String username, String currentPassword, String newPassword) {
        User user = userRepository.findByUsername(username);
        if (user != null && encoder.matches(currentPassword, user.getPassword())) {
            user.setPassword(encoder.encode(newPassword));
            userRepository.saveAndFlush(user);
            return true;
        }
        return false;
    }

    //Chỉnh sửa thông tin người dùng
    @Override
    public UserInfo updateProfile(String username, RequestUser requestUser) {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            user.setFirstName(requestUser.getFirstName());
            user.setLastName(requestUser.getLastName());
            user.setAddress(requestUser.getAddress());
            user.setCity(requestUser.getCity());
            user.setState(requestUser.getState());
            user.setPostalCode(requestUser.getPostalCode());
            user.setCountry(requestUser.getCountry());
            User _user = userRepository.saveAndFlush(user);
            Set<String> strRoles = new HashSet<>();
            user.getRoles().forEach(role -> strRoles.add(role.getRoleKey()));
            return new UserInfo(_user.getId(), _user.getFirstName(), _user.getLastName(), _user.getUsername(), _user.getAddress(), _user.getCity(), _user.getState(), _user.getPostalCode(), _user.getCountry(), strRoles);
        }
        return null;
    }

    //Lấy thông tin xác thực
    @Override
    public UserPrincipal findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        UserPrincipal userPrincipal = new UserPrincipal();
        if (user != null) {
            Set<String> authorities = new HashSet<>();
            if (null != user.getRoles()) user.getRoles().forEach(r -> {
                authorities.add(r.getRoleKey());
            });
            userPrincipal.setId(user.getId());
            userPrincipal.setUsername(user.getUsername());
            userPrincipal.setPassword(user.getPassword());
            userPrincipal.setDeleted(user.isDeleted());
            userPrincipal.setActivated(user.isActivated());
            userPrincipal.setAuthorities(authorities);
        }
        return userPrincipal;
    }

    //Lấy thông tin người dùng
    @Override
    public UserInfo getUserInfoByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            Set<String> strRoles = new HashSet<>();
            user.getRoles().forEach(role -> strRoles.add(role.getRoleKey()));
            return new UserInfo(user.getId(), user.getFirstName(), user.getLastName(), user.getUsername(), user.getAddress(), user.getCity(), user.getState(), user.getPostalCode(), user.getCountry(), strRoles);
        }
        return null;
    }

    //Enable/Disable tài khoản
    public boolean disableOrEnableUser(Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            boolean deleted = userOptional.get().isDeleted();
            userRepository.disableOrEnableUser(id, !deleted);
            return true;
        }
        return false;
    }

    //Kiểm tra username tồn tại hay chưa
    @Override
    public boolean existsByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            return false;
        } else {
            return user.isActivated();
        }
    }
}
package com.devcamp.shopplusbackend.service.impl;

import com.devcamp.shopplusbackend.entity.Token;
import com.devcamp.shopplusbackend.repository.TokenRepository;
import com.devcamp.shopplusbackend.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRepository tokenRepository;

    public Token createToken(Token token) {
        return tokenRepository.saveAndFlush(token);
    }

    @Override
    public Token findByToken(String token) {
        return tokenRepository.findByToken(token);
    }

    @Override
    public void deleteTokenByCreatedBy(Long userId) {
        tokenRepository.deleteTokenByCreatedBy(userId);
    }
}

package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.Graphics;
import com.devcamp.shopplusbackend.repository.GraphicsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GraphicsService {
    @Autowired
    private GraphicsRepository graphicsRepository;

    public Page<Graphics> getGraphics(String keyword, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return graphicsRepository.findGraphics(keyword, pageable);
    }

    public List<Graphics> selectSearchGraphics(String keyword) {
        return graphicsRepository.selectSearchGraphics(keyword);
    }

    public Graphics getGraphicsById(Long id) {
        Optional<Graphics> graphicsOptional = graphicsRepository.findById(id);
        return graphicsOptional.orElse(null);
    }

    public Graphics createGraphics(Graphics pGraphics) {
        pGraphics.setId(null);
        return graphicsRepository.save(pGraphics);
    }

    public Graphics updateGraphics(Long id, Graphics pGraphics) {
        Optional<Graphics> graphicsOptional = graphicsRepository.findById(id);
        if (graphicsOptional.isPresent()) {
            pGraphics.setId(id);
            return graphicsRepository.save(pGraphics);
        }
        return null;
    }

    public boolean deleteGraphics(Long id) {
        Optional<Graphics> graphicsOptional = graphicsRepository.findById(id);
        if (graphicsOptional.isPresent()) {
            graphicsRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean existsByGpuName(String gpuName) {
        return graphicsRepository.existsByGpuName(gpuName);
    }
}
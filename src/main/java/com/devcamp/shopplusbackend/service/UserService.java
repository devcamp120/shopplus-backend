package com.devcamp.shopplusbackend.service;

import com.devcamp.shopplusbackend.entity.User;
import com.devcamp.shopplusbackend.model.RequestUser;
import com.devcamp.shopplusbackend.model.UserInfo;
import com.devcamp.shopplusbackend.security.UserPrincipal;
import org.springframework.data.domain.Page;

import java.math.BigDecimal;
import java.util.List;

public interface UserService {
    Page<User> getUsers(String keyword, int page, int size);

    Page<User> getUsersByCustomerType(String keyword, BigDecimal totalSales, int page, int size);

    List<User> selectSearchCustomers(String keyword);

    User getUserById(Long id);

    Long getTotalUsers(String startDateString, String endDateString);

    User registerUser(RequestUser requestUser);

    User createUserByAdmin(RequestUser requestUser);

    User createUserBySeller(RequestUser requestUser);

    User updateUserBySeller(Long id, RequestUser requestUser);

    User updateUserByAdmin(Long id, RequestUser requestUser);

    UserInfo updateProfile(String username, RequestUser userEdit);

    boolean changePassword(String username, String currentPassword, String newPassword);

    UserPrincipal findByUsername(String username);

    UserInfo getUserInfoByUsername(String username);

    boolean disableOrEnableUser(Long id);

    boolean existsByUsername(String username);
}

package com.devcamp.shopplusbackend.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Set;

@Entity
@Table(name = "product_lines")
@Getter
@Setter
public class ProductLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, unique = true, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "product_brand_id", nullable = false)
    private ProductBrand productBrand;

    @Column(length = 2500)
    private String description;

    @OneToMany(mappedBy = "productLine")
    @JsonIgnore
    private Set<Product> products;

    @Transient
    private String fullName;

    public String getFullName() {
        if (productBrand == null) {
            return name;
        }
        return productBrand.getName() + " " + name;
    }
}

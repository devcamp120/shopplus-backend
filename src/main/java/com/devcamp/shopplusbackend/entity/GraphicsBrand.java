package com.devcamp.shopplusbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "graphics_brands")
@Getter
@Setter
public class GraphicsBrand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, unique = true, nullable = false)
    private String name;

    @Column(length = 2500)
    private String description;

    @OneToMany(mappedBy = "graphicsBrand")
    @JsonIgnore
    private Set<GraphicsType> graphicsTypes;

    @OneToMany(mappedBy = "graphicsBrand")
    @JsonIgnore
    private Set<Graphics> graphics;
}

package com.devcamp.shopplusbackend.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "payments")
@Getter
@Setter
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, unique = true, updatable = false, nullable = false)
    private String checkNumber;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(updatable = false, nullable = false)
    private Date paymentDate;

    @Column(precision = 10, scale = 2, nullable = false)
    private BigDecimal ammount;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}

package com.devcamp.shopplusbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product_brand_photos")
@Getter
@Setter
public class ProductBrandPhoto extends Photo {
    @OneToOne
    @JoinColumn(name = "product_brand_id", nullable = false)
    @JsonIgnore
    private ProductBrand productBrand;
}

package com.devcamp.shopplusbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "processors")
@Getter
@Setter
public class Processor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, unique = true, nullable = false)
    private String processorNumber;

    @ManyToOne
    @JoinColumn(name = "processor_brand_id", nullable = false)
    private ProcessorBrand processorBrand;

    @ManyToOne
    @JoinColumn(name = "processor_collection_id")
    private ProcessorCollection processorCollection;

    private Byte totalCores;

    private Byte totalThreads;

    @Column(precision = 10, scale = 2)
    private BigDecimal baseFrequency;

    @Column(precision = 10, scale = 2)
    private BigDecimal maxTurboFrequency;

    private Byte cache;

    @Column(length = 100)
    private String processorGraphics;

    @Column(precision = 10, scale = 2)
    private BigDecimal graphicsFrequency;

    @OneToMany(mappedBy = "processor")
    @JsonIgnore
    private Set<Product> products;

    @Transient
    private String fullName;

    public String getFullName() {
        if (processorCollection == null) {
            if (processorBrand == null) {
                return processorNumber;
            }
            return processorBrand.getName() + " " + processorNumber;
        }
        return processorBrand.getName() + " " + processorCollection.getName() + " " + processorNumber;
    }
}

package com.devcamp.shopplusbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "product_brands")
@Getter
@Setter
public class ProductBrand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, unique = true, nullable = false)
    private String name;

    @Column(length = 2500)
    private String description;

    @OneToMany(mappedBy = "productBrand")
    @JsonIgnore
    private Set<ProductLine> productLines;

    @OneToMany(mappedBy = "productBrand")
    @JsonIgnore
    private Set<Product> products;

    @OneToOne(cascade = CascadeType.REMOVE, mappedBy = "productBrand")
    private ProductBrandPhoto productBrandPhoto;
}
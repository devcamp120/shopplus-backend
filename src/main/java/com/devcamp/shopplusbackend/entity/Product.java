package com.devcamp.shopplusbackend.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "products")
@Getter
@Setter
public class Product extends BaseEntity {
    @Column(length = 50, nullable = false, unique = true)
    private String productCode;

    @ManyToOne
    @JoinColumn(name = "product_brand_id", nullable = false)
    private ProductBrand productBrand;

    @ManyToOne
    @JoinColumn(name = "product_line_id")
    private ProductLine productLine;

    @Column(length = 255, nullable = false)
    private String productName;

    @Column(length = 2500)
    private String productDescription;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "yyyy-MM")
    private Date releaseDate;

    @Column(precision = 10, scale = 2, nullable = false)
    private BigDecimal buyPrice;

    private Byte discountPercentage;

    @Column(length = 50, nullable = false)
    private String color;

    @ManyToOne
    @JoinColumn(name = "processor_id", nullable = false)
    private Processor processor;

    @ManyToOne
    @JoinColumn(name = "graphics_id")
    private Graphics graphics;

    @Column(length = 50, nullable = false)
    private String storageType;

    @Column(nullable = false)
    private Integer totalStorageCapacity;

    @Column(length = 50)
    private String ssdType;

    @Column(nullable = false)
    private Integer systemMemory;

    @Column(length = 50, nullable = false)
    private String typeOfMemory;

    private Integer systemMemorySpeed;

    private Byte numberOfMemorySlots;

    private Byte numberOfMemorySticksIncluded;

    @Column(length = 50)
    private String screenType;

    @Column(length = 50)
    private String displayType;

    @Column(precision = 10, scale = 2, nullable = false)
    private BigDecimal screenSize;

    @Column(nullable = false)
    private Integer screenResolutionX;

    @Column(nullable = false)
    private Integer screenResolutionY;

    @Column(columnDefinition = "boolean default false not null")
    private Boolean touchScreen = false;

    @Column(length = 50)
    private String wifi;

    @Column(length = 50)
    private String bluetooth;

    @Column(columnDefinition = "boolean default false")
    private Boolean headphoneJack = false;

    private Byte numberOfUSBPorts;

    @Column(length = 50, nullable = false)
    private String batteryType;

    private Byte batteryLife;

    private Byte batteryCells;

    private Integer powerSupply;

    @Column(length = 100, nullable = false)
    private String operatingSystem;

    @Column(columnDefinition = "boolean default false")
    private Boolean frontFacingCamera = false;

    @Column(columnDefinition = "boolean default false")
    private Boolean numericKeyboard = false;

    @Column(columnDefinition = "boolean default false")
    private Boolean backlitKeyboard = false;

    @Column(precision = 10, scale = 2, nullable = false)
    private BigDecimal productWidth;

    @Column(precision = 10, scale = 2, nullable = false)
    private BigDecimal productHeight;

    @Column(precision = 10, scale = 2, nullable = false)
    private BigDecimal productDepth;

    @Column(precision = 10, scale = 2, nullable = false)
    private BigDecimal productWeight;

    private Byte warranty;

    @Column(nullable = false)
    private Integer quantityInStock;

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private Set<OrderDetail> orderDetails;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
    private Set<ProductPhoto> productPhotos;

    @Transient
    private String fullName;

    @Transient
    private BigDecimal priceEach;

    public String getFullName() {
        if (productLine == null) {
            if (productBrand == null) {
                return productName;
            }
            return productBrand.getName() + " " + productName;
        }
        return productBrand.getName() + " " + productLine.getName() + " " + productName;
    }

    public BigDecimal getPriceEach() {
        if (discountPercentage != null && discountPercentage >= 0 && discountPercentage <= 100) {
            BigDecimal discount = (new BigDecimal(discountPercentage)).divide(new BigDecimal(100));
            return buyPrice.subtract(buyPrice.multiply(discount));
        } else {
            return buyPrice;
        }
    }
}

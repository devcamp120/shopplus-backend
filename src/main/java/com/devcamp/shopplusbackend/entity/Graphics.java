package com.devcamp.shopplusbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "graphics")
@Getter
@Setter
public class Graphics {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, unique = true, nullable = false)
    private String gpuName;

    @ManyToOne
    @JoinColumn(name = "graphics_brand_id", nullable = false)
    private GraphicsBrand graphicsBrand;

    @ManyToOne
    @JoinColumn(name = "graphics_type_id")
    private GraphicsType graphicsType;

    @OneToMany(mappedBy = "graphics")
    @JsonIgnore
    private Set<Product> products;

    @Column(nullable = false)
    private Byte gpuMemory;

    @Transient
    private String fullName;

    public String getFullName() {
        if (graphicsType == null) {
            if (graphicsBrand == null) {
                return gpuName;
            }
            return graphicsBrand.getName() + " " + gpuName;
        }
        return  graphicsBrand.getName() + " " + graphicsType.getName() + " " + gpuName;
    }
}

package com.devcamp.shopplusbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ShopPlusBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShopPlusBackendApplication.class, args);
    }

}

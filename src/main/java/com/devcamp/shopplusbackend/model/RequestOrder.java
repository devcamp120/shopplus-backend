package com.devcamp.shopplusbackend.model;

import com.devcamp.shopplusbackend.entity.Order;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RequestOrder {
    Order order;

    private List<ProductSelected> productsSelected;

    private List<Long> orderDetailsDeleted;
}

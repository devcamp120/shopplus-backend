package com.devcamp.shopplusbackend.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class LoginResult {
    private String token;
    private Set<String> strRoles;

    public LoginResult(String token, Set<String> strRoles) {
        this.token = token;
        this.strRoles = strRoles;
    }
}

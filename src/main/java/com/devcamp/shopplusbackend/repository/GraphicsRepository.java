package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.Graphics;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GraphicsRepository extends JpaRepository<Graphics, Long> {
    @Query(value = "FROM Graphics g WHERE CONCAT(g.graphicsBrand.name, ' ', g.gpuName) LIKE %:keyword%")
    Page<Graphics> findGraphics(String keyword, Pageable pageable);

    @Query(value = "FROM Graphics g WHERE CONCAT(g.graphicsBrand.name, ' ', g.gpuName) LIKE %:keyword%")
    List<Graphics> selectSearchGraphics(String keyword);

    boolean existsByGpuName(String gpuName);
}
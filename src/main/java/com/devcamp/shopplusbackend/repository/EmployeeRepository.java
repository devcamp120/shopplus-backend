package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	@Query(value = "FROM Employee e WHERE e.lastName LIKE %:keyword% OR e.firstName LIKE %:keyword% OR e.email LIKE %:keyword%")
	Page<Employee> findEmployees(String keyword, Pageable pageable);

	@Query(value = "FROM Employee e WHERE e.lastName LIKE %:keyword% OR e.firstName LIKE %:keyword% OR e.email LIKE %:keyword%")
	List<Employee> selectSearchEmployees(String keyword);

	boolean existsByEmail(String email);
}

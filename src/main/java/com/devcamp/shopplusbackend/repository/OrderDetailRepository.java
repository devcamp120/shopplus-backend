package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.OrderDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {
    @Query(value = "FROM OrderDetail od WHERE :orderId IN(od.order.id, -1)")
    Page<OrderDetail> findOrderDetails(Long orderId, Pageable pageable);
}

package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.ProcessorCollection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessorCollectionRepository extends JpaRepository<ProcessorCollection, Long> {
    @Query(value = "FROM ProcessorCollection pc WHERE pc.name LIKE %:keyword%")
    Page<ProcessorCollection> findProcessorCollections(String keyword, Pageable pageable);

    boolean existsByName(String name);
}

package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    //Lấy danh sách đơn hàng
    @Query(value = "FROM Order o WHERE o.orderCode LIKE %:keyword% AND :userId IN(o.user.id, -1) AND (o.orderDate BETWEEN :startOrderDate AND :endOrderDate) AND (o.requiredDate BETWEEN :startRequiredDate AND :endRequiredDate) AND :status IN(o.status, 'All')")
    Page<Order> findOrders(String keyword, Long userId, Date startOrderDate, Date endOrderDate,Date startRequiredDate, Date endRequiredDate, String status, Pageable pageable);

    //Lấy danh sách đơn hàng bởi người dùng
    @Query(value = "FROM Order o WHERE o.user.id = :userId ORDER BY o.orderDate DESC")
    Page<Order> getOrdersByUser(Long userId, Pageable pageable);

    //Lấy danh sách đơn hàng bởi select tìm kiếm
    @Query(value = "FROM Order o WHERE o.orderCode LIKE %:keyword%")
    List<Order> selectSearchOrders(String keyword);

    //Lấy tổng doanh thu theo thời gian
    @Query(value = "SELECT SUM(od.priceEach * od.quantityOrder) FROM Order o JOIN OrderDetail od ON o.id = od.order.id WHERE (o.orderDate BETWEEN :startDate AND :endDate) AND (o.status = 'Completed')")
    BigDecimal getTotalSales(Date startDate, Date endDate);

    //Lấy tổng số đơn hàng theo thời gian
    @Query(value = "SELECT COUNT(o) FROM Order o WHERE (o.orderDate BETWEEN :startDate AND :endDate)")
    Long getTotalOrders(Date startDate, Date endDate);

    //Lấy thông tin đơn hàng bằng mã đơn hàng
    Order findByOrderCode(String orderCode);
}

package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.ProcessorBrand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessorBrandRepository extends JpaRepository<ProcessorBrand, Long> {
    @Query(value = "FROM ProcessorBrand pb WHERE pb.name LIKE %:keyword%")
    Page<ProcessorBrand> findProcessorBrands(String keyword, Pageable pageable);

    boolean existsByName(String name);
}

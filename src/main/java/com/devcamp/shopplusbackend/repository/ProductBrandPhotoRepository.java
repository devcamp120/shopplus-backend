package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.ProductBrandPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductBrandPhotoRepository extends JpaRepository<ProductBrandPhoto, Long> {
}

package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.Office;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OfficeRepository extends JpaRepository<Office, Long> {
    @Query(value = "FROM Office o WHERE o.phone LIKE %:keyword% OR o.addressLine LIKE %:keyword%")
    Page<Office> findOffices(String keyword, Pageable pageable);

    boolean existsByPhone(String phone);
}

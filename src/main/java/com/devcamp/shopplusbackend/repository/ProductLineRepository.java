package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.ProductLine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductLineRepository extends JpaRepository<ProductLine, Long> {
    @Query(value = "FROM ProductLine pl WHERE pl.name LIKE %:keyword%")
    Page<ProductLine> findProductLines(String keyword, Pageable pageable);
    
    boolean existsByName(String name);
}

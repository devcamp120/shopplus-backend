package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface TokenRepository extends JpaRepository<Token, Long> {

    Token findByToken(String token);

    @Transactional
    @Modifying
    @Query(value = "UPDATE Token t SET t.deleted = 1 WHERE t.createdBy = :userId")
    void deleteTokenByCreatedBy(Long userId);
}

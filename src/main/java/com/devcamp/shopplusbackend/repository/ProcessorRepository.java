package com.devcamp.shopplusbackend.repository;

import com.devcamp.shopplusbackend.entity.Processor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProcessorRepository extends JpaRepository<Processor, Long> {
    @Query(value = "FROM Processor p WHERE CONCAT(p.processorBrand.name, ' ', p.processorNumber) LIKE %:keyword%")
    Page<Processor> findProcessors(String keyword, Pageable pageable);

    @Query(value = "FROM Processor p WHERE CONCAT(p.processorBrand.name, ' ', p.processorNumber) LIKE %:keyword%")
    List<Processor> selectSearchProcessors(String keyword);

    boolean existsByProcessorNumber(String processorNumber);
}
